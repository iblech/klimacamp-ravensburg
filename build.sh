#!/usr/bin/env nix-shell
#! nix-shell -I nixpkgs=https://nixos.org/channels/nixos-21.05/nixexprs.tar.xz -i bash -p ruby.devEnv bundix rake jekyll rubyPackages.public_suffix rubyPackages.colorator rubyPackages.concurrent-ruby rubyPackages.eventmachine rubyPackages.rake rubyPackages.httpclient rubyPackages.ffi rubyPackages.i18n rubyPackages.sassc nodejs perl

set -e

bundle install
bundle exec jekyll build

find _site -name '*.html' -print0 | \
  xargs -0 \
    sed -i \
      -e 's#<script type="text/javascript"#<script async="async"#g' \
      -e 's#<nav role="navigation"#<nav#' \
      -e 's#<title>[^<]*</title>##'

./relativize-urls.sh
