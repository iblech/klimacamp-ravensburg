---
layout: page
title: CO₂-Budget
permalink: /co2-budget/
nav_order: 60
---

# CO₂-Budget und -Emissionen von Ravensburg (Landkreis und Stadt)

Das Pariser Klimaabkommen aus dem Jahr 2015 sieht eine Reduktion
klimaschädlicher Treibhausgasemissionen für die Weltgemeinschaft vor, damit die
globale Erhitzung deutlich unter 1,5 °C bleibt.

Dieses Ziel wurde beschlossen, da sonst zu befürchten ist, dass Kippelemente
des Öko- und Klimasystems ausgelöst werden. Überschreiten wir diese 1,5 °C
Erhitzung, kann eine Spirale des sich selbst verstärkenden Klimawandels
entstehen. Die Bundesrepublik hat dieses Papier unterschrieben.

Die Klimaschutzziele der Stadt Ravensburg und des Landkreises Ravensburg sind jedoch völlig
unzureichend, um innerhalb des CO₂-Budgets zu bleiben, welches uns laut dem
IPCC-Bericht anteilig noch als Stadtgemeinschaft zustehen würde. Bis zum Jahr
2040 plant die Stadt Ravensburg mehr als **4 Millionen Tonnen CO₂** zu emittieren.
Ihr steht insgesamt aber nur noch eine Menge von **2 Millionen Tonnen** zu. Diese
Menge wird laut städtischer Zielsetzung bis 2024 emittiert sein! Dies ist
Unrecht, und deswegen sind wir hier.


## Welches Budget verbleibt uns?

Laut dem [IPCC-Sonderbericht von
2018](https://de.wikipedia.org/wiki/Sonderbericht_1,5_%C2%B0C_globale_Erw%C3%A4rmung) hat die Weltgemeinschaft ein
Emissionsbudget von **420 Gigatonnen** an CO₂, um mit einer ⅔-Wahrscheinlichkeit
die Erderhitzung auf 1,5 Grad zu beschränken.

Deutschland hat einen Anteil von 1,1% an der Weltbevölkerung. Dementsprechend
standen Deutschland Anfang 2018 4,6 Gigatonnen zu.

In den Jahren 2018 und 2019 emittierte Deutschland jährlich etwa 0,8
Gigatonnen. Also stehen im Jahr 2020 Deutschland noch etwa **3 Gigatonnen** zu.

0,06% Prozent der deutschen Bevölkerung lebt in der Stadt Ravensburg. Damit
steht der Stadt Ravensburg ab 2020 ein Rest-CO₂-Budget von 0,0018 Gigatonnen
zu. Das sind **1,8 Millionen Tonnen CO₂**. Entsprechend steht dem Landkreis ein
Restbudget von **10 Millionen Tonnen CO₂ zu**.


## Wann wird das CO₂-Restbudget bei den derzeitigen Emissionen aufgebraucht sein?

Die Stadt Ravensburg verursacht **[0,43 Milllionen Tonnen
CO₂](https://www.ravensburg.de/rv-wAssets/pdf/gesellschaft-soziales/Klimakommission-Ravensburg-29-11-2019-Vortrag-Veerle-Buytaert.pdf#page=14)** pro Jahr (Stand
2017). Damit ist das Restbudget **2024** aufgebraucht.

Für den Landkreis sieht die Rechnung ganz ähnlich aus: Der Landkreis Ravensburg
verursacht **[2,5 Millionen
Tonnen](https://www.rv.de/site/LRA-RV/get/params_E2047201752/11193598/2015_03_17_EKK%20LK%20RV_mit_EPAP.pdf#pafe=73)**
pro Jahr (Stand 2012). Damit ist das Restbudget 2024 aufgebraucht.

Im Übrigen ist es schon ein Skandal an sich, dass keine neueren Zahlen verfügbar
sind: Es kann nichts kontrolliert werden, was nicht gemessen wird!


## Was sind die aktuellen Klimaschutzziele?

Der Gemeinderat der Stadt Ravensburg [beschloss am 28. Juli
2020](https://www.ravensburg.de/rv/klimakonsens.php), bis 2040
klimaneutral zu werden, die Emissionen also auf Nettonull zu reduzieren. Bei
einer angenommenen linearen Reduktion plant die Stadt Ravensburg also, ab 2020
noch 4 Millionen Tonnen CO₂ zu verantworten. **Das ist das Doppelte der
CO₂-Menge, die Ravensburg noch zusteht.**

Der Landkreis Ravensburg hat sich gar keine Klimaneutralität zum Ziel gesetzt.
Er [plant
nur](https://www.rv.de/site/LRA-RV/get/params_E2047201752/11193598/2015_03_17_EKK%20LK%20RV_mit_EPAP.pdf#page=32), bis 2022 die Treibhausgasemissionen in Bezug auf 1990 um 40 % zu
senken. Ein Ziel für die Jahre ab 2023 ist nicht beschlossen. Es klafft also
nicht nur eine gewaltige Umsetzungslücke, sondern sogar noch eine gewaltige
Ambitionslücke.


## Kritik

1. Der Landkreis Ravensburg hat Industrie und stellt Güter für andere Städte her. Es wird
   gelegentlich argumentiert, dass die bei der Produktion anfallenden
   CO₂-Emissionen auf das Konto derjenigen Städte, in denen die Produkte
   konsumiert werden, geschlagen werden sollten. Für Ravensburg sind
   diesbezüglich leider keine Zahlen bekannt, wohl aber für Deutschland als
   Ganzes. Diese Betrachtungsweise führt aufgrund der Vielzahl CO₂-intensiver
   Importe zu einem *geringeren Restbudget*.
2. Die Rechnung basiert auf dem IPCC-Report. *Dieser ist weichgewaschen.* Alle
   UN-Nationen mussten ihm zustimmen, auch etwa Saudi-Arabien. Viele
   europäische Studien kamen zum Schluss, dass das Welt-CO₂-Budget viel
   geringer ist als in dem Sonderbericht von 2018 angegeben. Es wird auch
   erwartet, dass der nächste IPCC-Report viel geringere Margen angeben wird.
3. Die Rechnung beginnt willkürlich mit 2020 als Referenzdatum. Wir könnten auch
   1850 als Referenz nehmen — denn für den Treibhauseffekt kommt es auf die
   Gesamtmenge CO₂ an. Dann kommt aber heraus: Deutschland überschritt sein
   Budget schon vor vielen Jahrzehnten. Das ist eine Erkenntnis, die nur zu Lähmung
   führt: Im Baumhausklimacamp haben wir die Position: Klar, in der Vergangenheit wurde
   deutlich zu wenig eingespart. Aber Vergangenes ist Vergangenes. Es ist
   wichtig, jetzt entschlossen zu handeln. Kritik an früherer Politik lenkt nur ab.
