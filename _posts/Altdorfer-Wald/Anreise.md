---
layout: page
title: Anreise
permalink: /altdorfer-wald/anreise/
parent: 🏕 Altdorfer Wald 🏕
nav_order: 10
---

# Anreise

🗺 **Koordinaten:** [47.810973, 9.76126](https://www.openstreetmap.org/node/8481053203)

🚉 Anreisebahnhof: Wolfegg (von dort 4,8 km zu Fuß oder [Buslinie 7534](https://www.bodo.de/fileadmin/redakteur/pdf/linien/ueberlandverkehr/7534.pdf), Haltestelle "Vogt Abzw. Grund im Wald")

🚕 Shuttle ab Ravensburg, Wolfegg oder Weingarten können wir organisieren, wenn der Bus
nicht fährt -- bitte dazu [Kontakt aufnehmen](https://t.me/altdorfer_wald_austausch).

💶 Wir können möglicherweise Fahrtkosten übernehmen.
