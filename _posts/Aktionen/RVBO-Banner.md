---
layout: page
title: Kundgebung mit Dachbanner beim RVBO
permalink: /aktionen/rvbo-banner/
nav_order: 5^
parent: Aktionen
---

# Kundgebung mit Dachbanner beim RVBO (6.2.2022)

<style>
  .carousel-container { position: relative; }
  .carousel { overflow-x: scroll; scroll-snap-type: x mandatory; scroll-behaviour: smooth; display: flex; }
  .carousel a { flex-shrink: 0; scroll-snap-align: start; }
</style>

<div class="carousel-container" style="padding-top: 1em">
  <div class="carousel">
    <img src="/kundgebung-2021-02-06.jpeg" width="450" height="449" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)">
    <img src="/rvbo-1.jpeg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-2.jpeg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-3.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-4.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-5.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-6.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-7.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-8.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-9.jpeg" width="1200" height="800" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
  </div>
</div>

**Regionalplan, was ist das?**
Er wird vom Regionalverband neu geschrieben, kaum ein Mensch weiß wer dieses Gremium ist. Unsere Umweltverbände und die Wissenschaft **Scientist for Future** warnen davor, warnen davor weil die Rechnung ohne den Klimawandel gemacht wurde. 
Kommt vorbei, hört zu, denn die Profis stellen am Samstag den **6.2.2021** einen **zukunftsfähigen Gegenentwurf** vor!!! **Coronakonforme Aktionen** und **Redebeiträge** dann um **11 und 14 Uhr** am **Regionalverband (Hirschgraben 2)** in Ravensburg.

Einladung zum Protest, gerne weiterleiten. Bitte denkt an **FFP2-Masken** und genügend **Abstand**.
 
Schreibt auch gerne in eine der verlinkten Mustereinwendungen noch was Persönliches/Fehlendes dazu, druckt es aus und bringt den Brief mit (oder schickt es per Mail an [info@rvbo.de](mailto:info@rvbo.de), wir wollen den Postkasten des RVBO füllen ;-) Hier gibt es weitere Mustereinwendungen: 
[allgemein](/mustereinwendung-1.docx)
[Klima](/mustereinwendung-2.docx)
[Gewerbe](/mustereinwendung-3.docx)
[Wohnraum](/mustereinwendung-4.docx)
[Ländle4Future](https://regionbodenseeoberschwaben.blogspot.com/search/label/Muster%20Einwendungen)
[altdorferwald.org](https://altdorferwald.org/133/informationen-veranstaltungen/2-offenlegung-regionalplan)
