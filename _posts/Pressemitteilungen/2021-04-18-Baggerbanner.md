---
layout: page
title:  "18.04.2021: Banner an Bagger der Kiesgrube Oberankenreute mahnt Kiesunternehmen und Politik"
date:   2021-04-18 12:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2104180
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 18. April 2021*

# Banner an Bagger der Kiesgrube Oberankenreute mahnt Kiesunternehmen und Politik

Am heutigen Sonntagmorgen (18.4.2021) brachten Klimagerechtigkeitsaktivist\*innen der
Altdorfer Waldbesetzung an einem Bagger der Kiesgrube Oberankenreute ein Banner
mit der Aufschrift "Betritt der Meichle Moor oder Wald, flutet der Kies sein
Konto bald" an. Damit beziehen sie sich auf das Kiesunternehmen Meichle+Mohr,
das Teile des Altdorfer Walds zur weiteren Kiesgewinnung roden möchte.

Wie eine Recherche der SPD-Kreistagsfraktion Ravensburg ergab, ist
Meichle+Mohr "nicht etwa ein kleiner regionaler Familienbetrieb oder ein
kleiner Mittelständler, sondern sie ist Teil eines Geflechtes von Kiesfirmen
mit 18 Standorten" [1]. Damit erklärt Aktivist Samuel Bosch (18) die Aktion:
"Unser Protest trifft einen Großkonzern, der seine Profitinteressen über das
Wohl der Anwohner\*innen stellt. Diese müssen unter dem erhöhten
Schwerlastverkehr und dem Entzug ihrer natürlichen Trinkwasserquellen leiden!"

Boschs Mitstreiterin Emma Stadlbauer (17) ergänzt: "Wenn der Wettbewerbsdruck
Firmen zur Umweltzerstörung antreibt, ist die Politik gefragt, solche Vorhaben
unrentabel zu machen. Die Landespolitik versagt seit Jahren, den Kiesexport mit
Zöllen zu belegen, und die Lokalpolitik versagt, den Altdorfer Wald zum
Ausschlussgebiet für den Kiesabbau erklären."

Für Stadlbauer ist auch die in Aussicht gestellte Ersatzaufforstung keine
Rechtfertigung: "Boden ist nicht einfach Dreck. Bodenbildung dauert
Jahrtausende! Meichle+Mohr und der Regionalverband haben keinen Respekt vor
diesem Prozess und kein Verständnis für den Wert von Waldboden."

[1] https://satiresenf.de/ts26-21-regionalplan-und-so-kiesabbau-grenis-ein-russischer-oligarch-putin-navalny-und-trump/


## Fotos zur freien Verwendung

Hier herunterladen: https://www.speicherleck.de/iblech/stuff/.oberankenreute1


## Hintergrund

Die Bagger in Obenankenreute tragen auf bereits gerodetem Gebiet Bodenschichten
ab. Der aktuelle Regionalplanentwurf ermöglicht darüber hinausgehende weitere
Rodung sowie Klima- und Umweltzerstörung anderer Art, etwa wird das größte
Straßenneubauprojekt der letzten Jahrzehnte angestrebt. Der Entwurf wird vom
Regionalverband Bodensee-Oberschwaben bestimmt, der von der CDU dominiert ist
-- obwohl in den Gemeinden und im Land die Grünen stärkste Kraft sind. Mehr zum
Regionalplanentwurf unter https://ravensburg.klimacamp.eu/regionalplan/ und
mehr zur Waldbesetzung unter https://ravensburg.klimacamp.eu/altdorfer-wald/.


## Kontakt

Ingo Blechschmidt (+49 176 95110311) stellt gerne den Kontakt zu den
Waldbesetzer\*innen her. Es gibt keine autorisierte Gruppe und kein
beschlussfähiges Gremium, das "offizielle Gruppenmeinungen" für die
Besetzung beschließen könnte. Die Menschen in der Besetzung und ihrem
Umfeld haben vielfältige und teils kontroverse Meinungen. Diese
Meinungsvielfalt soll nicht zensiert werden, sondern gleichberechtigt
nebeneinander stehen. Kein Text und keine Aktion spricht für die ganze
Besetzung oder wird notwendigerweise von der ganzen Besetzung gut geheißen.
