---
layout: page
title:  "28.10.2021: Aktivisten gehen in Berufung -- nach Verurteilung von Klimaaktivist
Samuel Bosch wegen Gründung der \"Alti\"-Besetzung"
date:   2021-10-28 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2110280
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 28.10.2021*

# Aktivisten gehen in Berufung -- nach Verurteilung von Klimaaktivist Samuel Bosch wegen Gründung der "Alti"-Besetzung

Am 28.10.2021 stand Samuel Bosch wegen zweier mutmaßlichen Hausfriedensbrüche
und Versammlungsgesetzesverstöße vor dem Amtsgericht Ravensburg. Es ging um vier
Aktionen und die Vorbereitung der Besetzung des Altdorfer Waldes. Die
vorgeworfenen Sachverhalte räumte der Aktivist ein. "Vor jeder Aktion
Informieren wir uns umfassend bei unseren Anwälten, um auszuschließen, dass wir
Straftaten begehen" so Bosch.

Die vorbereitende Errichtung der Plattformen im Altdorfer Wald stellten nach
Ansicht des Verteidigers keine anmeldepflichtige politische Versammlung dar,
sondern lediglich nicht-öffentliche Vorbereitung zur späteren Besetzung. Zum
Tatzeitpunkt gab es keinerlei öffentliche politische Meinungskundgabe und daher
auch keine Pflicht zur Anmeldung, so der Verteidiger.

In drei von fünf Fällen argumentiert die Verteidigung damit, dass es sich bei
Banneraktionen auf Dächern oder Bäumen nicht um eine öffentliche Versammlung
handeln könne, da keine Menschen spontan und unvorbereitet daran teilnehmen
könnten. "Der Sinn der Anmeldepflicht von Versammlungen ist es, dass die Polizei
die öffentliche Sicherheit und Ordnung bei sich ausweitenden Versammlungen
schützen kann. Ist die Teilnehmerzahl indes durch die besondere Form der
Versammlung beschränkt und können keine weiteren Menschen dazustoßen, ist die
Versammlung nicht öffentlich und muss daher auch nicht angemeldet werden" so
Rechtsanwalt Klaus Schulz.

Die Verteidigung sah sowohl im Betreten der unumzäunten Kiesgrube als auch beim
Klettern auf Dächer öffentlicher Gebäude kein Eindringen gemäß §123 StGB, also
keinen Hausfriedensbruch. 

Staatsanwalt Spieler hielt eine Geldstrafe von 120 Tagessätzen für angemessen,
während Rechtsanwalt Klaus Schulz einen Freispruch in allen Fällen forderte. Die
Richterin verurteilte Bosch zu 40 Sozialstunden nach Jugendstrafgesetz.

"Da wir weiterhin einen Freispruch in allen Punkten anstreben, gehen wir in
Berufung" so Rechtsanwalt Schulz. "Es ist bedauerlich, dass das Gericht sich
über die von uns dargelegte Rechtsprechung des OLG Köln und des
Bundesverfassungsgerichts hinweggesetzt hat." so Schulz weiter.

Kurz vor der öffentlichen Verhandlung fand eine solidarische Mahnwache vor dem
Amtsgericht statt. Viele der Unterstützer\*innen zeigten sich enttäucht über den
"Mangel an Zuschauerplätze im Gerichtssaal". Ulla Köberle-Lang: "Wenn eine
Verhandlung öffentlich ist und ein so großes interresse besteht, müssen im
Gerrichtssaal mehr Plätze bereitgestellt werden".

Manfred Scheurenbrand (66) aus Waldburg unterstützt die Mahnwache vor dem
Amtsgericht und kam leider nicht in den Gerrichtssaal: "Vor Gericht sollten die
Umweltbrecher stehen, nicht die jungen Menschen, die mit friedlichem zivilen
Ungehorsam auf Missstände hinweisen."

## Hinweise zu den Einzelvorwürfen

Die Einzelvorwürfe lauten:
- Beginn des Baumhauscamps im Altdorfer Wald, Vorwurf: Versammlungsgesetz (§26 VersG)
- "Stoppt den Klimahöllenplan"-Banner in Amtzell, Vorwurf: Hausfriedensbruch (§123 StGB) & (§26 VersG)
- Banner an Bagger der Kiesgrube Tullius bei Oberankrenreute, Vorwurf: Hausfriedensbruch (§123 StGB)
- Baumbesetzung in der Bachstraße, Vorwurf: Versammlungsgesetz (§26 VersG)
- Banner mit Prof. Dr. Ertel an der RWU in Weingarten, Vorwurf: Versammlungsgesetz (§26 VersG)

-----

1. Die Aktivist\*innen besetzen seit Februar ein bedrohtes Waldstück im
Altdorfer Wald um die geplante Rodung für den Kießabbau zu verhindern.
[Artikel zur Aktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-ein-zweiter-hambacher-forst-aktivisten-besetzen-den-altdorfer-wald-_arid,11333867.html)

2. Die Aktivist\*innen kletterten auf die Mehrzweckhalle in Amtzell um
dort mit einem Banner ("Stoppt den Klimahöllenplan") gegen den aus ihrer
Sicht nicht zukunftsfähigen Regionalplanentwurf zu protestieren.
Währenddessen stimmte der Gemeinderat-Amtzell in der Mehrzweckhalle über
den Entwurf ab.
Dieselbe Form von Aktion wurde einige Tage später auch bei der
entsprechenden Sitzung des Gemeinderats in Weingarten durchgeführt.
[Artikel zur Aktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/amtzell_artikel,-regionalplan-sorgt-fuer-unmut-bei-klimaaktivisten-_arid,11332447.html)

3. Die Aktion an der Hochschule stieß den Prozess an, ungenutzte Hörsäle
im Winter nicht unnötig zu beheizen.Das hatte zuvor Prof. Dr. Wolfgang
Ertel über zehn Jahre lang erfolglos versucht.
[Artikel zur Aktion](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-professor-protestiert-mit-baumbesetzern-an-hochschule-_arid,11362406.html)

1. Bei der Baumbesetzung in der Bachstraße wird den Aktivist\*innen
vorgeworfen, Vögel so sehr gestört zu haben, dass sie ihre Brut nicht
mehr versorgten. "Tatsächlich zeigten sich die Vögel von unserer
Hängematte sichtlich unbeeindruckt und flogen immer wieder ein uns aus",
so Bosch. Ein von einem Anwohner am 18. Maiaufgenommenes Video belegt,
dass sich die Vögel auch nach der Aktion weiterhin ihre Brut versorgten
und normal verhielten.
[Artikel zur Aktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-besetzen-erneut-baum-in-ravensburg-_arid,11363667.html)

5. Bei der Aktion in der Kiesgrube handelt es sich aus Sicht der
Aktivist\*innen nicht um Hausfriedensbruch. Ihrer Ansicht nach ist das
Betreten von offenem Gelände ist erlaubt und das Entfernen des Banners
durch die Arbeiter\*innen der Kiesgrube dauerte wahrscheinlich nur wenige
Sekunden. "Warum werden überhaupt Aktionen verfolgt, die niemanden
stören und bei denen niemand zu Schaden kommt?" so Bosch zu den
Vorwürfen.
[Artikel zur Aktion](https://wochenblatt-online.de/banner-an-bagger-der-kiesgrube-oberankenreute-mahnt-kiesunternehmen-und-politik/)