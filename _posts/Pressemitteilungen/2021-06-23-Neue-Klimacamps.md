---
layout: page
title:  "23.06.2021: Neue Klimacamps in Ravensburg und Pfullendorf"
date:   2021-06-23 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106230
#          YYMMDDX
---

*Kurzmeldung des Ravensburger Klimacamps am 23.6.2021*

# Neue Klimacamps in Ravensburg und Pfullendorf

Bevor am Freitag (25.6.2021) um 9:00 Uhr in der Pfullendorfer Stadthalle die
Regionalverbandsversammlung zusammenkommt, um den in den letzten Monaten immer
wieder diskutieren umstrittenen Regionalplanentwurf zu verabschieden, werden von
der Gruppe "Ravensburger Klimacamp" noch mal mehrere Veranstaltungen
organisiert:

1. Demonstrationszug für einen zukunftsfähigen Regionalplan. Start und Ende vor
der Stadthalle in Pfullendorf (Jakobsweg 1, 88630 Pfullendorf), 24.6.2021, 15:00
Uhr bis 17:00 Uhr.

2. Zweitägiges durchgehendes Klimacamp in Pfullendorf vor der Stadthalle
(Jakobsweg 1, 88630 Pfullendorf), 24.6. 14:00 Uhr bis 25.6. 19:00 Uhr, nach dem
Vorbild des dreitägigen Klimacamps in Horgenzell.

3. Durchgehendes Klimacamp in Ravensburg vor dem Sitz des Regionalverbands
(Hirschgraben 2, 88214 Ravensburg) mit offenem Ende nach dem Vorbild des
Augsburger Klimacamps, welches seit knapp einem Jahr besteht -- "Protest direkt
an den Ort der verfehlten Planung tragen" -- "Raum für Debatten und Austausch
zur Erarbeitung neuer Lösungen und Ideen", Beginn 24.6. 14:00 Uhr.

4. Vorträge in der Altdorfer Waldbesetzung von Cécile Lecomte, einer in
Deutschland lebenden französische Umweltaktivistin, die sich insbesondere
in der Antiatomkraftbewegung und obwohl sie auf einen Rollstuhl
angewiesen mit diversen Kletteraktionen engagierte und in der
Kreativaktivistiszene international als "Das Eichhörnchen" bekannt ist:
    1. Offene Austauschrunde, 23.6. 15:00 Uhr
    2. Kletter-Workshop für Menschen mit körperlichen Einschränkungen, 23.6. 17:00 Uhr
    3. Lesung aus ihrem Buch "Kommen Sie da runter!", 23.6. 19:00 Uhr

Begleitend wird es Aktionen des zivilen Ungehorsams geben.

Schüler Samuel Bosch (18): "Am Freitag wird eine die reale Bevölkerung überhaupt
nicht abbildende Scheinmehrheit aus den 49 Männern und 7 Frauen der
Verbandssammlung die Rodung von zahlreichen Wäldern, das größte
Straßenneubauprojekt der letzten Jahrzehnte sowie Flächenversiegelung in nie
gekanntem Ausmaß beschließen. Auch wenn die Verbandsversammlung all diese
Projekte in grober Missachtung des wissenschaftlichen Sachstands zur Klimakrise
durchwinken wird: Sie sollte nicht glauben, dass ihre Beschlüsse jemals
umgesetzt werden können. Wir werden uns friedlich, aber mit aller Kraft, gegen
jede einzelne Baustelle stellen. Nicht weil wir wollen, sondern weil wir müssen.
Wandel wird kommen!"