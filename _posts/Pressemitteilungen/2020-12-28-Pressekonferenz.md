---
layout: page
title:  "28.12.2020: Einladung zur bündnisübergreifenden Pressekonferenz am 30.12.2020 um 11:00 Uhr"
date:   2020-12-28 14:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2012280
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 28. Dezember 2020*

# Einladung zur bündnisübergreifenden Pressekonferenz am 30.12.2020 um 11:00 Uhr

Von Dienstag auf Mittwoch übernachtet der stadtbekannte Prof. Dr. Wolfgang
Ertel auf dem Baumhaus. "Das ist eine ganz tolle und mutige Aktion, die die
Unterstützung aller Bürger verdient, die mehr Klimaschutz wollen", ließ Ertel
verlauten.

Im Anschluss veranstalten wir am Mittwoch (30.12.) um 11:00 Uhr am Fuße des
Baumhausklimacamps eine bündnisübergreifende Pressekonferenz, zu der wir Sie
herzlich einladen.

## Veranstalter\*innen

* Prof. Dr. Wolfgang Ertel (Scientists for Future Ravensburg)
* Samuel Bosch (Baumhausklimacamp)
* Sabine Buchmann-Mayer (Parents for Future Ravensburg)
* Smilla (Fridays for Future Ravensburg)
* Jonathan Oremek, Elgin Raupach (Fridays for Future Bodensee)
* Alexander Knor, Jens Erleke, Helmut Fimpel (Natur- und Kulturlandschaft Altdorfer Wald e.V.)
* Barbara Herzig (BUND Naturschutz)
* Manfred Walser (Regionalplan)
* Gabriel Brauchle (Aktionsbündnis Autofreie Innenstadt)
* Wolfgang Frommlet (Initiative gegen 1.000-Kühe-Stall)

## Themen

* Zukunft des Baumhauses und Reaktion der Stadt auf Traverse über Obere Breite Straße
* Bericht über Klimaschutzaktivitäten der Stadt
* Position von Professor Ertel
* Stellungnahme der verschiedenen Initiativen
* Und vor allem: all Ihre Fragen

## Hinweis zur Traverse

Wie angekündigt, wurde gestern verkehrssicher eine Traverse über die
Grüner-Turm-Straße gespannt und in 12 Meter Höhe ein Banner mit der Aufschrift
"1,5 Grad" angebracht. Fotos zur freien Verwendung gibt es auf
ravensburg.klimacamp.eu. Ein Ultimatum der Polizei, das Banner bis gestern um
14:00 Uhr zu entfernen, ließen die Klimagerechtigkeitsaktivist\*innen
verstreichen. Den Schüler\*innen wurde ein Übergriff durch das
Sondereinsatzkommando in Aussicht gestellt.

"Das Banner ist verkehrssicher befestigt, befindet sich weit oberhalb des
Lichtraums der Straße und behindert Autofahrer\*innen in keinster Weise",
erklärt Klimacamper Samuel Bosch (17). "Das Banner richtet sich ja auch nicht
an die Menschen, die derzeit zum Autofahren genötigt werden, sondern an die
Regierung, die die Mobilitätswende nicht angeht. Offenbar dürfen
systemkritische Banner aber nur da aufgehängt werden, wo sie nicht zu sehen
sind."
