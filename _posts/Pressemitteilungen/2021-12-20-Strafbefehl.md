---
layout: page
title:  "20.12.2021: Staatsanwaltschaft baut Kriminalisierung von Klimaprotest aus: 2.000 Euro Bußgeld für unbeteiligten Passant"
date:   2021-12-20 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2112200
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 20.Dezember.2021*

# Staatsanwaltschaft baut Kriminalisierung von Klimaprotest aus: 2.000 Euro Bußgeld für unbeteiligten Passant

Am 22. Februar 2021 kletterten zwei Aktivist*innen des Ravesburger Klimacamps
für eine halbe Stunde auf das Dach der Amtzeller Mehrzweckhalle, um dort mit
einem Banner für eine klimafreundliche Überarbeitung des Regionalplanentwurfs zu
demonstrieren, der an diesem Tag Sitzungsgegenstand war (Zeitungsartikel [[1]],
unten die Pressemitteilung von damals).

Nach Aussage der Klimacamper\*innen erntete die Aktion damals regen Zuspruch von
Passant\*innen. Einer von ihnen, ein über 60-Jähriger Familienvater aus Amtzell,
brachte den beiden jungen Demonstrierenden kurzerhand eine kleine Brotzeit
vorbei, bevor er selbst die Sitzung verfolgte.

Nun wurde den Aktivist\*innen bekannt, dass dieser Passant nach Sitzungsende
(und lange nach Aktionsende) von der Polizei nach Hause verfolgt wurde und am 7.
Dezember 2021 einen Strafbefehl der Staatsanwaltschaft erhielt: 40 Tage Haft
oder alternativ 2.000 Euro Geldstrafe.

"Das ist es, was wir meinen, wenn wir von Kriminalisierung legitimen Protests
sprechen", erklärt Samuel Bosch (18), gegen den zahlreiche Gerichtsverfahren
anhängig sind. "Auch unbeteiligte Anwohner\*innen werden kriminalisiert. Wo soll
das aufhören?", fragt Bosch. Der betroffene Anwohner erhält nun Unterstützung
von Klimacamp-Rechtsanwalt Klaus Schulz und dem restlichen Team. "Wir lassen
niemanden mit Repression alleine", so Bosch.

**Hinweis** \
Es kam schon früher vor, dass unbeteiligte Personen einen
staatsanwältlichen Strafbefehl erhielten, allerdings ist es das erste
Mal, dass es einen über 60-jährigen Bürger trifft.

## Quellen
[1]: https://www.wochenblatt-news.de/klimaaktivisten-hissen-banner-im-amtzell/
[1] [https://www.wochenblatt-news.de/klimaaktivisten-hissen-banner-im-amtzell/][1]