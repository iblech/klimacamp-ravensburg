---
layout: page
title:  "11.06.2021: Klimacamper*innen wehren sich gegen RAF-Vergleich von SPD-Bundestagskandidatin Heike Engelhardt"
date:   2021-06-11 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106110
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 11.6.2021*

# Klimacamper\*innen wehren sich gegen RAF-Vergleich von SPD-Bundestagskandidatin Heike Engelhardt

In der Nacht von Donnerstag (10.6.2021) auf Freitag (11.6.2021) brachten
Aktivist\*innen des Ravensburger Klimacamps ein Banner am Büro der
SPD-Bundestagskandidatin Heike Engelhardt mit der Aufschrift "KLIMAGERECHTIGKEIT
für RAFensburg -- Banner statt Bomben -- Klimacamp RAFensburg" an. Zuvor hatte
Engelhardt die friedlichen Aktionen der jungen Menschen des Klimacamps mit ihrer
Stellungnahme "Die Sprache erinnert mich an RAF-Zeiten" [[1]] in die inhaltliche
Nähe der RAF gerückt.

"Frau Engelhardt lenkt mit seltsamen Vergleichen vom eigentlichen Thema ab",
erklärt Klimacamper Samuel Bosch (18) die Aktion. Er verweist darauf, dass seit
Gründung des Klimacamps am 12.12.2020 alle Aktionen stets friedlich abliefen und
nur symbolischer passiver Widerstand geleistet wurde.

Ravensburgs Klimacamper\*innen fordern Klimagerechtigkeit und eine sozial
gerechte Mobilitätswende. "Wir möchten nicht, dass die Stadt zur
CO₂-Kompensation 10 Millionen Euro Strafe zahlt [[2]], sondern das Geld lieber
in eine soziale Umgestaltung unseres Verkehrswesens investiert", so Boschs
Mitstreiterin Rosina Kaltenhauser (17). Damit seien unter anderem ein
kostenloser und ausgebauter Nahverkehr sowie ein sicheres und durchgängiges
Radwegenetz auch für ältere und sehr junge Menschen, die auf dem Rad weniger
sicher sind als fitte Sportler\*innen, gemeint.

"Wir machen auf die Mobilitätswende aufmerksam, da die lieblos angegangene
Verkehrspolitik jeden Tag billigend neun Verkehrstote in Deutschland in Kauf
nimmt, die Klimakrise befeuert und Bürger zum Kauf und teuren Unterhalt von
Autos nötigt. Die SPD in Ravensburg hat viele kluge Ideen, ihre Stadträtin und
Bundestagskandidatin Heike Engelhardt ist aber eine denkbar schlechte
Vertreterin. Machen, nicht ablenken!", so Bosch.

## Quellen
[1]: https://wochenblatt-online.de/stellungnahme-der-bundestagskandidatin-und-stadtraetin-heike-engelhardt-zu-den-juengsten-unternehmungen-der-klimaaktivistinnen/
[1] [https://wochenblatt-online.de/stellungnahme-der-bundestagskandidatin-und-stadtraetin-heike-engelhardt-zu-den-juengsten-unternehmungen-der-klimaaktivistinnen/][1]
 
[2]: http://hs-weingarten.de/~ertel/s4f-stellungnahme-rkk.pdf
[2] [http://hs-weingarten.de/~ertel/s4f-stellungnahme-rkk.pdf][2]

## Hinweis
Adresse des Büros von Frau Engelhardt, wo das Banner befestigt ist: \
Spohnstr. 9/1, 88212 Ravensburg. Genaue Koordinaten: 47.784021,9.618179
