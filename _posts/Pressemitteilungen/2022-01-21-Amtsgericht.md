---
layout: page
title:  "21.01.2022: Fortsetzung des Prozesses: Unbeteiligte Passantin wegen Klima-Aktion vor dem Ravensburger Amtsgericht"
date:   2022-01-21 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2201210
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 21.01.2022*

# Fortsetzung des Prozesses: Unbeteiligte Passantin wegen Klima-Aktion vor dem Ravensburger Amtsgericht

Am 21.01.2022 findet vor dem Amtsgericht Ravensburg die Weiterführung des ersten
Strafprozesses zum Banner über der Schussenstraße und dem "Straßenfest" im
Frühjahr 2021 statt. Die erste Verhandlung zur gleichen Sache und gegen
dieselben Personen hat bereits am 13.01.2022 stattgefunden. Hierbei wurde
festgestellt, dass weitere Zeugen und Beweismittel erforderlich sind, um
einwandfrei die Anwesenheit der angeklagten Personen zu beweisen. Angeklagt sind
eine Passantin und eine Person, die sich am "Straßenfest" beteiligte.

## Was geschah am 15.05.2021?
Am Morgen des 15.05.2021 spannten drei junge Aktivist\*innen acht Meter über der
Schussenstraße ein Banner mit der Aufschrift "Wer Straßen sät, wird Stau
ernten", um gegen die "tosende Autostraße" im Zentrum Ravensburgs zu
demonstrieren. Hierbei platzierten sie das Banner für alle
Verkehrsteilnehmer\*innen gut sichtbar direkt über der vierspurigen Straße.
Anschließend sperrte die Polizei die Straße für alle Autos. Nur noch Busse und
Rettungsfahrzeuge durften fahren. "Leider sahen durch die Sperrung der Straße
viel weniger Menschen das Banner, als wir gehofft hatten", so die angeklagte
Aktivistin "Kolibri" zur Aktion.

Daraufhin entwickelte sich die Aktion zu einem bunten Straßenfest. "Es war ein
toller Nachmittag. Aktivist\*innen  und Passant\*innen spielten Ball und
musizierten", erinnert sich "Kolibri". "Wir führten viele Gespräche mit
Anwohner\*innen und Passant\*innen. Viele fanden die Aktion und die fröhliche
Stimmung sehr schön", so Aktivist Dr. Ingo Blechschmidt. Ein älterer Anwohner
wiederholte immer wieder "Es ist so leise hier, so leise!", erzählt Blechschmidt
weiter. Die Aktion wurde am Nachmittag von der Polizei aufgelöst. Die
Aktivist\*innen, welche morgens das Banner aufgehängt hatten, wurden anschließend
für 24 Stunden nach Friedrichshafen in Gewahrsam gebracht.

Die Aktion reihte sich in eine Vielzahl anderer Aktionen zum Thema
"Mobilitätswende" ein. Samuel Bosch (19) erklärt: "Unser Ziel ist eine sozial
gerechte Mobilitätswende für Ravensburg." Jahrelang haben verschiedene Gruppen
und Initiativen "viele konkrete Vorschläge" an die Stadt Ravensburg gemacht.
"Jetzt ist die Stadt am Zug.", so Bosch weiter.  

"Der ÖPNV muss sozial gerecht ausgebaut werden. Bis 1959 gab es beispielsweise
eine Straßenbahn, von Ravensburg bis nach Baienfurt! Nun ist es an der Zeit,
diese wieder aufzubauen und in Betrieb zu nehmen." sagt Samuel Bosch (19). Die
Aktivist\*innen haben nicht das Ziel,  Privatpersonen Autos zu verbieten.
Vielmehr müsse die Politik "dringendst" in umweltverträgliche Alternativen
investieren.

## Zur Verhandlung und den Vorwürfen:
Die Aktivistin und die Passantin leiten mit ihren Strafprozessen eine ganze
Reihe weiterer Prozesse zur Aktion auf der Schussenstraße ein. Bei beiden lautet
der Vorwurf Nötigung (§240 StGB). Aus ihrer Sicht haben sie aber keine Nötigung
begangen. "Sollte es zu einer Verurteilung kommen, ist es keine Niederlage für
mich, sondern eine Niederlage für den Rechtsstaat, der unbeteiligte
Passant\*innen vor Gericht stellt", so die angeklagte Passantin, die ihren vollen
Namen nicht in der Zeitung lesen möchte und den Spitzname "Merle" trägt. Die
angeklagte Aktivistin "Kolibri" habe, als das Banner über der Straße entrollt
wurde, noch in Ruhe geschlafen. "Der Vorwurf, ich wäre zu dieser Zeit an der
Aktion beteiligt gewesen, ist lächerlich!", so "Kolibri" zu den Vorwürfen. Die
Anwält\*innen der Angeklagten rechnen dementsprechend mit einem Freispruch.

Parallel zum Prozess veranstalten Unterstützer\*innen, wie am 13.1. auch, eine
Mahnwache mit dem Motto "Klimagerechtigkeit statt Kriminalisierung – Friedlicher
Aktivismus bedarf keiner Strafe!". Die Mahnwache findet am Freitag, den
21.1.2022, von 7:45 bis 9:00 Uhr vor dem Amtsgericht Ravensburg, Herrenstraße
40-44, statt. Der Verhandlungsbeginn: 8:30 Uhr.