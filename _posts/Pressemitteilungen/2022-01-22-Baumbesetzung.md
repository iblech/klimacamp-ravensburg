---
layout: page
title:  "22.01.2022: Klimaaktivist*innen wollen Stadt mit erneuter dezentraler Baumbesetzung zum Handeln bewegen"
date:   2022-01-22 11:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2201220
#          YYMMDDX
---

*Pressemitteilung vom 22. Januar 2022*

# Klimaaktivist\*innen wollen Stadt mit erneuter dezentraler Baumbesetzung zum Handeln bewegen

Sperrfrist Samstag (22.01.2022) 11:00 Uhr
{: .label .label-red }

**EINLADUNG:** \
*Sehr geehrte Pressevertreter\*innen, \
hiermit laden wir Sie herzlich zu unserer heutigen Aktion ein. \
Besuchen sie uns gerne ab 11 Uhr an unseren Bäumen im Herzen Ravensburgs. \
Weitere Informationen und die genauen Standorte der Bäume finden sie in unser Pressemitteilung unten. \
Mit freundlichen Grüßen \
die Baumbesetzung*

---------------

Am heutigen Samstag (22.01.2022) besetzen rund ein Dutzent Klimaaktivist\*innen
erneut Bäume in der  Ravensburger Innenstadt. Hierbei befinden sich die Personen
auf je einem Baum. Aus Sicht der Aktivist\*innen bewegt sich Ravensburg im
"Schneckentempo" auf die selbstgesteckten Ziele zu. "Es ist enttäuschend, dass
der Ravensburger Gemeinderat und die Stadtregierung den Ernst der
Klimakatastrophe scheinbar immernoch nicht verstanden haben" so Aktivist\*in
Charlie Kiehne (19). Bereits Mitte Dezember 2021 hatte eine gleich gelagerte
Aktion in Ravenburg für Aufsehen gesorgt. "Bei unserer letzten Aktion haben wir
viel Zuspruch erhalten, darum hoffen wir diesmal nicht weiter von der Politik
ignoriert zu werden" so Kiehne weiter. 

Nach der Aktion am 11.12.2021 wurden zahlreiche Ermittlungsverfahren wegen
Verstößen gegen das Versammlungsgesetz eingeleitet. Die Kletterer\*innen sollen
alle eine unangemeldete Versammlung geleitet haben. Rechtsanwalt und Aktivist
Klaus Schulz zu den Vorwurfen: "Wo jetzt da eine Versammlung gewesen sein soll,
ist mir unerfindlich, zumal Versammlung ja immer das physische Zusammensein
mehrerer Personen voraussetzt." Aktivist Samuel Bosch (19): "Das ist es wenn wir
von Kriminalisierung berichten. Wir demostrieren friedlich und ohne irgendwen zu
stören für mehr Klimamaßnahmen und trotzdem werden Verfahren gegen uns
eingeleitet." Er empfindet die eingeleiteten Verfahren als einen Versuch der
"Einschüchterung" welche den Protest delegitimieren soll. "Wir befinden uns auf
direktem Weg in eine exestenzielle Krise, da haben wir keine Zeit für solche
Spielchen. Wir müssen jetzt handen, sonst ist es zu spät." 

"Keiner unsererer Vorschläge bedarf Hexerei, wir fordern lediglich das Einlösen
längst gegebener Versprechen" so Hannah Schak (21). "Hier in Ravensburg hat der
Gemeinderat sich selbst mit der einstimmigen Verabschiedung des Klimakonsenses
Ziele gesteckt. Bei einer Politik des "weiter so" wie sie aktuell verfolgt wird,
werden die eigenen Klimaziele krachend verfehlt werden. Ich wünsche mir einen
Gemeinderat der glaubwürdig ist und sich an gegebene Versprechen hält" so Schaks
Mitstreiterin Charlie Kiehne  (19). 

"Wir alle engagieren uns aus verschiedensten Gründen. Einig sind wir uns aber
alle, dass die bisherigen Bemühungen der Stadt sehr zu wünschen übrig lassen.
Deshalb steigen wir auch diese Jahr, trotz Minusgraden wieder, auf die Bäume."
so Familienvater Martin Lang (55).

## Koordinaten der Bäume und der Pressesprecher\*innen
Positionen der Bäume:
- Charlie Kiehne (19): vor dem Rathaus `47.7816226723829, 9.613728004997174`
- Martin Lang (55): `47.781822686643395`, `9.612827857697898`
- Samuel Bosch (18): vor der „Nordsee“ `47.782208382829815, 9.613235553466874`
- Prof. Dr. Wolfgang Ertel: `47.78346398004137, 9.614867763055159`
- Maraike Siebert (23): hat es leider nicht geschafft
- Kia: vor dem Schadbrunnen
- Hannah Schak (21): `47.78122791589326, 9.608930608002876`
- Alfa Müller: vor dem Blaserturm 
- Sebastian Winkler: Bei Kornhaus
- Klaus Schulz: Spricht am Boden mit der Presse

8 von 9 Menschen konnten wie geplant auf ihren Bäume steigen. 
Die Aktivist*innen waren wie geplant von 10:30 bis 13:00 Uhr auf den Bäumen.

## Hinweise zu den Pressesprecher\*innen
Charlie Kiehne (19) ist Klimaaktivistin und erfahrene Kletterin. Im vergangenen
Jahr war sie unter anderem im Zuge von politischen Aktionen auf dem
Brandenburger Tor in Berlin und auf der Basilika in Weingarten um für
Klimagerechtigkeit zu kämpfen. 

Samuel Bosch (19) ist der stadtbekannte Klimaaktivist der unter anderem das
erste Baumhaus in Ravensburg und die Besetzung im Altdorfer Wald mitbegründet
hat sowie bei vielen (Banner-)Aktionen in der Region und bundesweit beteiligt
war.

Martin Lang (55) unterstützte zunächst als Anwohner die Aktivist\*innen im
Altdorfer Wald, macht mittlerweile aber auch selbst bei Kletteraktionen mit, wie
beispielsweise im September in Weingarten auf der Basilika.

Maraike Siebert (23) ist Mutter aus Ravensburg und engagiert sich im Rahmen von
verschiedenen Aktionen für Klimagerechtigkeit in ihrer Heimatstadt.

Hannah Schak (21) lebte mehrere Monate in der Waldbesetzung im Altdorfer Wald um
sich so und mit verschiedenen Aktionen gegen die Rodung durch den Kiesabbau zu
engagieren.