---
layout: page
title:  "24.06.2021: Nach Vereinbarungsbruch: Stadt blitzt vor Gericht ab"
date:   2021-06-24 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106240
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 24.6.2021*

# Nach Vereinbarungsbruch: Stadt blitzt vor Gericht ab

Das Verwaltungsgericht Sigmaringen bestätigte am heutigen Donnerstag in einem
Eilurteil, dass mündliche Vereinbarungen in einem der Errichtung des Klimacamps
vor dem Sitz des Regionalverbands zuvorgegangenen Kooperationsgespräch zwischen
der Stadt Ravensburg und Versammlungsanmelder Samuel Bosch (18) wieder von der
Stadt beachtet werden müssen. Anders als im Kooperationsgespräch vereinbart
hatte die Stadt die Auflage erlassen, dass im Klimacamp nicht auf Bäume
geklettert und nicht geschlafen werden dürfe. Diese Auflagen wurden vom
Verwaltungsgericht nun als rechtswidrig kassiert.

Seit dem heutigen Donnerstag besteht in Ravensburg vor dem Sitz des
Regionalverbands (Hirschgraben 2, 88214 Ravensburg) unter den Mottos "Protest
direkt an den Ort der verfehlten Planung tragen" und "Raum für Debatten und
Austausch zur Erarbeitung neuer Lösungen und Ideen öffnen" ein Klimacamp mit
offenem Ende. Das Klimacamp adressiert den Regionalverband, der nach Auffassung
der Klimaaktivist\*innen einen "Klimahöllenplan" als Regionalplan ausarbeitete,
sowie die Stadt Ravensburg mit ihrem Oberbürgermeister Rapp, der seinen Einfluss
als stellvertretender Verbandsvorsitzender hätte nutzen können, um den
Regionalplanentwurf in Einklang mit dem Grundrecht der jungen Generation auf
intakte Lebensgrundlagen zu bringen.

Im zuvorgehenden Kooperationsgespräch vereinbarten Stadt und
Versammlungsanmelder Samuel Bosch (18), dass wie in anderen Klimacamps in
Deutschland und ähnlich gelagerten Dauermahnwachen zu anderen Themen auch
Teilnehmer\*innen schlafen sowie zur Visualisierung ihres Protests auch Bäume auf
der Versammlungsfläche beklettern dürfen.

Im schriftlich eingegangenen Auflagenbescheid wurden dann aber trotz dieser
Vereinbarungen Schlafen und Baumbekletterung untersagt. Gegen diese Auflagen
legten die Klimacamper\*innen Beschwerde vor dem Verwaltungsgericht Sigmaringen
ein und erhielten noch am selben Tag Recht.

"Die Stadt Ravensburg sollte ihre Energie lieber in Klimagerechtigkeit als in
rechtliche Auseinandersetzungen mit uns stecken, die sie vor Gericht dann eh
verliert", kommentiert Versammlungsanmelder Samuel Bosch (18) die Entwicklung.
"Von drei Eilklagen am Verwaltungsgericht haben wir nun zwei gewonnen." (Die
andere gewonnene Eilklage bezog sich auf die Fahrraddemonstration über die A96,
die verlorene Eilklage auf eine Versammlung auf der Schussenstraße.)

## ORT
Hirschgraben 2, 88214 Ravensburg