---
layout: page
title:  "15.01.2021: Bericht zum gestrigen Gespräch mit der Stadtspitze"
date:   2021-01-15 08:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2101150
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 15. Januar 2021*

# Bericht zum gestrigen Gespräch mit der Stadtspitze

Am gestrigen Donnerstagabend kam es zum Gespräch zwischen Vertreter\*innen des
Ravensburger Baumhausklimacamps und der Ravensburger Stadtspitze, bei dem unter
anderem Oberbürgermeister Rapp sowie die Bürgermeister Blümcke und Bastin
anwesend waren.

"Im Gespräch gab es anders als erwartet einen echten inhaltlichen Austausch",
freut sich Klimacamper Samuel Bosch (18). "Wir hatten befürchtet, dass die
Stadtspitze die gesamte Zeit über nur unsere Aktionsform angreifen würde."

"Insgesamt prägte die Stadtspitze das Gespräch durch Hinweise auf viele
Einzelmaßnahmen, die in Sachen Klimaschutz schon ergriffen wurden, und verwies
immer wieder auf andere Kommunen, die diesbezüglich noch schlechter aufgestellt
sind als wir", erklärt Aktivistin Emma Junker (17). "Dass andere Städte in
Klimaschutz eine 6 bekommen, heißt nicht, dass wir uns auf unserer 4 ausruhen
dürfen. Note 4 in Klimaschutz ist ungenügend und verstößt gegen das
demokratisch beschlossene Pariser Klimaabkommen!"

Wie in fast allen Städten Deutschlands ist zwar in Ravensburg ein Rückgang der
CO2-Emissionen feststellbar. "Ravensburg befindet sich aber nicht auf einem
Reduktionspfad, der mit den eigenen Klimazielen kompatibel ist", erklärt
Professor Wolfgang Ertel, der die Schüler\*innen beim Gespräch unterstützte.
"Der letzten Juli im Gemeinderat einstimmig beschlossene 'Klimakonsens' ist
gut, doch nun versäumt es die Stadt, Maßnahmen zu ergreifen, um ihre Ziele
einzuhalten!" Nicht einmal der Klimarat, der die Umsetzung des Klimakonsens
begleiten sollte, wurde bisher von der Stadt eingesetzt.

"Wir schätzen die bisherige Klimaschutzarbeit unserer Stadtverwaltung sehr",
erklärt Klimacamperin Nele Kirn (17). "Nun ist aber die Politik gefragt, um die
großen Stellschrauben anzugehen: den ÖPNV deutlich ausbauen und vergünstigen,
die Innenstadt auf Fußgänger\*innen und Radler\*innen neu ausrichten, den auf
Wachstum ausgelegten Regionalplan grundüberarbeiten und vieles andere mehr.
Nur so sind Ravensburg und Ravensburgs Wirtschaft für die Zukunft gut
aufgestellt".

"Im Gespräch offenbarte die Stadtspitze auch Unverständnis und mangelnde
Weitsichtigkeit", gibt Rosina Kaltenhauser (17) zu Protokoll. Etwa deutete
Bürgermeister Bastin an, dass unsere Baumbesetzung den Bäumen angesichts der
schweren Hitzesommer nicht helfen würde. "Dabei sind Aktionen wie unsere das
letzte Mittel, um die Politik dazu zu bewegen, die Arbeit gegen die Hitzesommer
aufzunehmen und das Pariser Klimaabkommen einzuhalten." Als es im Gespräch um den
Erhalt des Altdorfer Walds ging, erklärte Oberbürgermeister Rapp, dass der dort
abgebaute Kies doch auch für die neuen Radwege benötigt würde, welche die
Klimacamper\*innen fordern. "Dabei wünschen wir uns ja eine mit dem Ausbau und
der Vergünstigung des ÖPNV einhergehende planvolle Umwidmung von Auto- in
Fahrradstraßen", so Kaltenhauser weiter. "Außerdem exportieren wir den Großteil
des in Deutschland gewonnenen Kies nach Österreich und die Schweiz, wo stärkere
Umweltauflagen herrschen."


## Wie es weiter geht

Weitere inhaltliche Gespräche mit der Stadt sollen folgen. Bürgermeister
Blümcke deutete an, dass eine Dauerversammlung am Boden akzeptiert werden
würde. "Die Versammlungsfreiheit schützt aber natürlich alle Versammlungen,
auch die in zehn Meter Höhe", erwiderte Bosch. "Wir werden unser
Baumhausklimacamp auf jeden Fall fortsetzen."

"Versammlungen und ziviler Ungehorsam sind seit jeher feste Elemente
einer gelebten Demokratie", konterte gestern Kaltenhauser. Sie bezog sich dabei
auf eine Erklärung von August Schuler (CDU, langjähriger Gemeinderat und
Landtagsabgeordneter), der die Aktion der Klimagerechtigkeitsaktivist\*innen für
jenseits von Demokratie befindlich erklärte.
