---
layout: page
title:  "14.05.2021: Nach Ablauf des Ultimatums: zeitlich unbefristetes Innenstadtbaumhaus neu errichtet"
date:   2021-05-14 08:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2105140
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 14. Mai 2021*

# Nach Ablauf des Ultimatums: zeitlich unbefristetes Innenstadtbaumhaus neu errichtet

Klimagerechtigkeitsaktivist\*innen der Gruppierung "Ravensburger Klimacamp"
errichteten am heutigen Freitag (14.5.2021) in aller Frühe ein neues zeitlich
unbefristetes Innenstadtbaumhaus. "Herrn Rapps Klimapolitik des Nichtstuns und
Abwartens soll Klimagerechtigkeit und einer Mobilitätswende weichen", erklärt
Samuel Bosch (18) die Aktion. "Die Stadt muss Rahmenbedingungen schaffen, dass
wir Privatpersonen uns klimafreundliches Verhalten zeitlich und finanziell
leisten können."

Dieselbe Gruppe errichtete am 12.12.2020 schon ein Baumhaus in Ravensburgs
Innenstadt. Es wurde am 29.12.2020 unter Einsatz des Sondereinsatzkommandos
geräumt, entstand einen Tag später aber einige hundert Meter weiter erneut (dpa
berichtete). Wertschätzend, dass die Stadt Zeit benötigt, um
Klimagerechtigkeitsmaßnahmen zu beraten und zu beschließen, bauten die
Aktivist\*innen am 30.1.2021 ihr Baumhaus ab und hinterließen der Stadt ein
dreimonatiges Ultimatum. Da die Stadt dieses Ultimatum ohne inhaltliche
Gesprächsangebote und ohne Beschluss von Klimagerechtigkeitsmaßnahmen
verstreichen ließ, machen die Klimacamper\*innen nun erneut auf das "politische
Versagen der Stadtregierung" aufmerksam.

"Wir bedauern, dass wir unsere Baumhausklimacamps nicht im Vorfeld beim
Ordnungsamt anmelden können", so Bosch. Damit bezieht er sich auf eine
Pressemitteilung der Verwaltung [1], dernach die Stadt Versammlungen auf Bäumen
grundsätzlich nicht genehmigen werde. "Allein die Formulierung zeugt von
Unkenntnis des Versammlungsgesetzes, denn Versammlungen sind in Deutschland
nicht genehmigungspflichtig", erklärt Samuels Mitstreiter Dr. Ingo Blechschmidt
(32). "Die Versammlungsfreiheit schützt alle Arten Versammlungen, auch solche
mehrere Meter über dem Boden. Für unseren Protest ist es fundamental wichtig,
den Widerstand auf die Bäume zu holen: Bäume sind zugleich Symbol für
Klimazerstörung als auch Klimagerechtigkeit. Klassische Demozüge werden von der
Regierung totgelobt und bewirken keine politischen Veränderungen."

Bosch ist sich sicher: "Die vorgeschobenen Sicherheits- und Umweltbedenken sind
politisch motiviert. Unser letztes Baumhaus hätte die Polizei wohl kaum einen
ganzen Monat bestehen lassen, wenn von ihm tatsächlich eine Gefahr für die
öffentliche Sicherheit ausgegangen wäre. Mit den besetzten Bäumen gehen wir
äußerst behutsam um, Baumschoner verhindern den direkten Kontakt von
Baumhausplatten und -seilen mit dem Baum, und jeder Baumbesetzung geht eine
gründliche Prüfung voraus, dass brütende Vögel nicht gestört werden. Die CDU
hat Nerven, Umweltbedenken zu zitieren, selbst in der Umwelt aber über Leichen
zu gehen!"

[1] https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-befuerchtet-erneut-baumbesetzungen-_arid,11363271.html


## Ort

Ecke Bachstraße/Goldgasse in 88214 Ravensburg, Koordinaten:
47.781387,9.609808 bzw. 47°46'53.0"N 9°36'35.3"O


## Hinweis

Die neue Baumbesetzung ist wie das erste Baumhaus auf unbestimmte Zeit
ausgelegt. Dieses Wochenende werden zahlreiche weitere Aktionen folgen, über
die in separaten Pressemitteilungen informiert wird. Auf
ravensburg.klimacamp.eu wird es stets eine aktuelle Übersicht über laufende
Aktionen sowie Fotos zur freien Verwendung geben. Aktionen des zivilen
Ungehorsams werden besonders gekennzeichnet.
