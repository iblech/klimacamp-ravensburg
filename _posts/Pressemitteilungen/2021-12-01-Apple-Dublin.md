---
layout: page
title:  "01.12.2021: Aktivisten befestigen Banner an Apple Store in Dublin"
date:   2021-12-01 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2112010
#          YYMMDDX
---

*Pressemitteilung vom 1. Dezember 2021*

# Aktivisten befestigen Banner an Apple Store in Dublin

Am frühen Morgen des heutigen Mittwochs brachten die Aktivist\*innen Charlie
Kiehne (19) und Samuel Bosch (18) ein Banner an einem von Dublins Apple Stores
an: "Apple every day keeps the tax away". Die beiden kritisieren, dass Apple auf
agressive Art und Weise Steuern vermeidet und so, nur an die eigene
Konzernbereicherung denkend, dem Gemeinwohl beträchtlichen Schaden zufügt.
"Apple sollte seinen fairen Teil zahlen", fordert Kiehne. "Jeder gewöhnliche
Arbeitnehmer zahlt einen höheren Steuersatz als Apple!", hebt Bosch die aus
seiner Sicht ungerechte Verteilung hervor.

Im Rahmen der veröffentlichen Panama-Papiere schätzt die Europäische Kommission,
dass Apples Steuersatz nur 3,7% beträgt, nach Angaben der BBC ist das weniger
als ein Sechstel der Durschnittsrate für Firmen weltweit. In manchen Fällen
beträgt Apples Steuersatz sogar nur 0,005% [[1]]. "Jeder Euro, der von Apple
veruntreut wird, ist ein Euro, der nicht in das Gemeinwohl investiert werden
kann", erläutert Kiehne die Motivation für ihren Protest.

Die beiden Aktivist\*innen begannen vor einigen Tagen, die Aktion vorzubereiten,
und nahmen dazu die vielen Apple Stores in Dublin in Augenschein. "Wir konnten
nicht Irland verlassen, ohne diesem wunderschönen Land etwas zurückzugeben",
erklärt Bosch. Kiehne und Bosch gefährden als profesionelle Kletter\*innen weder
sich noch Sicherheitspersonal oder Passant\*innen.

## Quellen
[1]: https://www.bbc.com/news/world-us-canada-41889787
[1] [https://www.bbc.com/news/world-us-canada-41889787][1]
 
[2]: https://www.swrfernsehen.de/landesschau-bw/kampf-gegen-kiesabbau-im-altdorfer-wald-100.html
[2] [https://www.swrfernsehen.de/landesschau-bw/kampf-gegen-kiesabbau-im-altdorfer-wald-100.html][2]

[3]: https://www.youtube.com/watch?v=xqcp4NBcGfk
[3] [https://www.youtube.com/watch?v=xqcp4NBcGfk][3]

## Ort
44-45 King St S, Dublin, D02 FA47, Irland

Koordinaten 53.3402765, -6.2620119