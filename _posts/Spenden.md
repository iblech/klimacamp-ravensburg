---
layout: page
title: Spenden
permalink: /spenden/
nav_order: 130
---

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.11/dist/css/splide.min.css">

# Spenden
**Blick hinter die Kulissen — Aktionen und Kosten im Jahresrückblick**

<div style="display: flex; justify-content: center;">
    <img src="/img/spenden/spenden1.jfif" style="width: 100%; max-width: 600px;" alt="">
</div>

<div style="display: flex; justify-content: center; margin-top: 1rem;">
    <a href="/spendenaufruf.pdf" class="btn btn-blue">Spendenaufruf als PDF herunterladen</a>
</div>

2021 ist vorbei – wir freuen uns auf ein neues gemeinsames klimaaktivistisches
Jahr mit vielfältigen bunten Aktionen! Dabei sagen wir ganz herzlich: Danke.
Danke für eure tatkräftige Unterstützung, danke an die zahlreichen engagierter
Menschen, die Aktionen medial begleiten, Sachspenden abgeben, nachts Fahrten
übernehmen, uns mit Rat zur Seite stehen und ihren Zuspruch ausdrücken. Danke
für eure Beiträge zur Klimagerechtigkeitsbewegung in Ravensburg!

Wir wissen außerdem die knapp 19.000 Euro Spenden sehr zu schätzen. Denn: Unsere
Aktionen kosten viel Geld. Die größten Posten sind Kletterausrüstung,
Baumhausbaumaterialien sowie Rechtskosten. Gerade auch weil immer wieder
Material ohne Protokoll konfisziert wird. Den größten Teil der Ausgaben
streckten bislang wenige Privatpersonen vor. Im letzten Monat erhielten wir aber
zahlreiche weitere behördliche Kostenbescheide, teilweise für lange
zurückliegende Aktionen, die unsere bisherigen Spendeneinnahmen unmöglich decken
können. Wenn Sie die Aktivist\*innen also unterstützen möchten, gibt es jetzt die
Möglichkeit dazu. Helfen Sie mit, unseren Widerstand gegen den Ökozid und die
Klimakatastrophe aufrecht zu erhalten, denn Klimagerechtigkeit bleibt
Handarbeit! Viele der jungen Aktivist\*innen werden zudem wegen Lapalien
juristisch verfolgt! Es wird versucht, sie durch Strafen zu "disziplinieren" und
zu demoralisieren, damit sie mit ihrem Protest aufhören! Es laufen gerade viele
Verfahren und wir erwarten Bußgelder und Strafbescheide im fünfstelligen
Bereich. Dabei kommen Repessionskosten zwischen 200 und 58.000 Euro auf uns zu,
die zusätzlich zu unserem schon bestehenden Defizit von 8.000 Euro unsere Kasse
sprengen. Wir sind daher dringend auf Spenden angewiesen.

| Gesamteinnahmen                            | 18.783,37 Euro   |
|:-------------------------------------------|:-----------------|
| Klettersachen                              | - 7.584,78 Euro  |
| "Poly-Seil" zum Baumhausbau                | - 6.832,45 Euro  |
| Sonstiges (Flyer, Lampen, Feuerlöscher, …) | - 5.064,95 Euro  |
| Repressionskosten                          | - 7.762,44 Euro  |
| **Defizit**                                |**-8.461,25 Euro**|

In diesem Sinne wünschen wir Ihnen ein friedvolles und gesundes neues Jahr mit freundlichen Grüßen von
allen Klimaaktivist*innen und Unterstützenden und freuen uns auf ein tolles gemeinsames Jahr mit bunten
Aktionen und vielen Menschen!

<div style="display: flex; justify-content: center;">
    <div style="width: 100%; max-width: 600px;">
        <div class="splide">
            <div class="splide__track">
                <ul class="splide__list">
                    <li class="splide__slide"><img src="/img/spenden/spenden2.jfif" alt=""></li>
                    <li class="splide__slide"><img src="/img/spenden/spenden3.jfif" alt=""></li>
                    <li class="splide__slide"><img src="/img/spenden/spenden4.jfif" alt=""></li>
                    <li class="splide__slide"><img src="/img/spenden/spenden5.jfif" alt=""></li>
                    <li class="splide__slide"><img src="/img/spenden/spenden6.jfif" alt=""></li>
                    <li class="splide__slide"><img src="/img/spenden/spenden7.jfif" alt=""></li>
                </ul>
            </div>
        </div>
    </div>
</div>

## Spendenkonto
    Name: Spenden & Aktionen
    IBAN: DE29 5139 0000 0092 8818 06
    BIC: VBMHDE5F
    Verwendungszweck: Ravensburg (unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

Hinweis: Wir können zwar Spendenbescheinigungen ausstellen -- [schreibt
uns](mailto:baumbesetzung.ravensburg@gmail.com) dazu an -- Spenden an uns sind
aber nicht steuerlich absetzbar.

<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.11/dist/js/splide.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var splide = new Splide('.splide');
        splide.mount();
    });
    if (typeof InstantClick !== 'undefined') {
        InstantClick.on('change', function () {
        setTimeout(() => {
            var splide = new Splide('.splide');
            splide.mount();
        }, 500);
    });
    }
</script>