---
layout: page
title: Fragen und Antworten
permalink: /faq/
nav_order: 80
---

# Fragen und Antworten

*Hinweis.* Dieser Beitrag wurde nicht mit der ganzen Bewegung abgesprochen. Es
gibt keine autorisierte Gruppe und kein beschlussfähiges Gremium, das
"offizielle Gruppenmeinungen" für das Ravensburger Klimacamp beschließen
könnte. Die Menschen im Baumhaus und ihrem Umfeld haben vielfältige und
teils kontroverse Meinungen. Kein Text spricht für die ganze Bewegung oder
wird notwendigerweise von der ganzen Bewegung gut geheißen.


## Politischer Druck und Gespräche mit Verantwortlichen

*Sollten wir besser den Dialog mit den Verantwortlichen suchen?*

Jein. Auf der einen Seite machen wir das bereits, etwa beteiligten wir uns ja
vorletztes und letztes Jahr an der
[Klimakommission](https://www.ravensburg.de/rv/klimakonsens.php). Und vor uns
suchten und bekamen schon andere den Dialog -- die Fakten um die Klimakrise
sind ja seit mehr als 40 Jahren bekannt.

<figure style="float: right">
  <img src="/co2-curve-summits.jpeg" width="900" height="450" style="width: 30em; height: auto; aspect-ratio: attr(width) / attr(height)">
  <figcaption style="text-align: center">
    Das CO₂ in der Atmosphäre nimmt immer weiter zu. (<a href="https://twitter.com/FFF_Frankfurt/status/1308352150209212417">Quelle</a>)
  </figcaption>
</figure>

Solche Dialoge sind zwar gut und wichtig, sie reichen aber nicht aus: Seit mehr
als 40 Jahren steigen ja die weltweiten Emissionen. Besonders deutlich wird das
im nebenstehenden Diagramm. Nach oben ist die CO₂-Konzentration aufgetragen,
nach rechts die Zeit. Markiert sind die Zeitpunkte, wo internationale
Klimaabsprachen getroffen wurden.

Wir benötigen daher nicht nur Dialoge, sondern auch politischen Druck, und den
erzeugen wir durch Aktionen wie dem Baumhausklimacamp.

Noch konkret auf Ravensburg bezogen eine Sache, die auch Professor Ertel viel
kritisiert: Der Gemeinderat verabschiedete letztes Jahr zwar den aus der
Klimakommission hervorgegangenen "Klimakonsens" (kein echter Konsens), aber er
verabschiedete keinerlei Maßnahmen, um die beschlossenen Klimaziele zu
erreichen. Dabei kosten manche Maßnahmen (etwa Appelle an überregionale Gremien
oder geeignete Straßenschilder) nicht einmal Geld oder nur ganz wenig.


## Investitionen durch unsere Steuergelder

*Fehlen die Steuergelder für den [SEK-Einsatz gegen uns](/pages/Pressemitteilungen/2020-12-29-Raeumung.html) dem Klimaschutz?*

Ja, wir sind auch nicht glücklich über den Polizeieinsatz. Der Einsatz im
[Dannenröder Wald](https://de.wikipedia.org/wiki/Dannenr%C3%B6der_Forst) in Hessen kostete übrigens mindestens 150 Millionen Euro --
was wir als Gesellschaft stattdessen damit machen könnten, rechneten wir mal an paar
[Beispielen](https://klimaschutz.madeingermany.lol/) aus.

Natürlich wirkt eine Baumbesetzung auf den ersten Blick ziemlich illegal.
Tatsächlich aber erfüllt eine Baumbesetzung der Art, wie wir sie durchführen,
keinen Straftatbestand. Das Versammlungsrecht verdrängt sogar die Verordnung
zur Ausgangssperre und die Baumschutzsatzung, dernach ansonsten die Besetzung
zwei Ordnungswidrigkeiten darstellen würde.

Die Stadt rechtfertigte ihren Einsatz in ihrem Räumungsbescheid mit einer
"unmittelbaren Gefahr für die Sicherheit" (wörtliches Zitat aus dem
Räumungsbescheid). Lag wirklich eine solche Gefahr vor? Von ein paar
Jugendlichen mit Bannern in einem Baumhaus? Wir argumentieren, dass der
Polizeieinsatz unnötig war.

Die Polizei ist übrigens vermutlich derselben Meinung -- das meinen wir daran
zu erkennen, dass sie mit uns eine Vereinbarung schloss, dass wir bis zum 10.1.
freies Geleit zu unserem neuen Baumhaus erhalten und dieses so weiter betreiben
können. (Nach dem 10.1. soll es dann Verhandlungen mit der Stadt geben.) So
unmittelbar kann die von uns ausgehende Gefahr also gar nicht sein, sonst
müsste die Polizei sofort eingreifen.

<figure style="float: right">
  <img src="/katapult-subventionen.jpeg" width="480" height="480" style="width: 30em; height: auto; aspect-ratio: attr(width) / attr(height); float: right">
  <figcaption style="text-align: center">
     Deutschland ist trauriger Vorreiter in Kohleinvestition. (<a
     href="https://katapult-magazin.de/de/artikel/artikel/fulltext/deutschland-auf-platz-1-eu-staaten-foerdern-weiterhin-fossile-brennstoffe/">Quelle</a>)
  </figcaption>
</figure>

Übrigens: Jährlich subventioniert Deutschland mit 38 Milliarden Euro fossile
Brennstoffe wie Kohle und Gas. Eine Grafik von Katapult zeigt, wie das in
Relation zu den Subventionen unserer Nachbarländer steht.
Zum Vergleich: Die Gesamteinnahmen durch Zug-, Bus- und Tramtickets
belaufen sich jährlich auf [13 Milliarden Euro](https://www.adac.de/news/oepnv-bilanz-2019-bus-bahn-tram/). Der Staat könnte
also, rein rechnerisch, sofort den ÖPNV für alle Bürger\*innen kostenlos machen
und hätte dann noch 25 Milliarden Euro übrig, um andere sinnvolle Investitionen
zu tätigen.


## Abwälzung von Verantwortung an Einzelpersonen

*Sollten wir lieber intelligente Konzepte entwickeln, die Verhaltensänderungen bei allen
Bürger\*innen fördern?*

Es ist eine Masche vieler Politiker\*innen, die Verantwortung für
Klimagerechtigkeit an uns Bürger\*innen, an Einzelpersonen, abzuwälzen. Diese
Masche muss mensch durchschauen! Es stimmt zwar, dass wir alle weniger Fleisch
konsumieren, auf Ökostromtarife wechseln und weniger fliegen müssen. (Übrigens:
Die Produktion von 1 kg Rindfleisch verursacht genau so viel Treibhausgase wie
fünf Jahre lang jeden Tag das Handy mit Kohlestrom aufzuladen (und übrigens
auch genau so viel wie die Produktion von 500 g Butter). Und Ökostromtarife
sind oft günstiger als Tarife bei den lokalen Stadtwerken.)

Aber die richtig großen CO₂-Einsparungen kann im aktuellen System nur der Staat bewirken. Momentan
sind etwa viele Menschen auf ein Auto angewiesen, weil Bus und Tram zu
wenig ausgebaut und zu teuer sind, und weil ein sicheres und
komfortables Radwegenetz fehlt. Diese Autoabhängigkeit ist durch die große
Treibhausgasbelastung nicht nur schlecht für unsere Zukunft (Verkehr ist Platz 3 der
Treibhausgasquellen in Deutschland) und durch die vielen Unfälle nicht nur
schlecht für uns alle, sondern sie verschärft auch gesellschaftliche
Ungerechtigkeit: Denn für ärmere Menschen kann der Unterhalt eines Autos eine
erhebliche Belastung darstellen. Wir fordern daher, dass der Staat zügig
Schritt für Schritt eine Mobilitätswende einleitet. Am Ende sollen dann weniger
Menschen aufs Auto angewiesen sein und in unseren Innenstädten soll es mehr
Platz zum Bummeln und Aufhalten geben.

Auch die Energiewende kann nur der Staat kräftig ankurbeln: einerseits, indem
er nicht mehr fossile Brennstoffe subventioniert. So werden die automatisch
unrentabel, sodass die großen Energiekonzerne auf erneuerbare Energie
umsteigen. Andererseits könnte er die Rahmenbedingungen so anpassen, dass es
leichter für Bürger\*innen wird, klimagerecht Energie zu produzieren. Das fängt
etwa bei der Förderung von Solaranlagen fürs eigene Haus und die Mietwohnung
an. Außerdem könnte er es leichter für Bürger\*innen machen¸ sich
zusammenzuschließen und einen Windpark oder eine Solaranlage zu betreiben.
Wir nehmen Windräder ganz anders wahr, wenn wir wissen: Mit jeder Umdrehung
verdienen wir einen Euro.

Viele von uns (vielleicht die absolute Mehrheit?) sind übrigens auch überhaupt
keine Fans von den Grünen. Auf der einen Seite machen und fordern sie zu wenig,
auf der anderen Seite denken sie den Gerechtigkeitsaspekt und Visionen direkter
Demokratie viel zu wenig mit.


## Technologische Lösungsbeiträge

*Sollten wir unsere Kraft und Energie für neue kreative und intelligente Lösungen
einsetzen und uns mit unserer Expertise zu Klimatolog\*innen machen?*

Manche von uns wollen später tatsächlich etwas in diese Richtung studieren oder
arbeiten schon als Wissenschaftler\*innen. Aber das ist weder nötig noch
zielführend. Erstens müssen die Emissionen in den nächsten Jahren rapide fallen
-- nicht dann, wenn wir irgendwann Machtpositionen erreicht haben.

Und zweitens gibt es ja all die Klimawissenschaftler\*innen und
Ingenieur\*innen schon! Wir wissen ganz genau, was zu tun ist, und alle nötigen
Technologien sind hinreichend gut entwickelt. Nur schenken die Regierungen
diesen Expert\*innen zu wenig Gehör, weil der politische Druck fehlt.

Das ist das Hintertückische an der Klimakrise: Anders als ein Säbelzahntiger
ist sie nur in schleichenden Veränderungen sichtbar. Drastisch erkennbar ist sie
nur in Diagrammen -- und wenn es zu spät ist.


## Einsatz um andere wichtige Belange

*Ist es aus unserer Sicht in Ordnung, wenn es ein weiteres Baumhaus zu
Altersarmut, sozialer Ungerechtigkeit, Corona-Verordnungen oder BLM gibt?*

Die Versammlungsfreiheit genießen alle Menschen in Deutschland, und wir werden
anderen Initiativen sicherlich nicht vorschreiben, wie sie ihren Protest zu
gestalten haben. Wir finden auch nicht, dass ein Baumhaus oder ein Banner
wirklich störend ist und Passant\*innen in ihrem Leben einschränkt.

Wir glauben aber schon, dass Aktionen wirksamer sind, wenn sie in ihrer
Bildsprache mit dem Protestthema harmonieren. Das ist bei Bäumen und
Klimagerechtigkeit der Fall, da ja Bäume einerseits CO₂ binden und die Luft
abkühlen, andererseits immer noch nicht angemessen wertgeschätzt und viel zu
oft unnötig gefällt werden (etwa, wie in Hessen, um Platz zu machen für eine
[neue Autobahn](https://wald-statt-asphalt.net/de/)! Brauchen wir wirklich noch
eine weitere? Oder sollte der Staat eine Mobilitätswende angehen, damit wir
Bürger\*innen weniger aufs Auto angewiesen sind?). Einen so direkten Baumbezug
sehen wir bei den anderen angesprochenen Themen eher nicht.

Gegen die soziale Ungerechtigkeit der Corona-Verordnungen demonstrierten etwa
schon Künstler\*innen, indem sie an einem zentralen Platz ein tolles Musikstück
spielten -- dabei aber je zwei von drei Takten ausließen. Das erachten wir als
eine aussagekräftige und starke Aktion!


## Umsichtiger Umgang mit dem Baum

*Beschädigt unser Baumhausklimacamp den Baum?*

Nein. Wir haben das Fachwissen, um Baumhäuser ohne Beschädigung des Baums bauen
zu können.

Uns tat es allerdings wahnsinnig weh, als das SEK den Baum unseres ersten
Baumhauses verletzte und mutwillig Äste abbrach und absägte. Viele von uns
waren im [Danni](https://de.wikipedia.org/wiki/Dannenr%C3%B6der_Forst) und
mussten da mitunter stundenlang am Stück ansehen, wie neben uns ein Baum nach
dem anderen fiel. Wir hätten nicht erwartet, dass die Stadt den SEK-Einsatz
anordnet, zumal er in der Zeit des sonst praktizierten behördlichen
["Weihnachtsfriedens"](https://de.wikipedia.org/wiki/Weihnachtsfrieden_(%C3%96ffentlicher_Dienst)) fiel.

Nach unserer erneuten Besetzung kam die Polizei auf uns zu und bot
Verhandlungen an. Dieses Gesprächsangebot nahmen wir dankbar an. Bis zum 10.1.
sichert nun die Polizei unser Grundrecht auf Demonstration, indem sie uns
freies Geleit auf den Baum garantiert und uns weiter ermöglicht, unser
Baumhausklimacamp zu betreiben. Danach sollen Gespräche mit der Stadt beginnen.

Wir wissen übrigens nicht, was der wahre Grund für den SEK-Einsatz war.
Provozieren wollten wir ihn sicher nicht -- er war auch für viele von uns und
unsere Familien überhaupt nicht einfach! Offiziell begründete die Stadt den
Einsatz damit, dass von unserem Baumhaus eine "unmittelbare Gefahr für die
öffentliche Sicherheit" ausging (wörtliches Zitat aus dem Räumungsbescheid).
Das kann aber nicht der wahre Grund sein -- welche Gefahr geht von ein paar
Jugendlichen mit Bannern in einem Baumhaus wirklich aus? Alle Konstruktionen
waren fachgerecht gesichert. Und jetzt hat die Polizei ja auch keine
Räumungsabsicht. Wir vermuten stattdessen einen politischen Hintergrund: Der
Stadt war unser Klimacamp ein Dorn im Auge, da wir damit auf ihre fehlende
Verantwortungsübernahme in Sachen Klimagerechtigkeit und Abkommenseinhaltung
hinwiesen.


## Kontinuierliche Auseinandersetzung

*Ist es für die Einsatzkräfte auch unangenehm, sich während
der Feiertage und zwischen den Jahren mit uns auseinandersetzen zu müssen?*

Es tut uns leid, dass Einsatzkräfte geruhsame Zeit mit ihrer
Familie verpassten. In Deutschland hat sich eigentlich
zwischen einer Woche vor Weihnachten und Anfang Januar die Praxis des
["Weihnachtsfriedens"](https://de.wikipedia.org/wiki/Weihnachtsfrieden_(%C3%96ffentlicher_Dienst))
etabliert, in der Behörden keine Verwaltungsakte erlassen, die Empfänger\*innen
belasten. Wieso die Stadt diesen Frieden in unserem Fall nicht respektierte,
wissen wir nicht. Und übrigens: Auch wenn wir uns mental auf eine Räumung
vorbereiteten -- einfach war sie für uns und unsere Familien trotzdem nicht.

Ansonsten aber müssen wir schon sagen: Die aktuellen Entscheider\*innen müssen
die schlimmsten Folgen der Klimakatastrophe nicht selbst miterleben. Wenn
wegen der Klimakrise unsere Wirtschaft zusammenbricht und Hungersnöte,
Krankheiten und Kriege an der Tagesordnung sind, dann geht es um unsere Zukunft.
Solange diese aktiv von den Regierungen sabotiert wird, fordern wir, dass sich
Entscheider\*innen jeden einzelnen Tag mit Klimagerechtigkeit auseinandersetzen.
