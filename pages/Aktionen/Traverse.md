---
layout: page
title: Traverse – Livestream vom 27.12. um 12:00 Uhr
permalink: /aktionen/traverse/
nav_order: 1
parent: Aktionen
---

# Traverse -- Livestream vom 27.12. um 12:00 Uhr

<style>
  .carousel-container { position: relative; }
  .carousel { overflow-x: scroll; scroll-snap-type: x mandatory; scroll-behaviour: smooth; display: flex; }
  .carousel a { flex-shrink: 0; scroll-snap-align: start; }
</style>

<div class="carousel-container">
  <div class="carousel">
    <a href="/traverse.jpeg"><img src="/traverse.jpeg" width="690" height="690" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt=""></a>
    <a href="/traverse-1.jpeg"><img src="/traverse-1.jpeg" width="640" height="1280" style="width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt=""></a>
    <a href="/traverse-2.jpeg"><img src="/traverse-2.jpeg" width="640" height="1280" style="width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt=""></a>
    <a href="/traverse-3.jpeg"><img src="/traverse-3.jpeg" width="640" height="480" style="width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt=""></a>
    <a href="/traverse-4.jpeg"><img src="/traverse-4.jpeg" width="480" height="640" style="width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt=""></a>
  </div>
</div>

**Weitere Fotos (auch in hoher Qualität) in der
[Fotogalerie](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/)**

🌳 Erinnert ihr euch an die
[großartige](https://twitter.com/keinea49/status/1324300861498105858)
[Traverse](https://www.youtube.com/watch?v=FVFFmHIPs0A) über die B62 beim Danni?

🌼 Wir spannen heute in Ravensburg in 12 Meter Höhe auch eine solche Traverse!
Ein fünf mal sieben Meter großes Banner wird fortan alle Autofahrer\*innen und
Passant\*innen auf das klimapolitische Versagen aufmerksam machen und die
Regierung zur Einhaltung des Pariser Klimaabkommens auffordern. 🌱

📸 Seid bei der Banneraufhängung dabei und verfolgt um 12:00 Uhr unseren
Livestream! [Instagram @baumbesetzung.ravensburg](https://www.instagram.com/baumbesetzung.ravensburg/).
Leider können wir technische Probleme nicht ausschließen.

🗞 Hintergründe in unserer
[Pressemitteilung](/pages/Pressemitteilungen/2020-12-27-Traverse.html).
