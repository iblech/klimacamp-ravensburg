---
layout: page
title: Erfolgte Räumung am 29.12. um 20:00 Uhr
permalink: /aktionen/raeumung/
nav_order: 2
parent: Aktionen
---

# Erfolgte Räumung am 29.12. um 20:00 Uhr

Am Abend des 29.12.2020 wurden wir pünktlich zu Beginn der Ausgangssperre mit
einem Großeinsatz von Polizei und Feuerwehr inklusive Sondereinsatzkommando
unerwartet geräumt. Auch wenn uns die Stadt kriminalisiert: Unser Protest ist legitim. Ihr
könnt unsere Häuser zerstören, aber nicht die Kraft, die sie schuf!

[Unsere Pressemitteilung zur Räumung](/pages/Pressemitteilungen/2020-12-29-Raeumung.html){: .btn .btn-green }
[Solibotschaft von Professor Ertel](https://www.youtube.com/watch?v=udhEKyvbw2Q){: .btn .btn-purple }
[Ertel: "Ich habe ihnen gesagt, dass ich noch zwei weitere Klettergurte habe"](https://www.youtube.com/watch?v=0ryYIYQ3uWs){: .btn .btn-purple }
[30-minütiges Räumungsvideo](https://www.youtube.com/watch?v=eAqwo357FME&list=PLzFYjJINUTzmczuws1TlyZoEoZcPGf1l0&index=1){: .btn .btn-purple }

Alle Fotos und Videos für Presse zur freien Verwendung.

<figure style="display: inline-block">
  <video controls width="180" height="320" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-0.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-0.mp4" type="video/mp4">
  </video>
  <figcaption>Räumungsankündigung</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="180" height="320" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-kosten.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-kosten.mp4" type="video/mp4">
  </video>
  <figcaption>Androhung von Kosten</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="180" height="320" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-schoen.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-schoen.mp4" type="video/mp4">
  </video>
  <figcaption>Baumhaus von innen</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="180" height="320" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-zerstoerung.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-zerstoerung.mp4" type="video/mp4">
  </video>
  <figcaption>Zerstörung</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="180" height="320" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-schwenk.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-schwenk.mp4" type="video/mp4">
  </video>
  <figcaption>In der Traverse</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="270" height="480" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-soli1.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-soli1.mp4" type="video/mp4">
  </video>
  <figcaption>Du bist nicht allein!</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="320" height="240" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-soli-augsburg.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-soli-augsburg.mp4" type="video/mp4">
  </video>
  <figcaption>Soli aus Augsburg</figcaption>
</figure>

<figure style="display: inline-block">
  <video controls width="212" height="116" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-final.jpeg">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-final.mp4" type="video/mp4">
  </video>
  <figcaption>Die letzten Sekunden</figcaption>
</figure>


