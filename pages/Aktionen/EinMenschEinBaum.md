---
layout: page
title: Ein-Mensch-Klima-Versammlung (Sep. 2022)
permalink: /aktionen/klimaversammlung/
nav_order: 4
parent: Aktionen
---

# Ein-Mensch-Klima-Versammlung in Ravensburg (September 2022)

<iframe width="560" height="315" src="https://www.youtube.com/embed/aOtXQMgUuvg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
