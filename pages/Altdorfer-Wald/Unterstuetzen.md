---
layout: page
title: Unterstützen
permalink: /altdorfer-wald/unterstuetzen/
parent: 🏕 Altdorfer Wald 🏕
nav_order: 20
---

# Die Waldbesetzung unterstützen

Wir freuen uns, dass du die Waldbesetzung unterstützen möchtest! Wir wissen
jeden Beitrag wahnsinnig zu schätzen und sind dir sehr
dankbar!

So kannst du konkret helfen:

* 🗣 Im Freundes- und Bekanntenkreis über Klimagerechtigkeit, die dringend nötige
  Mobilitätswende sowie den Altdorfer Wald sprechen.
* ✉️ Leser\*innenbriefe schreiben.
* 🔋 Unsere Batterien, die wir für unsere Laptops benötigen (Schule bzw. Home
  Office), aufladen: beim Wald abholen, später wieder zurückbringen. Ladegerät
  haben wir.
* 🧶 Materialien von der unten aufgeführten Liste vorbeibringen, falls du sie zu
  Hause hast und gerade nicht brauchst. (Bitte kaufe nicht extra Gegenstände
  von dieser Liste!)
* 💶 [Geld spenden](/spenden/) (insbesondere für Baumhausmaterialien und
  etwaige Rechtskosten)


## Materialien (Stand 4.4.2021)

Sehr dringend benötigt:
- Dielen, Bretter
- Sägespäne
- Balken
- Panzertape (~Gewebeklebeband)
- Paletten
- Erste-Hilfe-Sets

Ebenfalls benötigt:
- Bierbänke
- Decken
- Musikboxen
- Geschirr
- Klettersachen
- Matratzen 
- Eimer / Kanister
- Stirnlampen
- Wellblech (Dach)
- AAA-Akkus für Stirnlampen (+Ladegerät)
- Trinkwasser
- Warmhaltebox für Essen
- 1000l Wasserkanister
- Planen (LKW / Werbebanner etc geht auch)
- Biwaksäcke
- Powerbanks
- Nägel (alle Größen, v.a. 8er und 12er)  lieber 10.000 Stück als 1.000
- Schrauben
- Feuerzeuge
- Zangen
- Fenster
- Metallgehäuse
- Seitenschneider 
- Bolzenschneider 
- Zweimenschsäge 
- Handsägen
- Hämmer
- Schäleisen 
- Brecheisen 
- Schaufeln 
- Äxte
- Schnur
- Zollstock
- Hobel
- Bohrer 
- Menstruationsartikel
- Klopapier 
- Müllsäcke 
- Töpfe 
- Pfannen 

Nice to have:
- Transpistoff
- Acrylfarben, 
- Sekundenkleber
- Glitzer
- Edding
- Sikaflex
- Nähzeug 
- kleiner holzbetriebener Herd für Baumhausküche 
- kleine Schubladenschränke für oben auf dem Baumhaus  
- Solarpanels (Semipremium)
- Bitumen
- Kabelbinder
- Reifen
- Betonklötze
- Sand
- Zement
