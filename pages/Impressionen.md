---
layout: page
title: Impressionen
permalink: /impressionen/
nav_order: 120
redirect: https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/
---

# Impressionen

[Zur Fotogalerie](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/)

<script>
  location.replace("https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/");
</script>
