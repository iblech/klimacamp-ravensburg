---
layout: home
title: Start
permalink: /
nav_order: 10
---

<div style="background-color: lightgreen; padding: 1em">                                                                 
  <a href="https://t.me/joinchat/Sv8dMD4wxN4qs-AL">Soli-Gruppe auf Telegram</a><br>
  <strong>E-Mail:</strong>
  <a href="mailto:baumbesetzung.ravensburg@gmail.com">baumbesetzung.ravensburg@gmail.com</a>
</div>


# Film über Waldbesetzung ausverkauft

<img src="/pressespiegel/schwaebische-1423309.jpeg" width="723" height="664"
style="display: block; width: 100%">

<img src="/data/auffuehrungen-doku.jpeg" width="1034" height="606"
style="display: block; width: 100%">

Weitere Rezensionen:
<a href="https://www.diebildschirmzeitung.de/blix/aktuell/14766-von-menschen-auf-baeumen">Blix</a>,
<a href="https://www.youtube.com/watch?v=hW3vxY1skcY">Elfenbeinturm Media</a>


# Klimagerechtigkeit im Greenpeace Radio Stuttgart

## Heute Abend, 28.02, von 18:00 - 19:00 Uhr
<img src="/data/Radio_Slides_Story.jpg" width="180" height="320" style="display: block" alt="sharepic">

[Sharepic](https://ravensburg.klimacamp.eu/data/Radio_Slides_Story.jpg) \
[Freies Radio](https://www.freies-radio.de/programm/tagesansicht/heute) \
[Livestream Radio](https://streaming.fueralle.org/frs-hi.mp3) \
Antenne 99,2 MHz \
Kabel: 102,1 MHz \

### Du möchtest bei der nächsten Sendung auch etwas zu Klimagerechtigkeit sagen? 
### Super, dann schau hier vorbei:
[Website](https://rotmilan.eu/voice ) \
[Telegram-Gruppe](https://t.me/voice_bw) \

---

# Trailer zum Kinofilm - Samstag, 25.02. in der Linse Weingarten

<img src="/img/insta-linse.jpeg" width="591" height="1280" style="display: block" alt="Trailer zum Kinofilm">

Du willst den Trailer mit deinen Freunden teilen?\ 
Super, einfach Rechtsklick auf den Link unten und "Speichern unter..." auswählen.\
Du findest den Film in deinem Download-Ordner oder Gallery. \
[Trailer anschauen oder speicher und teilen](/data/Trailer_Linse_Weingarten.mp4)


# Berichte Q1 2023
[Kontext Wochenzeitung: Altdorfer Wald - Ein Hort der Hoffnung](https://www.kontextwochenzeitung.de/gesellschaft/620/ein-hort-der-hoffnung-8699.html)
[Schwäbische Zeitung: Windräder im Altdorfer Wald: Das Dilemma der Klimaschützer](https://www.schwaebische.de/regional/baden-wuerttemberg/das-dilemma-der-klimaschuetzer-1377940) [(JPG)](/data/2023_q1/szDilemmaKlimaschuetzer.jpg)
[Schwäbische Zeitung (JPG): Berlin/Stuttgart (dpa/kab)](/data/2023_q1/dpa.jpg) \
[Schwäbische Zeitung: Windpark Altdorfer Wald](https://www.schwaebische.de/regional/oberschwaben/ravensburg/windpark-altdorfer-wald-es-werden-die-groessten-anlagen-der-region-1372702)
Leserbriefe:
* Roland Banzhaf aus Vogt meint: "Ein handfester Streich des Staats an seinen Bürgern" [PDF](/data/2023_q1/sz/09_02_Windpark_Landschaft/Leeserbriefe/Roland Banzhaf aus Vogt meint_Ein handfester Streich des Staats an seinen B_rgern_.pdf) [JPG](/data/2023_q1/sz/09_02_Windpark_Landschaft/Leeserbriefe/Roland Banzhaf aus Vogt meint_Ein handfester Streich des Staats an seinen B_rgern_.jpg)
* Frank Kirchner aus Vogt meint: "Man opfert Vogt für eine irrsinnige Ideologie" [JPG](/data/2023_q1/sz/09_02_Windpark_Landschaft/Leeserbriefe/Frank Kirchner aus Vogt meint_Man opfert Vogt f_r eine irrsinnige Ideologie_.jpg)
* Alexander Knor aus Bad Wurzach meint: "Das Ende einer Landschaft" [JPG](/data/2023_q1/sz/09_02_Windpark_Landschaft/Leeserbriefe/Alexander Knor aus Bad Wurzach meint_Das Ende einer Landschaft_.jpg)      

---

# Demo am 11.02 - Stuttgart, Hauptbahnhof, um 14 Uhr

## Weitere Infos:
[Sharepic Demo](https://ravensburg.klimacamp.eu/data/sharepic_08_02.jpg) \
[Negativbeispiel Parität](https://www.youtube.com/shorts/am0OxBUUyYc) \
[Website RVBO](https://www.rvbo.de) \
[RVBO: 56 Mitglieder-Liste](/data/2020-12-18_RVBO.pdf) \
[Natur- und Kulturlandschaft Altdorfer Wald e.V.](https://altdorferwald.org) \
[S4F Gutachten Regionalplan](https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf) \
[Video Gehzeug-Aktion RV](https://www.youtube.com/watch?v=91ltl03b8xw) \
[SPIEGEL TV: Tübingens Weg in die Energiewende](https://www.youtube.com/watch?v=WywER5wRLEI)   
[hr-fernsehen: Welche Umweltauswirkungen haben Windräder im Wald?](https://www.youtube.com/watch?v=7fPyMytbsVw)

<img src="/data/sharepic_08_02.jpg" width="180" height="320" style="display: block" alt="sharepic">


---

# Demo & Waldspaziergang am 08.01.2023
<img src="/img/demo_08_01_23.jpg" width="592" height="395" style="display: block" alt="Waldspaziergang Alti">
[Programm / Sharepic zur Demo](/img/Waldspaziergang.jpg) \
[Merkzettel für Räumung Alti (PDF)](/data/Merkzettel_Alti_Räumung.pdf) \
Presse: \
[08.01.2023 SWR: Protest gegen Kiesabbau im Altdorfer Wald](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/demo-im-altdorfer-wald-gegen-kiesabbau-104.html) \
[Pressemitteilung 05.01.23](/pages/Pressemitteilungen/2022-09-12-Ab_ jetzt_gilt_Tempo_30_im_Stadtgebiet_Ravensburg.html) \
[Pressemitteilung 09.01.23](/pages/Pressemitteilungen/2023-01-05-Demo_Alti.html) \
[Blix 09.01.23](https://www.diebildschirmzeitung.de/diebildschirmzeitung/bad-waldsee/bad-waldsee-le/13877-mehr-als-400-menschen-demonstrierten-im-altdorfer-wald-fuer-waldschutz-trinkwasserschutz-klimaschutz)

---

# Gehzeug-Aktivist*innen legen Verkehr im Schussental lahm
<a href="https://www.youtube.com/watch?v=91ltl03b8xw">
<img src="/img/gehzeug_rv_dez_1.jpg" width="592" height="333" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Gehzeug-Aktion in Ravensburg"> </a>
[Hier geht es zum YouTube Video](https://www.youtube.com/watch?v=91ltl03b8xw)</br>
Die Gehzeuge stehen  laut Veranstalter:innen symbolisch für das "Tempo", mit der die Stadt Ravensburg in Sachen Klimaschutz derzeit unterwegs ist . Mit der angekündigten Aktion soll gezeigt werden, dass die Stadt im Schneckentempo in der Mobilitätswende sowohl den Ravensburger Klimakonsens als auch ihren Anteil zur Wahrnehmung der 1,5°-Grenze deutlich verfehlen wird. Auf den mitgeführten Bannern sind 
Botschaften wie „Stadtverbot für Autos“ oder „Fossiler Verkehr ist verkehrt“ zu lesen.

[Pressemitteilung Gehzeug-Aktivist*innen legen Verkehr im Schussental lahm](https://ravensburg.klimacamp.eu/pages/Pressemitteilungen/2022-16-12-Gehzeug_Aktivist_innen_legen_Verkehr_im_Schussental_lahm.html)

---

# Klimaschützer überkleben Verkehrsschilder 
Entlang der B30 zwischen RavensburgOberzell und Baindt haben Klimaaktivisten Schilder zur Geschwindigkeitsbegrenzung überklebt.
[08.12.2022 Schwäbische Zeitung: Klimaschützer überkleben Verkehrsschilder](https://pressreader.com/article/282093460766382)

---

# CDU-Bundestagsabgeordneter Axel Müller "korrigiert" Artikel in der Schwäbische Zeitung  
Werden Medien von Politiker:innen manipuliert? Die Schäbische Zeitung hat zwei Artikel mit dem Thema "Klimaaktivisten zeigen CDU-Politiker Müller an" veröffentlicht. Vergleicht selbst: 
[10.11.2022 Schwäbische Zeitung: Klimaaktivisten zeigen CDU-Politiker Müller an (Online-Version)](pages/Pressemitteilungen/2022-11-10-Klimaaktivisten_zeigen_CDU-Politiker_Mueller_an.html)
[12.11.2022 Schwäbische Zeitung: Klimaaktivisten zeigen CDU-Politiker Müller an (Print-Version)](https://pressreader.com/article/281711208629777)

---

# Aktivisten setzen Zeichen für Windkraft
<img src="/img/gehzeugWK.jpg" width="640" height="468" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Aktivisten setzen Zeichen für Windkraft">

[10.11.2022 Schwäbische Zeitung: Aktivisten setzen Zeichen für Windkraft](https://www.pressreader.com/article/282943864239396)


---

# Regio TV: Die Klimakonferenz 2022
<a href="https://youtu.be/CxQM7Nmclb4">
<img src="/img/regioTVcop27.jpg" width="640" height="362" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Regio TV Die Klimakonferenz 2022">
</a>
[Hier geht es zum Regio TV Video](https://youtu.be/CxQM7Nmclb4)

---

# Klimaaktivisten in der Bravo
<img src="/img/bravo_index.jpg" width="592" height="322" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Klimaaktivisten in der Bravo">
[Bravo Seite 1](img/bravo_1.jpg)
[Bravo Seite 2](img/bravo_2.jpg)

---

# Protestaktion am Kieswerk Tullius
<img src="/img/gehzeugTullius.jpg" width="640" height="480" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Protestaktion am Kieswerk Tullius">

[04.11.2022 Schwäbische Zeitung: Aktivisten blockieren Straße](https://www.pressreader.com/article/282080575787345)


# 90 Meter: Filmvorführung Besetzung des Altdorfer Waldes im Kulturzentrum Achberg 
Herzliche Einladung zu einem Filmabend mit anschließenden Gespräch mit dem Regisseur Claudio Brauchle sowie Samuel Bosch und Charlie Kiehne von den Aktivist·innen. 

Nähere Informationen zur [Filmvorführung](https://kulturzentrum-achberg.de/90-meter-filmvorfuehrung/)

---
# Zweite Rodung am Kieswerk Tullius im Jahr 2022

<a href="https://www.youtube.com/embed/X0NTVd0GwNs"><img src="/img/zweite-rodung-tullius-2022.webp" width="560" height="315" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt=""></a>

---

**[Offener Brief der Parents for Future Ravensburg zu der Banneraktion vom 15.
Mai 2021](Offener-Brief-PFF-2021-05-19.pdf)**

---

**Baumhausaktionswochenende gestartet**

🏕 Neues dauerhaftes Innenstadtbaumhaus in Ravensburg 🏕

"Klimaschutz ist uns sehr wichtig!!", meinte die Stadt Ravensburg damals zu
uns, als wir unser zweites Innenstadtbaumhaus abbauten und der Stadt ein
dreimonatiges Ultimatum hinterließen. Die drei Monate sind nun verstrichen,
Klimagerechtigkeitsmaßnahmen wurden in der Zeit aber keine ergriffen.

Daher sind wir ab heute (14.5.2021) wieder am Start und werden unentwegt einfordern, was
eine große Mehrheit schon lange möchte und das Bundesverfassungsgericht
bestätigte: Klimagerechtigkeit! Dieses Wochenende werden zahlreiche weitere
Aktionen folgen. Schaut gerne am Baum vorbei (die Versammlungsfreiheit
verdrängt die Kontaktbeschränkungen) 😊 Maske und Abstand dabei nicht
vergessen.

🌐 [Website](https://ravensburg.klimacamp.eu/)<br>
ℹ️ [Info-Kanal auf Telegram](https://t.me/klimacamp_ravensburg)<br>
💬 [Soli-Gruppe auf Telegram](https://t.me/joinchat/Sv8dMD4wxN4qs-AL)<br>
🗺 [Ecke Bachstraße/Goldgasse (47.781387,9.609808)](https://www.google.com/maps/place/47%C2%B046'53.0%22N+9%C2%B036'35.3%22E)

**[Pressemitteilung](/pages/Pressemitteilungen/2021-05-14-Aktionswochenende1.html)**
**[Fotos](https://www.speicherleck.de/iblech/stuff/.rav-aktion)**

[14.5. 5:25] Innenstadtbaum besetzt. Koordinaten: 47.781387, 9.609808<br>
[14.5. 9:39] Besetzi haben jetzt einen Platzverweis für "alle Bäume". Besetzi
bleiben auf dem Baum, Motto: CO₂-Reduktion an €DU-Umfragewerte koppeln!<br>
[15.5. 6:00] Traverse mit Banner "Wer Straßen sät, wird Verkehr ernten" über
die zentrale vierspurige Schussenstraße gespannt (weit oberhalb des Lichtraums
der Straße). Verkehr könnte ganz normal fließen, doch Polizei blockiert Straße.
Ecke Schützenstraße.

---

**Professor Ertel wollte heute (11.5.2021) in aller Frühe zusammen mit
Klimagerechtigkeitsaktivist\*innen auf einem Baum seiner Hochschule ein
Baumhaus bauen, um damit folgende extreme Forderung durchsetzen:** Dass die
Hochschule die Heizung ausstellt, wenn sie nicht benötigt wird. 10 Jahre lang
setzte sich Ertel schon vergeblich dafür ein und ging sogar an die
höchstmöglichen Stellen. Ein anderer Weg als ziviler Ungehorsam blieb ihm
nicht.

Pressekonferenz heute Dienstag (11.5.2021) um 11:30 Uhr am baumhauslosen Baum:
Doggenriedstraße 28, 88250 Weingarten, direkt neben der Mensa.

[Zur Pressemitteilung](/pages/Pressemitteilungen/2021-05-09-RWU.html)

---

<div class="btn-pink" style="padding: 1em">
  <a href="https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf"><img src="/s4f-regionalplan.webp" width="442" height="456" style="display: block; width: 40%; height: auto; aspect-ratio: attr(width) / attr(height); float: left; padding-right: 1em"></a>

  <strong>Der Regionalplan ist ein Klimahöllenplan!</strong><br><br>

  Es liegt nun ein <strong>wissenschaftliches Gutachten</strong> zum aktuellen
  Regionalplanentwurf vor, das sachlich fundiert eine ganze Reihe von
  Unstimmigkeiten und professionellem Fehlverhalten seitens des
  Regionalverbands dokumentiert.<br><br>

  👩‍🔬 <a style="color: white" href="https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf">Direkt zum Gutachten</a> (69 Seiten)<br>
  🌳 <a style="color: white" href="/regionalplan">Was ist überhaupt der Regionalplan?</a>
  <div style="clear:both"></div>
</div>

<a href="/altdorfer-wald/"><img src="/altdorfer-wald-besetzt.jpeg" width="640" height="480" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Der Altdorfer Wald ist besetzt."></a>

# Baumhausklimacamp in Ravensburg

* **[Professor Ertel](https://www.hs-weingarten.de/~ertel/index.php)**
  war auf unserem Baumhaus!
* Am Abend des 29.12.2020 wurden wir
  **[geräumt](/pages/Pressemitteilungen/2020-12-29-Raeumung.html){: .text-red-300 :}**.
* Am Abend des 30.12.2020 errichteten wir ein **[neues Baumhaus](/pages/Pressemitteilungen/2020-12-30-Kraft.html)**.
  Ihr könnt unsere Häuser zerstören, aber nicht die Kraft, die sie schuf!
  #nichtmituns #fightfor1point5 #wircampenbisihrhandelt
* Vorerst gilt eine [Absichtserklärung](/absichtserklaerung-bis-2021-01-10.pdf) von uns und der Polizei.
* Am 30.1.2021 pausierten wir unser Baumhausklimacamp
  **[mit einem Ultimatum](/pages/Pressemitteilungen/2021-01-22-Pause.html)**.
* Die Scientists for Future veröffentlichten ihre **[Stellungnahme zum Regionalplanentwurf](https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf)**.
* Seit dem 25.2.2021 ist der Altdorfer Wald **[besetzt](/altdorfer-wald/)**.
{: .infolist :}

[Der Regionalplan ist ein Klimahöllenplan -- Details](/regionalplan/){: .btn .btn-pink }
[Wir schützen den Altdorfer Wald, indem wir ihn besetzen](/altdorfer-wald/){: .btn .btn-purple }

Der fünfte Jahrestag des Pariser Klimaabkommens ist verstrichen und wir
befinden uns noch immer weit davon entfernt Politik zu führen, die mit dem
1,5-Grad-Ziel übereinstimmt. 

Stattdessen werden Projekte finanziert, die schon lange nicht mehr zeitgemäß
sind. So wird im Moment in Hessen ein Jahrhunderte alter Mischwald gefällt und
wozu? Für eine Autobahn. Verkehrswende geht anders! Daher streiken wir. Im
[Danni](https://waldstattasphalt.blackblogs.org/) und jetzt auch hier! Kommt
gerne dazu, auch Bodensupport ist immer gerne gesehen! 💪

Wir fragen: Mit welchem Recht nimmt sich die Stadt Ravensburg
heraus, ihr [CO₂-Restbudget](/co2-budget/) um mehr als Doppelte zu überschreiten?
#nichtmituns #fightfor1point5 #dannibleibt #wircampenbisihrhandelt

**Update**: Am Abend von
Tag 18 (29.12.) wurden wir [geräumt](/pages/Pressemitteilungen/2020-12-29-Raeumung.html),
am nächsten Tag errichteten wir ein neues Baumhaus. An Tag 50 (30.1.)
pausierten wir unser Camp mit einem Ultimatum, um der trägen Stadtverwaltung
Zeit zu geben, ihre Versprechen einzuhalten. Bis zum 1. Mai wird es von uns
kein neues Baumhaus in Ravensburg geben. Wohl aber zahlreiche Einzelaktionen, insbesondere
zum aktuellen Regionalplanentwurf. [Hier Petition
unterschreiben!](https://fairwandel-sig.de/regionalplan/) Seit dem 25.2. ist
der [Altdorfer Wald besetzt](/altdorfer-wald/).

<img src="/icons/telegram.svg" style="width: 1em; height: 1em">
**[Infokanal auf Telegram](https://t.me/klimacamp_ravensburg)**
(nur wenig Nachrichten; in der verknüpften Diskussionsgruppe auch Meldungen
über aktuelle Entwicklungen und Möglichkeit zum direkten Kontakt)

<img src="/icons/instagram.svg" style="width: 1em; height: 1em">
**[Instagram @baumbesetzung.ravensburg](https://www.instagram.com/baumbesetzung.ravensburg/)**

[Pressemitteilungen](/pressemitteilungen/){: .btn .btn-green }
[Fotos](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/){: .btn .btn-blue }
[Einladungsvideo](https://www.youtube.com/watch?v=8XOg_OQZueA){: .btn .btn-purple }
[Unsere Geschichte](/geschichte/){: .btn .btn-gray }
[Fragen und Antworten](/faq/){: .btn .btn-pink }


## Ort

Das Baumhausklimacamp befand an sich an der [Ecke
Karlstraße/Eisenbahnstraße](https://www.google.com/maps/place/47%C2%B046'59.7%22N+9%C2%B036'34.7%22E/@47.7833776,9.6093464,58m/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d47.783237!4d9.609628)
in 88212 Ravensburg (vormals [Ecke
Schussenstraße/Grüner-Turm-Straße](https://www.openstreetmap.org/note/2461517)).
Momentan ist es pausiert.

Melde dich, wir helfen dir, ein
Baumhausklimacamp in deiner eigenen Stadt zu errichten!


## Impressum

Diese Webseite wird gepflegt von: Ingo Blechschmidt, Arberstr. 5, 86179
Augsburg, [+49 176 95110311](tel:+4917695110311), [iblech@web.de](mailto:iblech@web.de). Diese Person stellt auch gerne den
Kontakt zum Baumhaus her. Das Klimacamp wird von Aktivist\*innen diverser
Klimagerechtigkeitsbewegungen gemeinschaftlich organisiert. In Deutschland gibt
es [noch viele weitere Klimacamps](https://klimacamp.eu/) und Solibaumhäuser
für den [Danni](https://waldstattasphalt.blackblogs.org/).

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>
