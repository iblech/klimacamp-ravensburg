---
layout: page
title: 🆘 Regionalplan 🆘
permalink: /regionalplan/
nav_order: 20
---

<img src="/rvbo-9.jpeg" width="1200" height="800" style="display: block; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">

# Der Regionalplan ist ein Klimahöllenplan

### Was ist der Regionalplan?

Der Regionalplan ist ein Plan, der die weitere
Entwicklung der Region bestimmt. Der momentan gültige Plan stammt aus dem Jahr
1996, in Kürze soll ein neuer verabschiedet werden. Dieser wird dann wieder für
etwa 20 Jahre gelten.

### Und was ist so schlimm daran?

Der aktuelle Regionalplanentwurf sieht Unmengen an Klima- und Umweltzerstörung
vor und ist damit ein großes Desaster für unsere Zukunft:

* 🛣 das **größte Straßenneubauprojekt** der letzten Jahrzehnte (inklusive
  **Durchschneidung von wertvollem Wald** für eine neue Bundesstraße, etwa zwischen
  Mengen und Meßkirch)
* 🌳 die teilweise **Rodung** von mehreren Wäldern, insbesondere dem Altdorfer Wald,
* 🚧 statt Flächenentsiegelung erhebliche **Neuversiegelung** (etwa 3.000
  Fußballfelder).

### Wer macht so etwas?

Dieser Klimahöllenplan wird vom [RVBO](https://www.rvbo.de/), dem
Regionalverband Bodensee-Oberschwaben, verantwortet. Skandalös:

* 📈 Das Statistische Landesamt sagt bis 2035 ein Bevölkerungswachstum von 2,7 %
  voraus, der RVBO plant aber einfach mal mit 10,3 %. Ein [Gutachten der
  Scientists](https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf)
  legt nahe, dass der RVBO absichtlich **mit Statistik trickst**, um die
  Landesamtrechnung fehlerhaft aussehen zu lassen. Dabei war sie tatsächlich
  erstaunlich akkurat!
* 🌱 Ein Antrag der Grünen, Klimaziele in den Entwurf aufzunehmen, wurde
  abgelehnt. Selbst an die bisher beschlossenen deutschen und
  baden-württembergischen **Klimaziele** (die ungenügend für 1,5 Grad sind) fühlt sich
  der RVBO **nicht gebunden**, der Verbandsdirektor gab mal zu Protokoll, dass er
  diese lediglich für gesetzlich unwirksame Absichtserklärungen halte.
* 👨‍🦳 Der RVBO ist ein ziemlich **undemokratisches Gremium**. Besetzt ist er vor
  allem aus Bürgermeistern und Altbürgermeistern von CDU und FWV. Diese
  Besetzung spiegelt die tatsächlichen Mehrheitsverhältnisse in den Gemeinderäten überhaupt nicht
  wider. Von insgesamt 63 Stimmberechtigten sind sechs Frauen.

**Aus all diesen Gründen muss der aktuelle Entwurf dringend überarbeitet
werden.**

[Stellungnahme der Scientists for Future](https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf){: .btn .btn-pink }
[Flyer](/regionalplanflyer.pdf){: .btn .btn-green }
[YouTube-Kanal](https://www.youtube.com/channel/UCJnhkewZqqKPB2x3Zu973Xg){: .btn .btn-purple }


## Was kann ich tun? Schritt 1

Gib deine Unterschrift für eine zukunftstaugliche Überarbeitung des
Regionalplanentwurfs!

[Mehr Informationen und Unterschriftenliste](https://fairwandel-sig.de/regionalplan/){: .btn .btn-blue }


## Was kann ich tun? Schritt 2

Schick dem RVBO bis zum **26. Februar 2021** eine offizielle Einwendung, per
Mail an [info@rvbo.de](mailto:info@rvbo.de) oder per Post. Du kannst die
Mustereinwendungen der Bürger\*inneninitiativen verwenden oder auch gerne was
Persönliches ergänzen:

* [Allgemein](/mustereinwendung-1.docx)
* [Klima](/mustereinwendung-2.docx)
* [Gewerbe](/mustereinwendung-3.docx)
* [Wohnraum](/mustereinwendung-4.docx)
* [Ländle4Future](https://regionbodenseeoberschwaben.blogspot.com/search/label/Muster%20Einwendungen)
* [altdorferwald.org](https://altdorferwald.org/133/informationen-veranstaltungen/2-offenlegung-regionalplan)


## Was kann ich tun? Schritt 3

Werde aktiv. Melde dich bei uns in unserer
[Telegram-Gruppe](https://t.me/joinchat/MW8ulxlUZK9yJQFY-LiBcA) und beteilige
dich an der Organisation von Protest. Gemeinsam gegen die Klimakrise! 💚

<img src="/rvbo-kralle.jpeg" width="434" height="614" style="display: block; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)" alt="Der RVBO stellt die Interessen der Wirtschaft über die Interessen der Menschen. Aber ohne Menschen keine Wirtschaft!">

<!--

## "WIE WOLLEN WIR in unserer Region LEBEN?"
### Lassen Sie sich nicht die Zukunft verbauen – reden SIE mit beim Regionalplan Bodensee-Oberschwaben!


Flyer zum selber ausdrucken: [PDF](/regionalplanflyer.pdf)
Youtube-Kanal zum Altdorfer Wald: [Alexander Knor](https://www.youtube.com/channel/UCJnhkewZqqKPB2x3Zu973Xg)


## Die Region & unsere Zukunft

Die Region Bodensee-Oberschwaben – mit ihren Landkreisen **Bodenseekreis**, **Ravensburg** und **Sigmaringen** – ist bei uns Bürgerinnen und Bürgern wie auch unseren Feriengästen aufgrund der gegebenen Lebensqualität sehr geschätzt. 
Auch in Zukunft wollen wir diese Lebensgrundlage beibehalten. Dabei müssen wir **drei globale Krisen** bewältigen: den Klimanotstand, den gravierenden Verlust der Biodiversität und den Verlust an Böden, dazu zählt auch der Verlust an freier Landschaft. Sie gefährden neben unserer Lebensqualität auch unseren Wohlstand! 
Um diese Hürden zu meistern, bedarf es eines **klimaneutralen**, **nachhaltigen Lebensstils** mit mehr Grün und mehr Bäumen. Dazu gehört auch eine drastische Reduktion des Verbrauchs an Fläche, Energie und Rohstoffen. Ein Mammutprogramm, das zu meistern ist – wenn wir jetzt handeln!

### Der Regionalplan, seine Rolle & die Politik

Der Regionalplan steuert Flächen für Wohnen, Arbeit, Verkehr, Rohstoffabbau und vieles mehr. Er spielt eine entscheidende Rolle für die zukünftige Entwicklung unserer Region. Die rechtsverbindlichen Entscheidungen bestehen für die nächsten mindestens 15 Jahre.

### Das Ziel der meisten unserer regionalen Politiker\*innen:

Bis zum Jahr 2035 sollen ca. 2.100 Hektar (Wohnen ca. 1.000, Arbeiten ca. 800 und Infrastruktur ca. 300 Hektar) für neue Bauund Verkehrsflächen ausgewiesen werden. Auch für den Rohstoffabbau (Kies, Kalkstein, Sand) sollen zusätzliche 500 Hektar Abbauflächen gesichert werden. 
Das ist ein klares „weiter so wie bisher“, welches alle wissenschaftlichen Erkenntnisse der vergangenen Jahre und die anwachsenden globalen Krisen ignoriert. 
Zudem werden die **europäischen**, **bundesdeutschen** und **baden-württembergischen Beschlüsse** und Vorgaben im Entwurf des Regionalplans **missachtet**. 
Profit und Wachstum ohne Grenzen scheinen für viele Akteur\*innen immer noch die oberste Prämisse zu sein. 
Für die Rechtfertigung von 1.000 Hektar Wohnfläche wird das vom Statistischen Landesamt prognostizierte Bevölkerungswachstum für unsere Region kurzerhand verdreifacht!

### Was ist möglich?

Kein „Weiter so“ sondern **„Erhalt“ des attraktiven Wohn- und Wirtschaftsstandorts!**
Das prognostizierte Bevölkerungswachstum lässt sich auch auf einem Drittel der im Entwurf veranschlagten Flächen realisieren – durch flächensparendes Bauen (z. B. Mehrfamilien- statt Einfamilienhäuser) und meh Innenentwicklung im bebautem Raum (z.B. Bestand modernisieren und ausbauen). 

**Klimaschutz** und **Klimaziele** müssen im Regionalplan beachtet und eingearbeitet werden.


## Wir fordern:

* Maximal 1.500 Hektar für neue Bauund Verkehrsflächen sowie Rohstoffabbauflächen wären ein gerade noch annehmbares Limit.
* Stärkung der Biodiversität, da massiver Rückgang an Insekten in BadenWürttemberg. Bei der Vogel-Population sind es im Bodenseeraum minus 25%.
* Mehr Vorrangflächen für Natur-, Boden- und Wasserschutz und nachhaltige Landwirtschaft.
* Einen regionalen Grünzug „Altdorfer Wald“ – auch in der für den Wasserschutz und die Biotopvernetzung so wertvollen „Südhälfte“ – und als Startschuß für ein Landschaftsschutzgebiet ohne Kies- und Torfabbau.
* Klimafreundliche Energie- und
Verkehrswende – mehr Bus- und Bahn-Verbindungen, Fahrrad- und Fußgängerwege.
* Weniger Energie- und Rohstoffverbrauch und mehr erneuerbare Energien.

## Wie können Sie sich einbringen?

* Informieren Sie sich über Ihre Situation und Planungen vor Ort.
* Schließen Sie sich mit anderen zusammen, organisieren Sie Veranstaltungen (z.B. Online-Veranstaltungen).
* Sprechen Sie mit den Entscheidungsträgern vor Ort – Bürgermeister\*innen, Gemeinderät\*innen, Landrät\*innen und Mitgliedern des Kreistages und des Regionalverbandes.
* Schreiben Sie Briefe oder Emails an die Entscheidungsträger.
* Schreiben Sie Pressemitteilungen / Leser\*innenbriefe an Tages-, Wochen- und Bildschirm-Zeitungen. Posten Sie News (z. B. unser share-pic) auf Instagram, Facebook, Twitter ...
* Verteilen Sie diesen Flyer und legen Sie Informationen über die örtlichen Planungen und Projekte mit den Namen der verantwortlichen Akteure dazu.


## Mehr Informationen zur Vorbereitung der Gespräche finden Sie auf folgenden Internetseiten:

* [Stellungnahme der Naturschutzverbände zum Regionalplan](https://www.bund-bodensee-oberschwaben.net/themen-projekte/naturschutz-planung/planungsvorhaben/)
* [Regionalverband BO: Fortschreibung Regionalplan](https://www.rvbo.de/Planung/Fortschreibung-Regionalplan)
* [Staatsministerium BW: Massives Insektensterben](https://www.baden-wuerttemberg.de/de/service/presse/pressemitteilung/pid/massives-insektensterben-in-baden-wuerttemberg/)
* [Max-Planck-Gesellschaft: Vogelsterben am Bodensee](https://www.mpg.de/13848390/vogelsterben-bodensee)

* [Statistisches Landesamt BW: Bevölkerungsstatistik](https://www.statistik-bw.de/BevoelkGebiet/Vorausrechnung/98015021.tab?R=RV43)
* [Mobiles BW: Mobilität im Wandel](https://www.mobiles-bw.de/)
* [Baubiologie-Magazin: Umweltschäden durch Baumaterialien](https://baubiologie-magazin.de/baubiologie-umwelt-und-klimaschutz/?highlight=Baubiologie%2C%20Umwelt-%20und%20Klimaschutz)


-----------------------------
V.i.S.d.P. Bruno Sing, BUND Bodensee-Oberschwaben, Schillerstrasse 47, 88326 Aulendorf
Kontakt: barbara.herzig@t-online.de
-->
