---
layout: page
title:  "15.10.2022: Klimaaktivist*innen spannen Banner an Rodungsfläche bei Kiesgrube"
date:   2022-10-15 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210151
#          YYMMDDX
---


*Pressemitteilung vom 15. Oktober 2022*

# Klimaaktivist*innen spannen Banner an Rodungsfläche bei Kiesgrube

Oberankenreute/Altdorfer Wald. 
Nach der erneuten Rodung zur Erweiterung der Kiesgrube Tullius bei Oberankenreute brachten Klimaaktivist*innen am heutigen Samstag um 14 Uhr zwei gut sichtbare Banner in den noch verbleibenden Bäumen neben der Straße L 317 an. Mit dem Aufdruck" Wer Bäume fällt wird Klimachaos ernten" möchten sie auf die weitere klimaschädliche Zerstörung durch die Fällung gesunder Bäume im Altdorfer Wald aufmerksam machen.
Ein zweites Banner trägt den Aufdruck "Vorsicht Klimahölle" mit einem Pfeil in Richtung des am Donnerstag geschaffenen Kahlschlags.

Lukas Häfele (35) aus Wolfegg dazu:" Einfach nur traurig, dass Zerstörung von Trinkwasser und Wald in Zeiten von Wasserknappheit auch noch von einem beschämenden Großaufgebot der Polizei ermöglicht wird." 

Am 13.10.2022 wurde in der Morgendämmerung die Landstraße 317 durch einen "überdimensional" großen Polizeieinsatz weiträumig gesperrt, um die Rodung der Bäume unter Ausschluss der Öffentlichkeit durchzuführen. Die Klimaaktivist*innen weisen darauf hin, dass die Rodung vergangenen Donnerstag nur ein kleiner Vorgeschmack ist, von dem, was an Waldvernichtung im Regionalplan auf uns zukommt. In vielen Gesprächen brachten Anwohner*innen ihren Ärger zum Ausdruck, dass durch den Polizeischutz für die klimaschädliche Rodung Steuergelder, die besser für Klimaschutzmaßnahmen eingesetzt werden können, verschwendet wurden. Rodungen werden sonst nicht im Beisein der Polizei durchgeführt. 

"Es wurde sichtbar, dass private Profitinteressen mit einem Großaufgebot an Polizeieinsatzkräften gegen den Willen vieler Anwohner*innen durchgesetzt wurden. Es macht mich betroffen, auf diese Weise die Zerstörung der Zukunft meiner Kinder und aller Menschen hilflos mit ansehen zu müssen." so Gudrun Bosch, 47, Mutter zweier Söhne aus Schlier. 

Die Anwohner*innen aus den umliegenden Gemeinden bekräftigen ihre Entschlossenheit, die bestehende Besetzung im Altdorfer Wald bei der Abzweigung Grund im dritten Winter verstärkt unterstützen zu wollen. Bei der Mahnwache am Donnerstag vor der Linse in Weingarten wurde deutlich, dass viele Menschen gegen die Rodungen für den Kiesabbau im Altdorfer Wald sind. Im Anschluss an die Mahnwache wurde der Dokumentarfilm zu den Anfängen der Altdorfer Waldbesetzung "90 Meter" von Claudio Brauchle gezeigt. In der anschließenden Podiumsdiskussion mit Klimaaktivist*innen, im ausverkauften großen Saal in der Linse, unterstrichen viele Gäste ihre Sympathie mit den Waldbesetzer*innen. 

Wer sich von einem wunderschönen, bisher noch intakten Waldstück überzeugen möchte, ist jeden Sonntag um 14 Uhr zum Waldspaziergang in die Waldbesetzung im Altdorfer Wald herzlich eingeladen.


Aktionsort:
47.799281958233934, 9.726226558811943