---
layout: page
title:  "09.05.2021: Waldbesetzer*innen danken mit Banner über L317 zum Muttertag"
date:   2021-05-09 17:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2105090
#          YYMMDDX
---

*Pressemitteilung der Altdorfer Waldbesetzung am 9. Mai 2021*

# Waldbesetzer\*innen danken mit Banner über L317 zum Muttertag

Am heutigen Muttertag (9.5.2021) spannten Klimagerechtigkeitsaktivist\*innen aus
der Altdorfer Waldbesetzung ein Banner über die angrenzende L317: "Danke Mama.
Danke Erde. Wir werden immer für euch da sein".

"Unsere Mütter müssen derzeit ganz schön was durchmachen", erklärt Samuel Bosch
(18), einer der Aktivist\*innen. Sie müssten nicht nur Sorge haben, dass beim
Baumhausbau in 20 Meter Höhe ein Unfall passiert. Am meisten macht unseren
Müttern unsere durchgehende Kriminalisierung seitens der Behörden und einiger
Kommentator\*innen zu schaffen, etwa wenn wir als 'Berufsdemonstranten'
tituliert werden", erklärt Bosch.

"Dabei sind wir vor allem Schüler\*innen und Azubis direkt aus der Region.
Zusätzlich leben einige Student\*innen und Berufstätige aus ganz Deutschland im
Wald, die dank Online-Vorlesungen bzw. Home Office ihrer Arbeit nachgehen
können." Dr. Ingo Blechschmidt (32, Mathe-Dozent an der Universität Padova),
der eines der Pressetelefone der Waldbesetzungsgemeinschaft bedient, betont:
"Keiner der Aktivist\*innen wird für sein gesellschaftliches Engagement bezahlt.
Wir jonglieren unseren ehrenamtlichen Einsatz nebst unseren anderen
Verpflichtungen und beruflichen Tätigkeiten. Nur ein paar wenige widmen derzeit
Klimagerechtigkeit ihre volle Aufmerksamkeit und sehen ihre Zeit im Wald als
freiwilliges ökologisches Jahr zwischen Schule und weiterführender Ausbildung
an. Es gibt keinen übergeordneten Verein, der die Aktivist\*innen bezahlen oder
leiten würde."

"Wir werden weiterhin auf uns und auf den Alti aufpassen", verspricht ein
anderes Banner den Müttern der Waldbesetzung. Bosch: "Danke, dass ihr uns wichtige
Werte vermittelt und uns zu den Menschen macht, die wir sind. Ihr verdient
Anerkennung und Dank, und natürlich sollte eure wertvolle Sorgearbeit genauso
entlohnt werden wie andere Arbeit auch. Wir werden uns weiterhin dafür
einsetzen, dass der Altdorfer Wald der friedfertige Ort bleibt, der er momentan
ist, und dass der Regionalplanentwurf der Altparteien CDU und FW
zukunftsfreundlich überarbeitet wird. Das ist unser Grundrecht! Wir werden uns
der gewaltvollen Zerstörung des Walds und des über tausende Jahre entstandenden
Bodens in den Weg stellen."

## Fotos zur freien Verwendung

Hier herunterladen: https://www.speicherleck.de/iblech/stuff/.alti-muttertag
(wird im Lauf des Tages ergänzt, auch durch Fotos mit höherer Bildqualität)

## Hinweis

Das Banner wurde in 14 Meter Höhe angebracht, weit über dem fließenden Verkehr.
Das verwendete Traversenmaterial (PP-Splitfilm-Tauwerk mit Durchmesser 14 mm)
hält Belastungen von bis zu 3.000 kg stand. "Die Bannerbefestigung ist
verkehrssicher", so Bosch.

## Hintergrund

Der aktuelle Regionalplanentwurf sieht Klima- und Umweltzerstörung ungeahnten
Ausmaßes vor, etwa wird das größte Straßenneubauprojekt der letzten Jahrzehnte
angestrebt, mehrere Waldflächen sollen gerodet und weitere Flächen versiegelt
(statt entsiegelt) werden. Der Entwurf wird vom Regionalverband
Bodensee-Oberschwaben bestimmt, der von der CDU dominiert ist -- obwohl in den
Gemeinden und im Land die Grünen stärkste Kraft sind. Mehr zum
Regionalplanentwurf unter https://ravensburg.klimacamp.eu/regionalplan/ und
mehr zur Waldbesetzung unter https://ravensburg.klimacamp.eu/altdorfer-wald/.

KONTAKT
Ingo Blechschmidt (+49 176 95110311) stellt gerne den Kontakt zu den
Waldbesetzer\*innen her. Es gibt keine autorisierte Gruppe und kein
beschlussfähiges Gremium, das "offizielle Gruppenmeinungen" für die
Besetzung beschließen könnte. Die Menschen in der Besetzung und ihrem
Umfeld haben vielfältige und teils kontroverse Meinungen. Diese
Meinungsvielfalt soll nicht zensiert werden, sondern gleichberechtigt
nebeneinander stehen. Kein Text und keine Aktion spricht für die ganze
Besetzung oder wird notwendigerweise von der ganzen Besetzung gut geheißen.
