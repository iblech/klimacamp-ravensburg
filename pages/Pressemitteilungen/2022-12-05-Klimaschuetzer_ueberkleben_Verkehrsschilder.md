---
layout: page
title:  "05.12.2022: Klimaschützer überkleben Verkehrsschilder"
date:   2022-05-12 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210151
#          YYMMDDX
---


*Pressemitteilung vom 1.2.2022*

+++ Sperrfrist Freitag 2.12.2022 21:00 +++

# Ravensburger Klimaaktivist*innen nehmen Tempolimit selbst in die Hand -- und sparen bislang knapp 20 Tonnen CO2 ein

In der Nacht vom vergangenen Mittwoch (30.11.2022) auf den Donnerstag (1.12.2022) überklebten Klimaaktivist*innen die vorhandenen Tempolimitschilder auf dem gesamten ausgebauten Abschnitt der B30 (zwischen Baindt und Obereschach) mit Tempo-100-Stickern. "Bundesverkehrsminister Wissing wäre eigentlich zuständig", erklärt Verkehrswendeaktivistin Lara Werner (24), "doch er und seine Partei bremsen die Verkehrswende radikal aus". Pro Tag spart die Aktion etwa 10 Tonnen CO2 ein, das ist genau so viel, wie rechnerisch ein durchschnittlicher Ravensburger über das ganze Jahr verteilt in der Atmosphäre ablagert.

Die Schwäbische hatte erst am vergangenen Samstag berichtet, dass die B30 ein Unfallschwerpunkt darstelle. In einer Mitteilung gaben die Aktivist*innen an, dass dieser Zeitungsartikel ursächlich für die Wahl des Aktionsorts war [6].

"In Zeiten der Energie- und Klimakrise blockiert die FDP selbst einfachste Sicherheitsmaßnahmen wie ein Tempolimit von 100 km/h auf Autobahnen", so Werner weiter. Nach Auskunft des Umweltbundesamts würde eine solche Begrenzung pro Jahr 5,4 Millionen Tonnen CO2 einsparen [1] – außerdem wäre der Verkehr flüssiger und Unfälle seltener tödlich oder mit schwerwiegenden Folgen [2,3]. Werner folgt dem Satiriker Jan Böhmermann, der die FDP in die inhaltliche Nähe der RAF rückte: "Die FDP ist die Verkehrswendeblockade-RAF. Sie klebt an Geschwindigkeiten von 130 km/h und mehr fest."

Nach Angaben von Werner inspirierte eine Aktion der Initiative "Letzte Generation" vom Oktober Werner und ihre Mitstreiter*innen zu dem Protest. Diese hatte auf der A2 ein Tempolimit von 100 km/h umgesetzt, indem sie mit zwei Autos parallel 100 km/h fuhr [4]. Wer wiederum die Aktion von Werner nachahmen möchte, erhalte die nötigen Sticker sowie eine ausführliche Sicherheitseinweisung jederzeit gerne. Werner freue sich über Kontaktaufnahme zum Thema.

Wie Autohaus.de berichtet, sprachen sich in einer Umfrage 80 % der Frauen und 70 % der Männer für ein Tempolimit auf deutschen Autobahnen aus [5]. Die FDP verhalte sich radikal undemokratisch, so Werner, wenn sie diesen Bevölkerungswillen nicht umsetze, nur um "Posern aus der eigenen Anhängerschaft" zu gefallen.

[1] https://www.umweltbundesamt.de/presse/pressemitteilungen/tempolimit-auf-autobahnen-mindert-co2-emissionen
[2] http://www.isa-verkehrssicherheit.de/download/183180.vision_zero_mehr_verkehrssicherheit_frak.pdf#page=10, Fußnote 12
[3] https://www.spiegel.de/auto/aktuell/tempolimit-mit-130-km-h-sinken-die-unfallzahlen-drastisch-a-1249595.html
[4] https://www.braunschweiger-zeitung.de/helmstedt/article236571513/Von-Rennau-bis-Magdeburg-Aktivisten-bremsen-Verkehr-auf-A2.html
[5] https://www.autohaus.de/nachrichten/politik/aktuelle-umfrage-tempolimit-ja-aber-bei-wieviel-km-h-3180736
[6] https://www.schwaebische.de/landkreis/landkreis-ravensburg/baindt_artikel,-es-bleibt-bei-freier-fahrt-auf-der-b-30-_arid,11580361.html

+++ Sperrfrist Freitag 1.12.2022 21:00 +++

KALKULATION
1. Nach der Bundesanstalt für Straßenwesen fahren auf der B30 pro Tag 17.145 Autos (Zählstelle Eberhardzell, Stand 2021). Hinzu kommt erheblicher Schwerlastverkehr, 1.851 Fahrten/Tag, die aber nicht in die Kalkulation einbezogen wurden, weil sie ohnehin selten schneller als 100 km/h fahren. https://www.bast.de/DE/Verkehrstechnik/Fachthemen/v2-verkehrszaehlung/Aktuell/zaehl_aktuell_node.html
2. Im Rahmen der Aktion wurde auf 20 Kilometer Strecke ein Tempolimit von 100 km/h eingeführt. Zuvor herrschte dort größtenteils 120 km/h: https://www.b30oberschwaben.de/tempolimits.html
3. Somit finden durch die Aktion nun pro Tag 17.145 * 20 km = 342.900 km Fahrtkilometer bei 100 km/h statt 120 km/h statt.
4. Laut Umweltbundesamt führt ein Fahrtkilometer bei 100 km/h durchschnittlich zu etwa 140 Gramm CO2-Emissionen. Bei 120 km/h sind es stattdessen etwa 170 Gramm, also eine Zusatzablagerung von mehr als 20 Prozent. https://www.umweltbundesamt.de/sites/default/files/medien/1410/publikationen/2020-06-15_texte_38-2020_wirkung-tempolimit_bf.pdf#page=19
5. Damit ergibt sich allein pro Tag eine CO2-Reduktion von 342900 * 40 Gramm = 10,2 Tonnen. Also sogar deutlich mehr, wie rechnerisch ein durchschnittlicher Ravensburger über das ganze Jahr verteilt in der Atmosphäre ablagert (8,5 Tonnen). https://www.ravensburg.de/rv-wAssets/pdf/wirtschaft-planen-bauen/energie-und-co2-bilanz_06_2012.pdf#page=19