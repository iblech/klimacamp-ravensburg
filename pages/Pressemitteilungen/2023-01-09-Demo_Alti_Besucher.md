---
layout: page
title:  "09.12.2022: Ab jetzt gilt Tempo 30 im Stadtgebiet Ravensburg"
date:   2022-12-09 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210151
#          YYMMDDX
---


*Pressemitteilung vom 09.12.2022*

# Ravensburg: Ab jetzt gilt Tempo 30 im Stadtgebiet Ravensburg und Tempo 100 auf der B30 – Klimaaktivist*innen nehmen Tempolimit erneut selbst in die Hand

In der Nacht vom Donnerstag (08.12.2022) auf Freitag (09.12.2022) überklebten Klimaaktivist*innen vorhandene Tempo-50 und -60 Schilder in der Ravensburger Innenstadt mit "30"er-Aufklebern. 

"Wir fordern eine autofreie Innenstadt und ein Tempolimit von 30 km/h für die restlichen Straßen im Stadtgebiet Ravensburg. Diese Maßnahmen machen das Autofahren unattraktiver. Gleichzeitig wird ein sicherer Verkehrsraum für Radfahrende und Fußgänger*innen geschaffen. Um ein Wegkommen vom Auto zu schaffen, braucht es genau das und attraktiven ÖPNV." erklärt Lara Werner (24). 

B30 Temposchilder erneut überklebt
Parallel änderten die Verkehrswendeaktivist*innen an einer Stelle auf der B30 erneut auf Tempo 100. 
"Wir müssen die meisten Geschwindigkeitsbegrenzungen auf unseren Straßen überdenken." so Werner. Die Einführung eines Tempolimits hatten bereits vergangene Woche Klimaaktivist*innen mit Aufklebern umgesetzt[1].

Schon lange fordern Umweltverbände eine deutliche Reduktion des motorisierten Individualverkehrs in den Innenstädten. In den meisten Städten gibt es auch schon erste Ansätze, weniger Autos in die Innenstadt zu locken, beispielsweise durch höherer Parkplatzpreise. Dennoch ist das Auto in deutschen Städten das Hauptverkehrsmittel und nimmt weiterhin mit Abstand den meisten Platz ein [2]. Dabei birgt es zahlreiche  Nachteile, z.B. Lärm- und Feinstaubbelastung, Ausstoß von Klimasgasen und die Gefährdung von schwächeren Verkehrsteilnehmer*innen. Weniger Platz für Autos: mehr Platz für Fußgänger*innen und Radfahrer*innen.

"Ein Tempolimit von maximal 30 km/h in Städten ist längst überfällig, um die Verkehrswende anzupacken. Nicht nur die großen Straßen und Autobahnen brauchen ein Tempolimit. Wenn die Politik es nicht hinbekommt, diese einfachen Maßnahmen umzusetzen, dann nehmen wir das eben selbst in die Hand." so Werner.

In vielen deutschen Großstädten, darunter Bonn, Leipzig, Düsseldorf und Stuttgart, wird ein solches Tempolimit bereits angestrebt. Das Problem bei der Umsetzung ist momentan unter anderem der umstrittene Paragraph 45 der Straßenverkehrsordnung (StVO). Er erlaubt beispielsweise bei Durchgangs- und Vorfahrtsstraßen eine Umgestaltung des Straßenraums nur dann, wenn die Kommunen eine konkrete Gefährdung nachweisen können – etwa für den Radverkehr. Ein entsprechender Antrag, damit dieser Missstand geändert wird, wurde bereits während der vergangenen Legislaturperiode angenommen, eine Gesetzesänderung gab es jedoch nie.

"Schon erstaunlich, dass wir in Deutschland immer gerne mit dem Finger auf andere Länder zeigen, die noch viel schlimmer sein sollen als wir, dabei können wir uns beispielsweise in Sachen Verkehrswende und Tempolimit einiges bei unseren europäischen Nachbarn abschauen. Spanien hat unter anderem seit  Mai 2021 ein Tempolimit von 30 km/h in allen Städten eingeführt. Damit erkennen diese Länder die Notwendigkeit einer Temporeduzierung in Städten, die Maßnahme führt zu weniger Verkehrstoten, weniger Feinstaub, weniger CO2 und weniger Lärm [3]."  so Simon Kuhn (31).


[1]https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaschuetzer-ueberkleben-verkehrsschilder-an-b30-zwischen-ravensburg-und-baindt-_arid,11582252.html
[2] https://www.klimareporter.de/advertorials/wie-viel-platz-nehmen-pkw-in-staedten-ein
[3] https://www.zdf.de/comedy/heute-show/what-the-fakt-tempolimit-innerorts-geschwindigkeit-unfall-verkehr-madrid-paris-helsinki-102.html