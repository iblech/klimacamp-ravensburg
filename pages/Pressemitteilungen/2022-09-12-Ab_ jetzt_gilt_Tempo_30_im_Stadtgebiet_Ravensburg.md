---
layout: page
title:  "05.01.2023: L317 wird für Anti-Kies-Demo gesperrt"
date:   2023-05-01 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210151
#          YYMMDDX
---



*Pressemitteilung  vom 05.01.2023*

# "Waldschutz, Trinkwasserschutz und Klimaschutz": L317 wird für Anti-Kies-Demo gesperrt

Da in der Region Gerüchte über eine baldige Räumung der Baumbesetzung im Altdorfer Wald bei Grund die Runde machen, wird es am Sonntag, den 08.01.2023 eine große Demonstration mit anschließendem Waldspaziergang direkt an der Besetzung geben. Das Motto der Demo ist "Waldschutz, Trinkwasserschutz und Klimaschutz". Organisiert wird die angemeldete Versammlung von einem breiten Bündnis aus Anwohner*innen und Klimaaktivist*innen die sich auch im Neuen Jahr gegen die geplante Erweiterung des Kiesabbaus im Altdorfer Wald und für den damit verbundenen Trinkwasserschutz einsetzen. Aufgrund der breiten Mobilisierung mit Flyern, Bannern, Plakaten und mehr, erwarten die Organisator*innen mehrere hundert Teilnehmer*innen.

Die Demonstration beginnt um 14:00 Uhr (Anreise 13:30 Uhr) an der Waldbesetzung bei Grund. Die Route führt direkt durch das geplante Kiesabbaugebiet. Die Landstraßen  L317 und L323 werden extra für die Versammlung gesperrt. Versammlungsteilnehmer*innen dürfen bis zum Versammlungsort durchfahren und dort auf der Landstraße parken. 
Im Anschluss an den Demonstrationszug sind alle zum großen Waldspaziergang durch die Waldbesetzung "Alti"  eingeladen. 
Die Demonstrierenden fordern die Verantwortlichen in der lokalen Politik auf, entschlossener für den Wald- und Klimaschutz einzutreten und von der weiteren Zerstörung durch großflächigen Kiesabbau abzusehen. 

"So viele Menschen und Initiativen kämpfen seit Jahren auf die unterschiedlichsten Arten gegen den Kiesabbau im Wald. Ich frage mich schon, was müssen wir noch tun,  welches Zeichen noch setzen, um zu zeigen, so geht es nicht weiter!? Ich bin es meinen Kindern und der nachfolgenden Generation schuldig, mich weiterhin aktiv für eine lebenswerte Zukunft einzusetzen. Dazu müssen wir endlich auch vom aktuellen Kurs, Richtung unendlichem Wachstum und Profit wegkommen!" so Gudrun Bosch (47) aus Schlier.

Grundlage für den weiteren Kiesabbau im Altdorfer Wald ist der vorliegende Regionalplanentwurf. Darin sind 60 Hektar weitere Waldflächen im Altdorfer Wald als Vorranggebiete für Kiesabbau ausgewiesen. Verbände, Anwohner:innen, Wissenschaftler:innen und Klimaaktivist:innen kritisieren seit Jahren, dass wertvolle  Waldflächen in Zeiten der rasant fortschreitenden Erderhitzung für Kiesexporte zerstört werden sollen. Mit Spannung erwarten die Waldbesetzer*innen und viele Bürger*innen in den kommenden Monaten die Ablehnung des Regionalplanentwurfs durch die Stuttgarter Landesregierung.

# Angesichts fortschreitender Klimaerhitzung seien die geplanten Kiesgruben "besonders kritisch"

"Ich komme aus einem südeuropäischen Land.  Diesen Sommer litten Frankreich, Portugal, Spanien, Italien und Griechenland unter dauerhaften, verheerenden Waldbränden. Aber auch ohne Brände; als ich meine Familie besucht habe, hat es mich sehr schockiert, an den Berghängen mehrere Hektar große Flächen komplett vertrockneter Bäume zu entdecken. Noch nie habe ich diesen Ort so gesehen!
Muss man aus dem Ausland kommen, um Euch den unschätzbaren Wert Eurer schönen Wälder und dem guten Trinkwasser zu erklären? Leute, meine Heimat stirbt ohne dass ich es stoppen kann und ihr zerstört diese Schätze mutwillig aus Profitgier! " empört sich Aktivistin Verena Cat (44).

Deutschland zählt weltweit zu den zehn größten CO²-Verursachern und ist mitverantwortlich für die weltweite Klimakatastrophe, die heute schon Millionen Menschen in Elend und Flucht stürzt. In ihrer Pressemitteilung unterstreichen die Veranstalter den Beitrag den unsere Region bei den internationalen Bemühungen zur Begrenzung der Erderwärmung leisten muss. Der Altdorfer Wald sei der "Amazonas Oberschwabens", den es zu bewahren und zu schützen gilt.

------------------------

PROGRAMM AUF DEN KUNDGEBUNGEN: 
Beiträge von Wolfram Frommlet, Roland Roth (Metereologe), Hermann Schad (Geologe), Manne Walser (BUND) und Waldbesetzer:innen, sowie Musik von Franziska Groß.

ORT:
Direkt an der Waldbesetzung Altdorfer Wald;
Koordinaten 47.810973, 9.76126
