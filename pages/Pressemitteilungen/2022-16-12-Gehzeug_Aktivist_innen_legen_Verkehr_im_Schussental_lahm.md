---
layout: page
title:  "16.12.2022: Gehzeug-Aktivist_innen legen Verkehr im Schussental lahm"
date:   2022-12-16 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2212161
#          YYMMDDX
---


*Pressemitteilung vom 16.12.2022*

# Gehzeug-Aktivist*innen legen Verkehr im Schussental lahm

Mit Gehzeugen wollen Aktivist:innen aus Ravensburg und dem Altdorfer Wald, am kommenden Samstag (17.12.2022), ab 10:30 Uhr, in der Ravensburger Innenstadt für mehr Tempo bei der Umsetzung einer klimaverträglichen Verkehrswende demonstrieren . Mehr Platz für Fußgänger und Radfahrer, sichere Radwege und den konsequenten Ausbau des öffentlichen Nahverkehrs sind dabei zentrale Forderungen an die Verantwortlichen in der Stadt.

"Diese Forderungen müssen sich schnellstmöglich auch in den kommunalen Investitionen in Fuß- und Radinfrastruktur sowie den ÖPNV wiederspiegeln, um endlich einen ersten Schritt Richtung klimafreundliche Mobilitätswende zu gehen. ", so  Gehzeugaktivist Robert Klauer.

"Ich möchte mich in der Innenstadt mit dem Rad oder als Fußgängerin sicher fühlen und mich in meiner Freizeit abgasfrei in der Stadt aufhalten können. Es ist höchste Zeit, dass unsere Forderungen nicht nur in Worten, sondern auch sichtbaren Taten umgesetzt werden. Was müssen wir noch tun, wie viele Bäume noch besetzen, damit das endlich verstanden wird!?" ergänzt Rosmarie Vogt.

Die Gehzeuge stehen  laut Veranstalter:innen symbolisch für das "Tempo", mit der die Stadt Ravensburg in Sachen Klimaschutz derzeit unterwegs ist . Mit der angekündigten Aktion soll gezeigt werden, dass die Stadt im Schneckentempo in der Mobilitätswende sowohl den Ravensburger Klimakonsens als auch ihren Anteil zur Wahrnehmung der 1,5°-Grenze deutlich verfehlen wird. Auf den mitgeführten Bannern sind 
Botschaften wie „Stadtverbot für Autos“ oder „Fossiler Verkehr ist verkehrt“ zu lesen.

"Als Einwohnerin der Gemeinde Schlier warte ich nun seit über 20 Jahren vergeblich darauf, dass 
Ravensburg eine Radverbindung von Fenken in die Innenstadt realisiert. Der Radweg endet seit Jahren einfach in einem Schotterparkplatz an der Ǵemeindegrenze. Das ist nur eines von vielen Beispielen, die zeigen, wie "ernst" es der Stadt wirklich ist mit der Umsetzung einer klimaschonenden Verkehrswende!" erklärt Gudrun Bosch,
Der Umzug um die Ravensburger Innenstadt startet am kommenden Samstag um 11:00 Uhr in der Parkanlage am Grünen Turm (am Baum der ersten Baumbesetzung vor zwei Jahren).
Während der Veranstaltung ist laut Veranstalter mit vorübergehenden Verzögerungen im Straßenverkehr zu rechnen. 

„Autos nehmen immer noch mit Abstand den meisten Platz in deutschen Städten ein, das ist 2022 wirklich eine traurige Bilanz in Sachen Verkehrswende. Wenn nicht endlich Maßnahmen gegen den ständig wachsenden Individualverkehr gestartet werden, fliegt uns das Klima um die Ohren.“ erklärt Klaus Schulz.