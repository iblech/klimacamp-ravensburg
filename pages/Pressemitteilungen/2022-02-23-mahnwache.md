---
layout: page
title:  "23.02.2022: Mahnwache vor Polizeirevier Weingarten anlässlich Inhaftierung von Waldverteidiger*in"
date:   2022-02-23 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2202231
#          YYMMDDX
---

*Eilmitteillung aus der Altdorfer Waldbesetzungsgemeinschaft am 23.2.2022*

# Mahnwache vor Polizeirevier Weingarten anlässlich Inhaftierung von Waldverteidiger\*in

Von den vier Aktivist\*innen, die heute morgen nach Verteidigung des akut
rodungsbedrohten Waldstücks bei Oberankenreute verhaftet wurden, sind drei
wieder frei.

Eine Person, die im Wald unter dem Spitznamen "Duplo" bekannt ist, bleibt aber
mindestens bis morgen Donnerstag 9:00 Uhr in Haft. Wie es danach weitergeht, ist
unklar. Der Person wird Nötigung und Verstoß gegen das Versammlungsgerecht
vorgeworfen.

Deswegen organisieren zahlreiche Anwohner\*innen, Unterstützer\*innen und Eltern
eine solidarische Mahnwache vor dem Polizeirevier Weingarten (Promenade 13/1,
88250 Weingarten). Aktuell sind mehr als 10 Personen vor Ort. Die Mahnwache wird
auch über Nacht gehen.

Die oben zitierte rechtliche Einschätzung ist einseitig. Denn der Kieskonzern
Tullius sowie alle unterstützenden Behörden betreiben Raubbau an der Natur und
missachten so das wegweisende Bundesverfassungsgerichtsurteil zur
Klimagerechtigkeit aus dem letzten Jahr. Sie befeuern die Erdaufheizung und
gefährden so unsere öffentliche Ordnung. Anders als Duplo stehen sie aber nicht
vor Gericht.

Die regionale Nachfrage nach Kies blieb trotz Bauboom weit hinter den
Erwartungen zurück. Kies wird nach Österreich und in die Schweiz exportiert, wo
Umweltauflagen den Kiesabbau unrentabel machen. Ein Zentimeter Waldboden
benötigt 100 Jahre in der Entstehung und ist nicht ersetzbar.