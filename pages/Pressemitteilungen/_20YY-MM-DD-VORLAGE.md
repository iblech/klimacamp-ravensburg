---
layout: page
title:  "DD.MM.20YY: TITEL"
date:   20YY-MM-DD 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: YYMMDD0
#          YYMMDDX
---

*Pressemitteilung vom 20. Januar 2022*

# TITEL

Sperrfrist Donnerstag (20.01.2022) 9:00 Uhr
{: .label .label-red }

TEXT bla bla QUELLE1 [[1]] bla bla

`WICHTIG: * mit \* ersetzen`

## Quellen
[1]: https://www.bbc.com
[1] [https://www.bbc.com][1]
 
[2]: https://www.swrfernsehen.de
[2] [https://www.swrfernsehen.de][2]