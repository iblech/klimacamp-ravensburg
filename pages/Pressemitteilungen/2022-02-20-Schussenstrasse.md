---
layout: page
title:  "20.02.2022: Kletteraktivist Samuel Bosch nach Ingewahrsamnahme im Mai wegen Banneraktion über der Schussenstraße vor Gericht"
date:   2022-02-20 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2202200
#          YYMMDDX
---

*Pressemitteilung vom 20. Januar 2022*

# Kletteraktivist Samuel Bosch nach Ingewahrsamnahme im Mai wegen Banneraktion über der Schussenstraße vor Gericht

Diesen Dienstag (22.02.2022) ab 8:30 Uhr steht Klimaaktivist Samuel Bosch (19)
zum zweiten Mal vor dem Amtsgericht in Ravensburg. Es handelt sich um den von
Klimaaktivist\*innen lange ersehnten Prozess zur Banneraktion über der
Schussenstraße, die im Mai 2021 für viel Aufsehen sorgte. 

Am Morgen des 15.05.2021 spannten drei Aktivist\*innen eine sogenannte
"Traverse" (waagrechtes Sicherungsseil) zwischen zwei Bäumen über die viel
befahrene Schussenstraße mitten in Ravensburg. "Unter mir fuhr bis 1959 eine
elektrische Straßenbahn ('s Bähnle). Heute wälzt sich da 24 h/Tag eine
verschmutzende und verlärmende Blechlawine. Wir fordern, dass die Straßenbahn
zwischen Ravensburg und Baienfurt wieder aufgebaut wird", so Samuel Bosch (19)
damals im Interview aus der Traverse. Die verkehrssicher befestigten Banner
sollten Bürger\*innen über die "Blockadehaltung" von Ravensburgs
Mobilitätspolitik informieren.

"In dem öffentlichen Prozess werden endlich die Fakten ans Licht kommen", drückt
Bosch seine Hoffnung aus. Er bezieht sich mit seiner Aussage auf einzelne
Medienhäuser sowie "Falschaussagen der Polizei", die etwa von gefährlichen
Stahlseilen sprach. "Tatsächlich werden Traversen im Klimaaktivismus nie mit
Stahlseilen errichtet. In unseren Pressemitteilungen gaben wir sogar die exakten
technischen Daten des stattdessen von uns verwendeten besonders baumschonenden
und ultraleichten Polypropylenseils an." Bosch kritisiert auch, dass einzelne
Medien anders als DPA, Südkurier und Wochenblatt News kaum über den politischen
Aktionshintergrund berichteten, Banner mit inhaltlichen Forderungen etwa nur
unlesbar von hinten abdruckte und nie den Zustand der Ravensburger
Mobilitätswende thematisierte.

Bosch erhofft sich von dem Prozess auch, die Logik der Aktion erklären zu
können. "Die Polizei ließ damals nur noch Busse durch, für Autos blockierte sie
die Schussenstraße. Durch diese unprofesionelle Überreaktion griff sie erheblich
in die Versammlungsfreiheit ein, da sie uns das ursprünglich angedachte Publikum
entzog. Tausende Autofahrer\*innen wären sonst von uns über die Blockadehaltung
von Ravensburgs Mobilitätspolitik informiert worden!" Nach Ansicht von
Rechtsanwalt Klaus Schulz, der Bosch vor dem Amtsgericht vertritt, war die
Totalsperrung der Schussenstraße "unnötig". "Auf der Suche nach dem mildesten
Mittel sollte man nicht den Holzhammer zücken", so Schulz.


## Zur Aktion und den Ereignissen

Am Morgen des 15.05.2021 spannten drei junge Aktivist\*innen acht Meter über der
Schussenstraße ein Banner mit der Aufschrift „Wer Straßen sät, wird Stau
ernten“, um gegen die „tosende Autostraße“ im Zentrum Ravensburgs zu
demonstrieren. Hierbei platzierten sie das Banner für alle
Verkehrsteilnehmer\*innen gut sichtbar hoch über der vierspurigen Straße.
Anschließend sperrte die Polizei die Straße für alle Autos. Nur noch Busse und
Rettungsfahrzeuge durften passieren.

„Leider sahen durch die Sperrung der Straße viel weniger Menschen das Banner,
als wir gehofft hatten“, so die ebenfalls angeklagte Aktivistin „Kolibri“ zur
Aktion. Die Aktion entwickelte sich zu einem bunten Straßenfest. „Es war ein
toller Nachmittag. Aktivist\*innen und Passant\*innen spielten Ball und
musizierten“, erinnert sich „Kolibri“. „Wir führten viele Gespräche mit
Anwohner\*innen und Passant\*innen. Viele fanden die Aktion und die fröhliche
Stimmung sehr schön“, so Aktivist Dr. Ingo Blechschmidt (33). Ein älterer
Anwohner wiederholte immer wieder „Es ist so leise hier, so leise!“, erzählt
Blechschmidt weiter.

Die Aktion wurde am Nachmittag von der Polizei aufgelöst. Die Aktivist\*innen,
welche morgens das Banner aufgehängt hatten, wurden anschließend für 24 Stunden
nach Friedrichshafen in Gewahrsam gebracht. Die Aktion reihte sich in eine
Vielzahl anderer Aktionen zum Thema „Mobilitätswende“ ein. Samuel Bosch (19)
erklärt: „Unser Ziel ist eine sozial gerechte Mobilitätswende für Ravensburg.“
Jahrelang haben verschiedene Gruppen und Initiativen „viele konkrete Vorschläge“
an die Stadt Ravensburg gemacht. „Jetzt ist die Stadt am Zug“, so Bosch weiter.
Die Aktivist\*innen hätten nicht das Ziel, Privatpersonen Autos zu verbieten.
Vielmehr müsse die Politik „dringendst“ in umweltverträgliche Alternativen
investieren.


## Zur Verhandlung und den Vorwürfen

Samuel Bosch wird Nötigug (§240 StGB), Hausfriedensbruch (§123 StGB) sowie die
Leitung einer unangemeldeten Versammlung (§26 VersG) vorgeworfen. Verteidigt
wird er von Klaus Schulz. Die Verhandlung beginnt um 8:30 Uhr vor dem
Amtsgericht Ravensburg (Herrenstraße 40-44).


## Mahnwache

Vor dem Amtsgericht findet am Dienstag (22.2.) ab 7:45 Uhr eine
angemeldete Versammlung mit dem Motto "Trotz Prozess blockiert
Ravensburg weiterhin die Mobilitätswende, für mehr und stärkere
Traversen zwischen Ravensburg und Klimagerechtigkeit!" statt.