---
layout: page
title:  "02.11.2022: L317: Anwohner :innen bremsen Klimasünder mit Gehzeugen aus"
date:   2022-11-02 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2211021
#          YYMMDDX
---


*Pressemitteilung vom 02. November 2022*

# L317: Anwohner :innen bremsen Klimasünder mit Gehzeugen aus

Mit einer symbolischen Aktion der besonderen Kunst auf der L317 am heutigen Mittwoch (2. November 2022) drückt t en Anwohner:innen aus Oberankenreute, Schlier , Weingarten und Wolfegg ihre Empörung über "business-Zerstörung as usual" von Forst BW, Lokalpolitik und privatwirtschaftliche Interessen in Zeiten der „ Klimakatastrophe “ aus. Mit sogenannten "Gehzeugen" [[1]](https://www.deutschlandfunknova.de/beitrag/gehzeug-bauen-zeigen-wie-viel-platz-ein-auto-einnimmt){:target="_blank"}, Holzgestellen in der Größe eines PKWs, die zu Fuß getragen Werden und auf der Straße nach § 25 StVO auf der Straße transportiert werden müssen, verlangsamten sie für mehr als eine Stunde den Lasterverkehr bei der Kiesgrube Tullius bei Oberankenreute. Die Gruppe aus 35- bis 70- jährigen Bürger:innen stellt sich gegen die ungebremste Ausbeutung des Altdorfer Waldes durch Kieskonzerne.


Mit Botschaften wie „Eure Gewalt gegen das Leben ist unermesslich! Kapitalterroristen“ und „Der Weg zur Klimahölle wird gekiest“ fordern sie Wald- und Klimaschutz über die Partikularinteressen einzelner Konzerne zu stellen. „Unfassbar, dass hier wertvoller Staatswald, der der Allgemeinheit gehört, für private Profite geopfert wird. Was muss noch passieren, damit sich Politik eindeutig dem Gemeinwohl verpflichtet und Klimaschutz ernst nimmt?“, fragt Martin Lang (56) aus Oberankenreute. 

Den Ort für ihre Aktion hat die Gruppe bewusst gewählt. In den frühen Morgenstunden des 13. Oktober dieses Jahres ließ ForstBW mit Hilfe eines massiven Polizeiaufgebotes drei Hektar Buchen- und Mischwaldbestände für die Erweiterung der Kiesgrube Tullius roden [[2]](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-polizei-sichert-rodungen-im-altdorfer-wald-_arid,11564393.html){:target="_blank"}. Gudrun Bosch (47) aus Schlier dazu: „Alle, die hier spazieren gehen, Sport treiben und den Wald als ihr Naherholungsgebiet nutzen, sehen, wie hier die Natur vor der eigenen Haustür zerstört wird. So etwas auch noch in Zeiten der Klimakrise zu genehmigen, ist ein absolutes Unding und nicht verständlich. Selbst der wertvolle Waldboden ist von den schweren Rodungsfahrzeugen völlig kaputt. Ich frage mich: Wie erklären wir das unseren Kindern und Enkeln?"

Boschs Mitstreiterin Rosemarie Vogt (70), Lehrerin im Ruhestand, ergänzt: "Die Klimakrise schreitet ungebremst voran und unsere Politiker schlagen alle Warnungen und Empfehlungen der Wissenschaft in den Wind. Verantwortungslos und kriminell, entgegen besserem Wissen, einfach weiterzumachen wie bisher!" Lukas Häfele (35) aus Wolfegg stimmt ihr zu und greift Ravensburgs Oberbürgermeister verbal an: "Oberbürgermeister Rapp erzählt uns Märchen von „Ravensburg bla 2040 bla bla Klimaneutral“. Die traurige Realität: Die Baubranche - ein Klimakiller - macht unbehelligt weiter und zerstört artenreiche Wälder für private Profite. Solange wir so viel ungenutzten Wohnraum haben, muss bei Kies gespart werden."

FORDERUNGEN DER ANWOHNER:INNEN IM BESONDEREN:
- Kiesausbeute im Altdorfer Wald und anderen Wäldern sofort beenden
- Bodenschonende Waldbewirtschaftung (Harvester raus aus dem Wald!)
- Ausweisung weiterer Klimaschutzzonen im Wald
- keine weiteren Kahlschläge in Zeiten der Klimakrise
- Kieskonzerne zur Offenlegung ihrer Kiesexporte 
verpflichten 
- Umweltabgabe für Kies, so wie es in Österreich üblich ist

## Quellen
[1] https://www.deutschlandfunknova.de/beitrag/gehzeug-bauen-zeigen-wie-viel-platz-ein-auto-einnimmt

[2] https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-polizei-sichert-rodungen-im-altdorfer-wald-_arid,11564393.html
