---
layout: page
title:  "11.12.2021: Klimaaktivist*innen jeden Alters besetzen zahlreiche Bäume in Ravensburg Innenstadt"
date:   2021-12-11 11:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2112110
#          YYMMDDX
---

*Pressemitteilung vom 11. Dezember 2021*

# Klimaaktivist\*innen jeden Alters besetzen zahlreiche Bäume in Ravensburg Innenstadt

Sperrfrist Samstag (11.12.2021) 11:00 Uhr
{: .label .label-red }

*Einladung zur Pressekonferenz: Samstag 11.12.2021 11:00 in der Ravensburger
Innenstadt, genauer Ort wird Samstagfrüh auf der unten verlinkten Website sowie
vorab telefonisch bekanntgegeben*

Am heutigen Samstag (11.12.2021) besetzten fünfzehn Klimaaktivist\*innen in der
Ravensburger Innenstadt jeweils einen Baum, um gegen die aus ihrer Sicht zu
langsame Umsetzung der Ravensburger Klimaziele zu protestieren.  Die Aktion
findet genau ein Jahr nach der Errichtung des ersten Ravensburger
Klimaprotestbaumhauses statt [[1],[2]] und weist auf den sechsten Jahrestag des
Pariser Klimaabkommens hin, das am 12. Dezember 2015 verabschiedet wurde. Die
Aktivist\*innen haben gemeinsam mit dem Scientists for Future eine Liste an
Forderungen an die Stadt Ravensburg erstellt. Dort sind verschiedene Vorschläge
für Maßnahmen aufgezählt, welche ihrer Meinung nach ergriffen werden müssen und
können, die 1,5°-Grenze nicht zu überschreiten.

"Wenn Dr. Rapp und der Gemeinderat weiterhin keine effektiven Maßnahmen
ergreifen, fliegt uns das Klima in wenigen Jahren um die Ohren", so Samuel Bosch
(18). "Keiner unsererer Vorschläge bedarf Hexerei zur Umsetzung. Der
Ravensburger Gemeinderat könnte schon morgen anfangen diese wichtigen Maßnahmen
zur Einhaltung des 1,5°C-Limits umzusetzen", führt Charlie Kiehne (19) weiter
aus.

Eines hat sich indes doch geändert: War es 2020 noch ein einzelner Baum, der von
jugendlichen Klimaschützer\*innen besetzt wurde, protestieren heute
Vertreter\*innen aller Altersgruppen auf einem Dutzend städtischer Bäume für
mehr Klimagerechtigkeit. Sie engagieren sich aus den verschiedensten Gründen:

"Ich fände es beschämend, wenn wir die Verhinderung des drohenden Klimakollaps
allein der jungen Generation aufbürden. Zu meiner Zeit verschlief die Politik
die auch schon, Rahmenbedingungen für klimafreundliches Handeln zu schaffen", so
der 55-jährige Familienvater Martin Lang. 

"Wenn wir jetzt nicht endlich handeln, wird mein einjähriger Sohn eine Jugend
geprägt von Umweltkatastrophen, Welthunger und Kriegen erleben. Wir können nicht
tatenlos zusehen, wie uns und unseren Kindern die Zukunft genommen wird", sagt
Maraike Siebert (23), Mutter und Auszubildende aus Ravensburg.

"Natürlich können wir in Ravensburg nicht die Versäumnisse auf Bundes- und
internationaler Ebene kompensieren, was aber Ravensburg im vergangenen Jahr,
trotz zahlreicher Proteste, vorlegte ist blamabel und entspricht weder dem Geist
noch den Minimalzielen von Paris", so Hannah Schak (21).

## Quellen
[1]: https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-ziehen-bilanz-wir-erlebten-grossen-zuspruch-_arid,11322187.html
[1] [https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-ziehen-bilanz-wir-erlebten-grossen-zuspruch-_arid,11322187.html][2]

[2]: https://www.ardmediathek.de/video/landesschau-baden-wuerttemberg/landesschau-baden-wuerttemberg-vom-7-1-2021/swr-bw/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODE4OTM/
[2] [https://www.ardmediathek.de/video/landesschau-baden-wuerttemberg/landesschau-baden-wuerttemberg-vom-7-1-2021/swr-bw/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODE4OTM/][2]

## Koordinaten der Bäume und der Pressesprecher*innen
Hinweis: Die Aktion dauerte wie geplant bis 13 Uhr und lief wie erwartet ab.
Es gab viele nette Gespräche mit Passant\*innen.

Positionen der Bäume:
- Charlie Kiehne (19):  gegenüber von der Nordsee
- Martin Lang (55): vor Blaserturm, Kreissparkasse
- Samuel Bosch (18):   nahe 2.Solibaumhaus, Karls str. (Koordinaten 47.783155,9.609461)
- Maraike Siebert (23): wurde von der Polizei gehindert
- Hannah Schak (21): Kreuzung Frauentor...Beginn Schussenpark ...Nähe grüner Turm 
- Klaus Schulz: gegenüber Schwäbisch Media
- Kia vor Stadtbibliothek
- "Opa": hinter dem Lederhaus
- Sebastian: an der Karlstraße
- Prof. Dr. Wolfgang Ertel wurde von Polizei gehindert

Acht der zwölf Menschen wurden nicht von der Polizei gehindert und konnten wie
geplant auf ihren Baum klettern.

Geplant wurde die Aktion von mehreren Aktivist*innen, Samuel Bosch war nicht
Aktionsleiter! Neben Samuel Bosch gibt es noch weitere Menschen die für die
Aktion Presse sprechen.

## Hinweise zu den Pressesprecher\*innen 
Charlie Kiehne (19) ist Klimaaktivistin und erfahrene Kletterin. Im vergangenen
Jahr war sie unter anderem im Zuge von politischen Aktionen auf dem
Brandenburger Tor in Berlin und auf der Basilika in Weingarten um für
Klimagerechtigkeit zu kämpfen. Nach ihrem Abitur nimmt sie sich im Moment ein
Jahr Zeit zur Orientierung und um sich vollkommen dem Aktivismus zu widmen.  

Samuel Bosch (18) is der stadtbekannte Klimaaktivist der unter anderem das erste
Baumhaus in Ravensburg und die Besetzung im Altdorfer Wald mitbegründet hat
sowie bei vielen (Banner-)Aktionen in der Region und bundesweit beteiligt war.

Martin Lang (55) unterstützte zunächst als Anwohner die Aktivist*innen im
Altdorfer Wald, macht mittlerweile aber auch selbst bei Kletteraktionen mit, wie
beispielsweise im September in Weingarten auf der Basilika.

Maraike Siebert (23) ist Mutter aus Ravensburg und engagiert sich im Rahmen von
verschiedenen Aktionen für Klimagerechtigkeit in ihrer Heimatstadt.

Hannah Schak (21) lebte mehrere Monate in der Waldbesetzung im Altdorfer Wald um
sich so und mit verschiedenen Aktionen gegen die Rodung durch den Kiesabbau zu
engagieren.