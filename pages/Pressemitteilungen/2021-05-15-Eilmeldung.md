---
layout: page
title:  "15.05.2021: Eilmeldung vom Ravensburger Klimacamp"
date:   2021-05-15 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2105150
#          YYMMDDX
---

*Eilmeldung vom Ravensburger Klimacamp am 15.5.2021*

## Eilmeldung vom Ravensburger Klimacamp

Klimagerechtigkeitsaktivist\*innen spannten am heutigen Samstagmorgen eine
Traverse über die vierspurige Schussenstraße (Ecke Schützenstraße) in
Ravensburg. Mit dem an der Traverse befestigten Banner "Wer Straßen sät, wird
Verkehr ernten" fordern die Aktivist\*innen, dass die Regierung zügig eine echte
Mobilitätswende in Gang setzt und alle Straßenneubauprojekte sofort einstellt.
Aktivist\*innen befinden sich in Hängematten in der Traverse oberhalb der
Straße.

"Die einseitige Bevorzugung des Autoverkehrs muss aufhören", erklärt Aktivist
Samuel Bosch (18) die Aktion. "Die Hälfte der Deutschen besitzt kein Auto! Die
Stadt muss den öffentlichen Personennahverkehr massiv ausbauen und vergünstigen
sowie ein durchgängiges und sicheres Radwegenetz schaffen, damit sich alle
Bürger\*innen Ravensburgs klimafreundliche Mobilität zeitlich und finanziell
leisten können. Vierspurige Straßen haben in Städten nichts zu suchen."

Die Aktivist\*innen befinden sich weit oberhalb des Lichtraums der Straße (der
erstreckt sich laut Gesetz lediglich bis 4,70 Meter über dem Boden) und
beeinträchtigen nicht den darunter befindlichen Verkehr. Die Polizei sperrte die
Schussenstraße trotzdem in beide Richtungen. Das verwendete Traversenmaterial
(PP-Splitfilm-Tauwerk mit Durchmesser 14 mm) hält Belastungen von bis zu 3.000
kg stand. "Die Traverse ist verkehrssicher", so Bosch.