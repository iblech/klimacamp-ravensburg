---
layout: page
title:  "16.04.2021: Klimagerechtigkeitsaktivist*innen fordern mit Innenstadtbanneraktion einen sofortigen Stopp des umstrittenen 1.000-Kühe-Stalls"
date:   2021-04-16 08:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2104161
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 16. April 2021*

# Klimagerechtigkeitsaktivist\*innen fordern mit Innenstadtbanneraktion einen sofortigen Stopp des umstrittenen 1.000-Kühe-Stalls

Diesen Samstag (17.4.2021) werden Klimagerechtigkeitsaktivist\*innen ein Banner
mit der Aufschrift "Kein Megastall in Ostrach und anderswo!" in der
Ravensburger Innenstadt (Marienplatz, in der Nähe des Schnellrestaurants Nordsee)
anbringen. Über dieses geplante Umwelt- und Klimazerstörungsprojekt wurde schon
ausführlich berichtet [1,2], seit vielen Jahren arbeitet das "Aktionsbündnis
gegen den 1000-Kühestall Ostrach" zu diesem Thema.

Die Banneraktion ist Teil einer Abmachung mit der Stadt Ravensburg: Im Gegenzug
dazu, dass die Ravensburger Klimacamper\*innen ihr Baumhaus am 30.1.2021 ihr
Innenstadtbaumhaus abbauten, wurde ihnen zugesichert, vier Banner im
Stadtgebiet anbringen zu können [3].

Im Vorfeld gab es hinsichtlich dieser Aktion Differenzen mit Ordnungsamtleiter
Alfred Oswald (+49 751 82-291, alfred.oswald@ravensburg.de), die nicht
ausgeräumt werden konnten. Oswald genehmigte das Banner bislang nicht. Die
Aktion findet trotzdem statt. Interessierte sind eingeladen, die Aktion zu
verfolgen und bei der Gelegenheit die nötigen Kletterfähigkeiten zu erlernen,
sofern sie den Corona-Mindestabstand deutlich überschreiten und durchgehend
FFP2-Maske tragen.

"Wir möchten alle Ravensburger\*innen zu kreativem Protest befähigen", so
Klimacamper Samuel Bosch (18). "Unsere Stadtregierung ist um ihr grünes Image
bemüht, ist insgeheim jedoch wissenschaftsfeindlich und arbeitet mit viel
Energie gegen das Gebot, die Schöpfung zu bewahren", so Bosch weiter. "Deswegen
rufen wir alle Ravensburger\*innen auf, zivilen Ungehorsam zu leisten und sich
der Klimazerstörung durch die Stadt Ravensburg in den Weg zu stellen."

[1] https://www.bund-bodensee-oberschwaben.net/themen-projekte/naturschutz-planung/1000-kuehe-stall/<br>
[2] https://satiresenf.de/sts10619-demo-gegen-1-000-kuehe-stall-dilettantentango-fuer-alle-nachrichtensperre-fuer-sase/<br>
[3] https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-raeumen-ihr-lager-_arid,11319583.html


## Über das Ravensburger Klimacamp

Das Ravensburger Klimacamp entstand am 12.12.2020 als acht Meter hohes
Baumhaus in Ravensburg. Am Abend vom 29.12. wurde es geräumt, am
nächsten Tag neu errichtet. Am 30.1. pausierten die
Klimagerechtigkeitsaktivist\*innen ihr Baumhaus und hinterließen der
Stadt Ravensburg ein Ultimatum bis zum 1.5. Am 25.2. initiierte die Gruppe eine
Besetzung des Altdorfer Walds, um diesen vor Rodung zu schützen. Zur Gruppe
gehören Klimagerechtigkeitsaktivist\*innen diverser Initiativen.


## Kontakt

Ingo Blechschmidt (+49 176 95110311)
