---
layout: page
title:  "14.12.2022: Vier Baumbesetzer*innen wegen angeblicher Nötigung von Harvester angeklagt"
date:   2022-12-14 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2212141
#          YYMMDDX
---


*Pressemitteilung vom 14.12.2022*

# Rodung für Kiesgrube Tullius: Vier Baumbesetzer*innen wegen angeblicher Nötigung von Harvester angeklagt

Am Donnerstag, den 15.12.2022, ab 8:30 Uhr wird am Amtsgericht Ravensburg gegen die vier Klimaaktivist*innen Samuel Bosch, Patrick Sander, Leander John und Martin Lang verhandelt.
Im Februar besetzten sie ein Waldstück bei Oberankenreute, um dessen Rodung für den Kiesabbau zu verhindern. Durch die selbst gespannten Seile und Hängematten zwischen den Bäumen konnten sie den Wald zwei Nächte und einen Tag lang besetzt halten. Das erklärte Ziel der Besetzung war, die Rodung bis zum Ende der bevorstehenden Rodungssaison hinauszuzögern. Jetzt wird den Aktivist*innen unter anderem Nötigung und ein Verstoß gegen das Versammlungsgesetz vorgeworfen.

"Wir hätten in dem gewonnenen Jahr dann gegen die Rodung des Waldes klagen können. Damit hätten wir den Wald vor der Zerstörung durch die Erweiterung der Kiesgrube Tullius bewahren und somit ein weiteres klimaschädliches Projekt verhindern können." so Samuel Bosch (19).

"Waldrodung in Zeiten der Klimakatastrophe ist Raubbau an unserer Lebensgrundlage und respektlos denen gegenüber, die heute schon massiv unter den Folgen der Klimakatastrophe leiden. Dass die Bäume auch noch für Kies und damit die Zementindustrie fallen, die global mit 8% der Emissionen maßgeblich zur Erderwärmung beiträgt, frustriert mich besonders." ergänzt Leander John (25)

Die Kiesgrube Tullius bei Oberankenreute im Altdorfer Wald ist bereits über 30 Hektar groß. An der Stelle der kurzen Besetzung war Ende 2021 die Abbaugenehmigung ausgelaufen. Wegen geringerer Nachfrage war eine Teilfläche noch unversehrt geblieben, für die der Betreiber kurz zuvor eine Verlängerung der Abbaugenehmigung von der Gemeinde Schlier erhalten hatte. Im Vorfeld zu dieser Entscheidung hatte es bereits Proteste aus der Bevölkerung gegeben. Besonders unverständlich finden die Baumbesetzer*innen, dass das Unternehmen, trotz fehlender Nachfrage, im neuen Regionalplan bereits 30 weitere Hektar Wald als Kiesabbaufläche beantragt hat.


Hundertschaft und SEK rückten zur Räumung an
Um das besetzte Waldstück für die Rodung frei zu machen, rückte die Polizei am frühen Morgen des folgenden Tages mit einem Großaufgebot von rund 200 Einsatzkräften samt Hundestaffel an. Die gesamte Straße entlang der Grube war stundenlang für die Bevölkerung gesperrt, das Waldstück weiträumig abgeriegelt.

"Wenn pro Aktivisti, friedlich in einer Hängematte liegend, rund 50 Polizeibeamte aufgefahren werden, kann man nur von Steuergeldverschwendung sprechen. Der Staat sollte endlich in die Reduzierung schädlicher Emissionen investieren anstatt legitime Proteste von Klimaschützenden zu kriminalisieren" so Martin Lang (56)


Mahnwache
In Solidarität mit den Angeklagten, laden am Donnerstag, 15.12., ab 8:00 Uhr Unterstützende zu einer angemeldeten Mahnwache vor dem Amtsgericht, unter dem Motto "Bäume fallen für den Kiesabbau", ein.
.

Die Waldbesetzung bei der Abzweigung Grund/Vogt im Wald, der "Alti" , besteht weiterhin.
-------------------------

Bosch, Sander, John und Lang sind mit der namentlichen Nennung auch in Bezug auf den Prozess einverstanden.

ORT: 
Herrenstraße 42, 88212 Ravensburg; 
Koordinaten: 47.78127652286534, 9.617143267446817

FOTOS UND VIDEOS ZUR FREIEN VERWENDUNG
https://drive.google.com/drive/folders/1OcWdRprYoJhVcW5vaFMGpk9WjWYO0nG9

KONTAKT
Samuel Bosch +49 15908156028
Martin Lang +49 176 41065932
Leander John +49 176 57519279


Am Donnerstag, den 15.12.2022, ab 8:30 Uhr wird am Amtsgericht Ravensburg gegen die vier Klimaaktivist*innen Samuel Bosch, Patrick Sander, Leander John und Martin Lang verhandelt.
Im Februar besetzten sie ein Waldstück bei Oberankenreute, um dessen Rodung für den Kiesabbau zu verhindern. Durch die selbst gespannten Seile und Hängematten zwischen den Bäumen konnten sie den Wald zwei Nächte und einen Tag lang besetzt halten. Das erklärte Ziel der Besetzung war, die Rodung bis zum Ende der bevorstehenden Rodungssaison hinauszuzögern. Jetzt wird den Aktivist*innen unter anderem Nötigung und ein Verstoß gegen das Versammlungsgesetz vorgeworfen.

"Wir hätten in dem gewonnenen Jahr dann gegen die Rodung des Waldes klagen können. Damit hätten wir den Wald vor der Zerstörung durch die Erweiterung der Kiesgrube Tullius bewahren und somit ein weiteres klimaschädliches Projekt verhindern können." so Samuel Bosch (19).

"Waldrodung in Zeiten der Klimakatastrophe ist Raubbau an unserer Lebensgrundlage und respektlos denen gegenüber, die heute schon massiv unter den Folgen der Klimakatastrophe leiden. Dass die Bäume auch noch für Kies und damit die Zementindustrie fallen, die global mit 8% der Emissionen maßgeblich zur Erderwärmung beiträgt, frustriert mich besonders." ergänzt Leander John (25)

Die Kiesgrube Tullius bei Oberankenreute im Altdorfer Wald ist bereits über 30 Hektar groß. An der Stelle der kurzen Besetzung war Ende 2021 die Abbaugenehmigung ausgelaufen. Wegen geringerer Nachfrage war eine Teilfläche noch unversehrt geblieben, für die der Betreiber kurz zuvor eine Verlängerung der Abbaugenehmigung von der Gemeinde Schlier erhalten hatte. Im Vorfeld zu dieser Entscheidung hatte es bereits Proteste aus der Bevölkerung gegeben. Besonders unverständlich finden die Baumbesetzer*innen, dass das Unternehmen, trotz fehlender Nachfrage, im neuen Regionalplan bereits 30 weitere Hektar Wald als Kiesabbaufläche beantragt hat.


Hundertschaft und SEK rückten zur Räumung an
Um das besetzte Waldstück für die Rodung frei zu machen, rückte die Polizei am frühen Morgen des folgenden Tages mit einem Großaufgebot von rund 200 Einsatzkräften samt Hundestaffel an. Die gesamte Straße entlang der Grube war stundenlang für die Bevölkerung gesperrt, das Waldstück weiträumig abgeriegelt.

"Wenn pro Aktivisti, friedlich in einer Hängematte liegend, rund 50 Polizeibeamte aufgefahren werden, kann man nur von Steuergeldverschwendung sprechen. Der Staat sollte endlich in die Reduzierung schädlicher Emissionen investieren anstatt legitime Proteste von Klimaschützenden zu kriminalisieren" so Martin Lang (56)


Mahnwache
In Solidarität mit den Angeklagten, laden am Donnerstag, 15.12., ab 8:00 Uhr Unterstützende zu einer angemeldeten Mahnwache vor dem Amtsgericht, unter dem Motto "Bäume fallen für den Kiesabbau", ein.
.

Die Waldbesetzung bei der Abzweigung Grund/Vogt im Wald, der "Alti" , besteht weiterhin.
-------------------------

Bosch, Sander, John und Lang sind mit der namentlichen Nennung auch in Bezug auf den Prozess einverstanden.

ORT: 
Herrenstraße 42, 88212 Ravensburg; 
Koordinaten: 47.78127652286534, 9.617143267446817