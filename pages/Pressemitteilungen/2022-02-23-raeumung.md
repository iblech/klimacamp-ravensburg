---
layout: page
title:  "23.02.2022: Räumung und Rodung des notbesetzten Waldstücks bei Oberankenreute"
date:   2022-02-23 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2202230
#          YYMMDDX
---

*Eilmitteillung aus der Altdorfer Waldbesetzungsgemeinschaft am 23.2.2022*

# Räumung und Rodung des notbesetzten Waldstücks bei Oberankenreute

Am heutigen Dienstagmorgen (23.2.2022) umstellten um 7:30 Uhr Polizeieinheiten
aus 30 bis 50 Polizeibussen die neue Not-Besetzung im Altdorfer Wald bei der
Kiesgrube "Tullius" bei Oberankenreute und räumten diese. Aktivist\*innen aus
der fast einjährigen Waldbesetzung bei Grund hatten diese in der Nacht von
Sonntag auf Montag aufgrund akuter Rodungsgefahr etabliert. Unmittelbar im
Anschluss der Räumung setzten die Behörden spezielle Rodungsmaschinen, sog.
"Harvester", zur Fällung des gesamten Areals ein.

Bei Ankunft der Polizei befanden sich vier Aktivist\*innen auf den Bäumen, die
auch die Nacht in der neuen Besetzung verbrachten. Um das Waldstück trotz
Aufteilung auf die beiden gefährdeten Teile im Altdorfer Wald möglichst gut zu
verteidigen, hatten die Aktivist\*innen ein komplexes Netzwerk (waagrechte
Querverbindungen aus speziellem baumschonenden Polypropylenseil) errichtet. So
konnte jede Kletter\*in mehrere Bäume gleichzeitig beschützen. Die Polizei
inhaftierte die geräumten Aktivist\*innen. Auf welche Gefangenensammelstelle sie
verbracht wurden und wie lange sie in Gewahrsam bleiben, wird den
Unterstützer\*innen vorenthalten.

Zum Zeitpunkt der Verfassung dieser Mitteilung befand sich als letzter Aktivist
noch Samuel Bosch (19) in den Bäumen. Anwohner\*innen und Eltern machen sich nun
auf, eine Mahnwache zur Unterstützung der Klimaaktivist\*innen aufzuschlagen.
"Bei uns bleibt keiner allein" sei wichtiger Grundsatz der
Waldbesetzungsgemeinschaft.


## Motto: Profit vor Nachhaltigkeit

"Das Motto der Behörden ist Profit vor Nachhaltigkeit!", deutet Kletteraktivist
Samuel Bosch (19) die Situation. "Die Behörden sperrten das gesamte
Räumungsgebiet großflächig ab. Unterstützer\*innen kamen nicht mehr zu uns
durch, wurden auf mehr als 150 Meter Entfernung gedrängt und konnten so unsere
Räumung nicht dokumentieren. Doch in all der Zeit durften die Kieslaster weiter
ungehindert fahren – den abgebauten Kies in die Schweiz und nach Österreich
transportieren, wo Umweltauflagen den Kiesabbau unrentabel machen."

Martin Lang (54), Anwohner aus Oberankenräute, der ebenfalls den Wald
verteidigte und heute aus seiner improvisierten Übernachtungsmöglichkeit in 10
Meter Höhe geräumt wurde, betont: "Durch die Räumung schufen die Behörden
Fakten, ohne einen demokratischen Diskurs abzuwarten."

Doch der eigentliche Skandal sei nicht die polizeiliche Räumung, sondern der
Hintergrund der Entscheidung: "Tullius spricht immer von Versorgungssicherheit
für die Region. Aber trotz des mehrjährigen Baubooms blieb die regionale
Kiesnachfrage weit hinter den Erwartungen zurück. Dennoch besteht Tullius
unbedingt auf der Erweiterung der Kiesgrube." Nach Ansicht der Aktivist\*innen
geht es Tullius nur darum, Profitrechte für die nächsten Jahrzehnte auf Kosten
der Umwelt zu sichern. "In Zeiten der Klimakrise Bäume fällen, die älter als man
selbst sind? Bäume sind unsere wichtigsten Verbündeten im Kampf gegen die
Erdaufheizung!", so Lang.

"Für die Kinder dieser Welt tut es mir leid, dass ich den Wald nicht besser
verteidigen konnte", gab Lang unmittelbar vor seiner Räumung durch.