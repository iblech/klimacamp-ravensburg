---
layout: page
title:  "10.11.2022: Klimaaktivisten zeigen CDU-Politiker Müller an"
date:   2022-10-15 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210151
#          YYMMDDX
---

*Schwäbische Zeitung: Online Artikel vom 10.11.2022*

# Drei Klimaaktivisten zeigen CDU-Bundestagsabgeordneten Müller nach Interview an 

Drei Klimaaktivisten haben den Ravensburger Bundestagsabgeordneten Axel Müller angezeigt. Grund ist eine Aussage des CDU-Politikers in einem Interview mit der „Schwäbischen Zeitung“. Grund sei eine Falschaussage des CDU-Abgeordneten, erklärt Samuel Bosch, einer der Aktivisten.

In dem Interview ging es hauptsächlich um Präventivstrafen für radikale Protestformen ging. Müller hatte im Gespräch gesagt:

"Darf ich die Basilika in Weingarten hinaufklettern, dort Transparente festmachen und dabei das Kirchendach beschädigen? Das kann es doch nicht sein." 

Diese Aussage stellt für die drei Klimaaktivisten, die vor der Bundestagswahl im vergangenen Jahr ein Banner am Dach der Weingartener Basilika aufgehängt hatten, eine Verleumdung und üble Nachrede dar.

„Axel Müller unterstellt uns, am Dach der Basilika in Weingarten schwere Sachbeschädigung begangen zu haben. Und das stimmt einfach nicht“, erklärt Samuel Bosch. Der 19-Jährige betont dagegen, bei der Banner-Aktion mit größter Sorgfalt vorgegangen zu sein.

"Deswegen verletzt uns eine solche Aussage, weil wir extra darauf geachtet haben, dass nirgends ein Schaden an der Basilika entsteht." Samuel Bosch.

Auch die Kirchengemeinde habe keinen Schaden ausmachen können, so Bosch. Das Landgericht sah es allerdings als erwiesen an, dass die Aktivisten einen Balkon der Basilika betraten und stellte Hausfriedensbruch fest – eine Geldstrafe in Höhe von 250 Euro war fällig.

Bei Axel Müllers Aussage zur Dachbeschädigung hätte sich die Klimaaktivisten „aber falsch dargestellt gefühlt und deswegen am Donnerstag bei der Polizeiwache in Ravensburg Anzeige erstattet“, betont Bosch.

Müller von Anzeige überrascht
Der Bundestagsabgeordnete Müller erfuhr erst auf Nachfrage der „Schwäbischen Zeitung“ von der Strafanzeige gegen ihn. „Ich kann mir das nicht erklären. Ich kenne aber auch nicht ihre Motive“, sagt der CDU-Politiker. Fest steht für ihn aber, dass er in dem Interview keine Behauptung aufgestellt hätte, sondern eine Frage in den Raum geworfen habe.

Das sei ein juristischer Unterschied. Wie es nun weitergeht, sei Angelegenheit der Ravensburger Staatsanwaltschaft, so Müller. „Aber ich vertraue da ganz auf die fachliche Expertise unserer unabhängigen Justiz.“

Allerdings rechnen nicht mal die drei Klimaaktivisten selbst mit einem Erfolg der Anzeige. „Wir hoffen, dass es eine symbolische Wirkung hat“, erklärt Samuel Bosch.

Es solle nicht mehr um die Scheindebatte gehen, welcher Protest erlaubt ist, sondern darum, welche konkreten Maßnahmen schnellstmöglich für den Klimaschutz angegangen werden müssen.

<figure>
    <img src="/img/cdu_mueller_original.jpg" width="925" height="685" alt="Ursprünglicher Artikel vom 10.11.2022" style="display: block; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">

  <figcaption>Überarbeitete Print-Version von CDU-Bundestagsabgeordneter Axel Müller vom 12.11.2022</figcaption>
</figure>

## Überarbeitete Version von CDU-Bundestagsabgeordneter Axel Müller
[12.11.2022 Schwäbische Zeitung: Klimaaktivisten zeigen CDU-Politiker Müller an
](https://pressreader.com/article/281711208629777)