---
layout: page
title:  "09.01.2023: Überraschend viele Menschen demonstrieren im Altdorfer Wald gegen Kies"
date:   2022-12-09 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210151
#          YYMMDDX
---


*Pressemitteilung vom 09.01.2023*

# Überraschend viele Menschen aus Anrainer-Gemeinden demonstrieren im Altdorfer Wald für Waldschutz, Trinkwasserschutz, Klimaschutz


Mehr als 400 Menschen, Kinder, Jugendliche, Erwachsene jeglichen Alters, auch ganze Familien aus der nächsten und weiteren Region waren dem Aufruf des breiten Bündnisses gefolgt und zeigten am heutigen Sonntag (8.1.2022) ab 14:00 Uhr bei einer Demonstration für Wald-, Trinkwasser- und Klimaschutz beim Altdorfer Wald mit ihrer Präsenz, den Plakaten und vielem mehr unmissverständlich, dass sie mit Entscheidungen der regionalen Politik nicht einverstanden sind.

Die Route führte direkt durch das im Regionalplanentwurf vorgesehene Kiesabbaugebiet auf dem Waldburger Rücken bei Grund, das aktuell noch als Teil des Altdorfer Waldes für die besonders hohe Trinkwasserqualität verantwortlich ist und auch zur Naherholung dient. Sie wurde von einem Fernsehteam des SWR und lokaler Presse begleitet. Die L317 wurde für die Demonstration polizeilich gesperrt.

# UMSTRITTENER "KLIMAHÖLLENPLAN"

Mit der Demonstration appellierten die Anwohner*innen an das Wirtschaftsministerium in Stuttgart, die zahlreichen Einwendungen gegen den umstrittenen Regionalplanentwurf des Regionalverbands sowie die aktuellen Entwicklungen der Klimakrise und der Klimaziele ernstzunehmen. Die große Anzahl an Menschen überraschte selbst die Organisator*innen, die erst vor wenigen Tagen mit den Planungen begonnen hatten, daher mit weniger Resonanz rechneten und für die große Teilnehmer*innenzahl nicht genügend Flyer vorrätig hatten.

Als "Klimahöllenplan" bezeichnete ein übermenschengroßes Leinwaldbild den Regionalplanentwurf. "Dank Regionalverband und Herrn Franke werden wir vom Kieslastverkehr überrollt!", mahnte das Plakat einer Anwohnerin in Bezug auf den Regionalplanentwurf. Die umstrittenen neuen Kiesgruben würden den Schwerlastverkehr, der jetzt schon die umliegenden Kommunen schwer belastet, noch intensivieren.

Reden von Wolfram Frommlet (Journalist), Roland Roth (Meteorologe), Hermann Schad (Geologe), Manne Walser (BUND) und Waldbesetzer*innen sowie Musik von Franziska Groß thematisierten Aspekte des Regionalplanentwurfs und der Dringlichkeit des Wasserschutzes, mit Bäumen als Speicher und Kies als Filter für Trinkwasser. "Kein Kiesabbau in diesem Wassereinzugsgebiet", hieß es auf der Demonstration immer wieder.


# REGIONALVERBAND ALS UNDEMOKRATISCH BEZEICHNET

"Es bestärkt uns immer wieder zu sehen, dass so viele Menschen unsere Überzeugung teilen. Gleichzeitig ist es schon dreist von den hiesigen Politiker*innen zu behaupten, sie würden die Meinung der Bevölkerung vertreten, wenn doch so offensichtlich ist, dass so viele gegen den Kiesabbau sind!", so Samuel Bosch. Bosch kritisiert dabei auch die aus ihrer Sicht undemokratische Zusammensetzung des Regionalverbands, der die Kiesabbaupläne erst auf den Weg brachte. In der Verbandsversammlung sind fast ausschließlich Bürgermeister und Altbürgermeister von CDU und FWV vertreten. "Die Grünen haben kaum Sitze – obwohl sie in den Gemeinderäten starke Kräfte sind. Auch andere Parteien sind kaum vertreten. Unter den 56 Mitgliedern sind nur sieben Frauen [1]. Wir bezweifeln die demokratische Legitimation des RVBO."


# ANWOHNER*INNEN BEREITEN SICH AUF RÄUMUNG DER BESETZUNG VOR

Mit Sorge wurden Gerüchte über eine baldige Räumung der seit knapp zwei Jahren bestehenden Baumhausdörfer im Altdorfer Wald vernommen, die sich seit einigen Tagen in der Region verbreiten. Anwohner*innen wünschten sich von den Kletteraktivist*innen im Altdorfer Wald konkrete Tipps, wie sie im Räumungsfall die Besetzung unterstützen können. Eine ausführliche Zusammenstellung wurde auf der Demonstration verteilt und ist auch auf der Website des Klimacamps, ravensburg.klimacamp.eu, verfügbar. Im Nachgang der Demonstration fand ein spezielles Klettertraining für Anwohner*innen statt, damit diese die Aktivist*innen im Räumungsfall unterstützen können.

[1] https://www.rvbo.de/Verband/Gremien-des-Regionalverbandes-Bodensee-Oberschwaben
