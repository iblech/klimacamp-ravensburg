---
layout: page
title:  "09.11.2022: L317: Anwohner bremsen erneut Kieslaster mit Gehzeug aus"
date:   2022-11-09 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2211021
#          YYMMDDX
---


*Pressemitteilung vom 09. November 2022*

# L317: Anwohner bremsen erneut Kieslaster mit Gehzeug aus

Mit Blick auf die internationale Klimakonferenz in Ägypten, mahnen sie, auch regional entschlossener gegen die Erderwärmung vorzugehen. Im Fokus steht dabei derweitere Kiesabbau im Altdorfer Wald. Dieser gefährdet nicht nur wertvolle Trinkwasserquellen, sondern zerstört großflächig Waldboden, der zusammen mit Bäumen große Mengen an CO2 absorbiert.
Intakte Wälder sind mit ihrer kühlenden Wirkung auch für das Regionalklima von entscheidender Bedeutung. Erklärtes Ziel der Aktivisten, rund um die Waldbesetzung im Altdorfer Wald bei Grund, ist die Verhinderungerung von Rodungen und Zerstörung von rund 60 Hektar Waldfläche im Altdorfer Wald für zusätzlichen Kiesabbau.

Die Klimabewegung hat in den letzten Tagen viel an Kritik einstecken müssen. Kanzler Scholz appelierte, dass Aktionen nicht zur Gefährung anderer beitragen sollten. Er fügt hinzu "Vielleicht könnte etwas Kreativität helfen"[1]. , In der aktuellen Pressemitteilung von heute, 9. November 2022 unterstreichen Ravensburger Klimaaktivist:innen, dass  die Sicherheit aller Beteiligten und der Bevölkerung bei jeder der durchgeführten Aktionen oberste Priorität habe. 
"Ob unsere Gehzeug-Aktionen oder auch andere kreativ sind, ist Geschmacksache,  aber wir lenken damit wichtige Aufmerksamkeit auf die schnell fortschreitende Klimakrise. Das sind wir unseren Kindern, Enkeln und Mitmenschen, schuldig erklärt Gudrun Bosch.
"Unsere Protestform mit Gehzeugen bringt zwar den Verkehr kurzfristig ins Stocken, aber Rettungswagen weichen wir selbstverständlich aus und unterbrechen unseren Protest. Auch andere Fahrzeuge können uns , wie schon beim letzten Mal, einfach überholen", fügt Lukas Häfele, Klimaaktivist aus Wolfegg hinzu.

Die Schwäbische Zeitung berichtete bereits letzte Woche über eine Gehzeug-Aktion am Kieswerk Tullius[2].
Mittlerweile ermittelt die Staatsanwaltschaft Ravensburg und prüft, ob die Gehzeug-Aktion rechtlich zu beanstanden ist [3]. Die Rechtsprechung, ob Gehzeuge mit politischen Parolen auch vom Paragraf 25 der Straßenverkehrsordnung gedeckt sind, wird derzeit ermittelt. Laut Polizeiauskunft ist es auch fraglich ob wirklich Gegenstände mit den Gehzeugen transportiert wurden. Bei der heutigen Aktion werden daher nur leere Banner zu sehen sein und protestieren damit für demokratische Grundrechte[4]. Weiter wird auch ein Windrad auf dem Gehzeug transportiert, um auf den dringenden Ausbau von Erneuerbare Energien aufmerksam zu machen.

## Quellen
[1] https://www.spiegel.de/politik/deutschland/olaf-scholz-laestert-ueber-radikale-klimaaktivisten-etwas-kreativitaet-koennte-helfen-a-cfde653d-f125-4462-ae5e-d1123a697ac3

[2] https://www.pressreader.com/article/282080575787345

[3] https://www.pressreader.com/article/282110640579528

[4] https://twitter.com/kevinrothrock/status/1502761903046774786

FORDERUNGEN DER ANWOHNER:INNEN IM BESONDEREN:
- Kiesausbeute im Altdorfer Wald und anderen Wäldern sofort beenden
- Bodenschonende Waldbewirtschaftung (Harvester raus aus dem Wald!)
- Ausweisung weiterer Klimaschutzzonen im Wald
- keine weiteren Kahlschläge in Zeiten der Klimakrise
- Kieskonzerne zur Offenlegung ihrer Kiesexporte verpflichten 
- Umweltabgabe für Kies, so wie es in Österreich üblich ist
- Baldige Energiewende auch in Süddeutschland - Ausbau von Windkraftanlagen