---
layout: page
title:  "13.10.2022: Spontane Mahnwache vor der Linse in Weingarten wegen Waldrodung bei der Kiesgrube Tullius"
date:   2022-10-13 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2210131
#          YYMMDDX
---


*Pressemitteilung vom 13. Oktober 2022*

# Spontane Mahnwache vor der Linse in Weingarten wegen Waldrodung bei der Kiesgrube Tullius

Heute Abend um 18 Uhr findet vor dem Kulturzentrum "Linse" in Weingarten eine spontane Protestaktion von Bürger*innen und Aktivist*innen statt. Grund für die Mahnwache ist die Rodung für die Erweiterung der Kiesgrube Tullius bei Oberankenreute. Diese war unter Polizeischutz in den frühen Morgenstunden des heutigen Donnerstags durchgeführt worden. Zudem findet heute Abend im Kulturzentrum Linse eine Vorführung des Films „90 Meter“ und eine anschließende Diskussionsrunde mit Aktivist*innen und dem Regisseur statt.

„Wir setzen heute Abend ein Zeichen gegen die Klimazerstörung, die hier in Oberschwaben unter Polizeischutz durchgeführt wird. Es kann nicht sein, dass weiterhin Bäume fallen und Kiesgruben erweitert werden, obwohl wir wissen, dass wir uns dadurch unsere Lebensgrundlage nachhaltig zerstören. Wir fordern, dass solche Rodungen nicht mehr unter Polizeischutz durchgeführt werden, sondern gar nicht mehr. Protest lässt sich ganz einfach durch die Umsetzung von wirksamen Klimamaßnahmen überflüssig machen.“ so Manfred Scheurenbrand (67) aus Waldburg.