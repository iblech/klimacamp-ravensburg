---
layout: page
title:  "23.12.2020: Weihnachtskonzert am Baumhaus"
date:   2020-12-23 23:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2012230
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 23. Dezember 2020*

# Weihnachtskonzert am Baumhaus

[Zur Aufzeichnung des Konzerts (Ausschnitt)](https://www.youtube.com/watch?v=EiQ6_IhSoHw){: .btn .btn-purple }

Am morgigen Heiligabend wird die Gruppe "Das Bäumchen-Kollektiv" von 13:30 Uhr
bis 14:30 Uhr ein Weihnachtskonzert am Fuße des Ravensburger Baumhausklimacamps
geben. "Die Gruppe spielte auch schon im Dannenröder Wald in Hessen", freut
sich Aktivistin Mia (18) über den Besuch. "Auf Gitarre, Ukulele und Cajón werden wir
versuchen, den Klimacamper\*innen ein schönes Weihnachtsfest zu bereiten.
Zwischen einzelnen Stücken verlesen wir stimmungsvolle Gedichte", so Kim
Schulz (23) vom Bäumchen-Kollektiv.

Ein Grund zum Feiern seien die Weihnachtsfeiertage für die Schüler\*innen indes
nicht. "Besonders traurig machte uns die Stellungnahme von Stadtsprecher Alfred
Oswald", führt Aktivist Samuel Bosch (17) aus. Dieser gab in der Schwäbischen
[1] bekannt, dass die Stadtverwaltung den Hinweis auf die gewaltige
Ambitionslücke Ravensburgs in Sachen Klimagerechtigkeit nicht verstehe. "Wie
viele Wissenschaftler\*innen möchtet ihr denn noch geringschätzen? Ihr plant,
das uns zustehende CO2-Restbudget um mehr als das Doppelte zu überschreiten!"
[2]. Bosch kritisierte auch, dass die "dringend benötigte Mobilitätswende nicht
angegangen wurde, sondern nur Rückschritte erlebt hätte". "Laut
Polizeistatistik [3] sahen sich 2019 mehr als 10.000 Menschen genötigt, sich
neu Autos anzuschaffen, da die Stadt immer noch nicht in ein effektives
Radwegenetz und einen leistungsfähigen und erschwinglichen öffentlichen
Personennahverkehr investiert."

"Da die Stadt unseren legitimen Protest immer noch kriminalisiert, können wir
nicht ausschließen, dass das Ordnungsamt morgen das Weihnachtskonzert trotz
erfolgter Anmeldung unterbindet", warnt Boschs Kollegin Mia. "Wir sind aber
zuversichtlich und freuen uns auf die Corona-sichere Darbietung." Alle
Interessierten seien herzlich eingeladen, sofern Maske getragen und ein
erheblicher Sicherheitsabstand eingehalten werde.

In den letzten Tagen erfuhr das Klimacamp immer weiter Zuspruch von der
Bevölkerung. "Mädchen aus der Nachbarschaft sägten Weihnachtssterne", freut
sich Bosch. "Diese hängen nun überall in unserem Baumhaus als Dekoration."
Zudem komme seit einigen Tagen immer ein kleines Kind zum Baum. "Es liest bei
jedem seiner Besuche unsere Forderungsbanner vor."

[1] https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-will-aktivisten-zum-aufgeben-bewegen-_arid,11308373.html<br>
[2] https://ravensburg.klimacamp.eu/co2-budget/<br>
[3] https://ppravensburg.polizei-bw.de/wp-content/uploads/sites/25/2020/03/20200219-PPRV_Sicherheitsbericht-Verkehr-2019.pdf#page=6

## Hinweis

Das Klimacamp ist auch über die Feiertage rund um die Uhr besetzt (die
Klimakrise macht ebenfalls keine Ferien). Die schon angekündigte Traverse über die
Schussenstraße wird absolut verkehrssicher befestigt werden (auf Erfahrung von
Statik-Expert\*innen aus der Besetzung des Dannenröder Walds wird
zurückgegriffen).
