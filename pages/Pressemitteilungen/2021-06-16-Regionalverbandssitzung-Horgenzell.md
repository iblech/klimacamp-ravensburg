---
layout: page
title:  "16.06.2021: Klimacamperinnen demonstrieren nackt vor Regionalverbandssitzung in Horgenzell"
date:   2021-06-16 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106160
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 16.6.2021*

# Klimacamperinnen demonstrieren nackt vor Regionalverbandssitzung in Horgenzell

Anlässlich der Sitzung des Planungsausschusses des Regionalverbands
Bodensee-Oberschwaben, bei der die über 2.700 Einwendungen aus der
Zivilgesellschaft gegen den aktuellen Regionalplanentwurf diskutiert werden,
demonstrieren Aktivistinnen des seit Dienstagabend bestehenden Klimacamps in
Horgenzell nackt für Klimagerechtigkeit und demokratische Teilhabe.

"Von den 20 Mitgliedern des Planungsausschusses sind nur drei Frauen. Der
Ausschuss spricht mit seinen Klimazerstörungsplänen nicht für uns!", so eine
Aktivistin, die im besetzten Altdorfer Wald als Carina (24) bekannt ist. "Wir
sind genauso ungeschützt wie unser Alti", erklärt sie die Aktion.

Die Aktivist\*innen des Klimacamps in Horgenzell fordern eine Demokratisierung
des Regionalverbands: Die tatsächlichen Mehrheitsverhältnisse in den
Gemeinderäten sollten von der Verbandsversammlung akkurat abgebildet werden und
Frauen sollten ein Mitspracherecht erhalten [[1]].

Nach einer Überblicksstudie [[2]] tragen Frauen im statistischen Durchschnitt
viel weniger zur Klimakrise bei wie Männer, sind gleichzeitig aber viel stärker
von ihr betroffen und in den relevanten Gremien in Politik und Wirtschaft kaum
vertreten.

## Quellen

[1]: https://www.rvbo.de/Verband/Gremien-des-Regionalverbandes-Bodensee-Oberschwaben
[1] In der Verbandsversammlung sind fast
ausschließlich Bürgermeister und Altbürgermeister von CDU und FWV
vertreten. "Die Grünen haben kaum Sitze -- obwohl sie in den
Gemeinderäten starke Kraft sind. Auch andere Parteien sind kaum
vertreten", so Johanna Wenz (17). \
Quelle: [https://www.rvbo.de/Verband/Gremien-des-Regionalverbandes-Bodensee-Oberschwaben][1]


[2]: https://www.endlich-wachstum.de/wp-content/uploads/2018/12/Moderationshilfe.pdf
[2] Frauen essen nur halb so viel Fleisch und fahren nur halb so viel Auto; die
Sterberate bei Frauen über 65 stieg bei Hitzewellen in Europa um 23 %, bei
Männern dagegen nur um 16 %, 80 % der durch den klimawandel vertriebenen
Menschen sind Frauen; von 790 europäischen Ministerialbeamt\*innen in den
Sektoren Umwelt, Verkehr und Energie sind nur 202 Frauen und bei den 20 größten
Energieunternehmen sind die Geschäftsleitungen nur zu 5 % mit Frauen\* besetzt. \
Quelle: [https://www.endlich-wachstum.de/wp-content/uploads/2018/12/Moderationshilfe.pdf][2]

## Hinweis
Polizeiliche Intervention könnte die Aktion verhindern. Aktuelle Informationen
über den Aktionsverlauf gibt Ingo Blechschmidt.

## Termin und Ort
Die Aktion wird zwei Mal statt: Am heutigen Mittwoch (16.6.) kurz vor Beginn der
Ausschussitzung um 14:00 Uhr, sowie zusätzlich am heutigen Mittwoch (16.6.)
zwischen 9:00 Uhr und 10:00 Uhr, kurz vor dem Beginn einer anderen RVBO-Sitzung.

Die Aktion findet vor dem Tagungsgebäude, der Mehrzweckhalle in Horgenzell
(Bushaltestelle Horgenzell Schule), statt.